$(function () {

    $(document).on('click', '.simple-delete-row', function (e) {
        e.preventDefault();

        var $this = $(this);

        $this.closest('tr').remove();
    });

    $(document).on('click', '.simple-add-row', function (e) {
        e.preventDefault();

        var $this = $(this);

        var newTr = '<tr>' +
            '<td>' + '<input type="text" class="form-control" name="name[]" placeholder="Enter Name" />' + '</td>' +
            '<td>' + '<input type="text" class="form-control" name="appendix[]" placeholder="Enter Appendix" />' + '</td>' +
            '<td>' +
            '<button class="btn btn-danger btn-sm simple-delete-row">' +
            '<i class="fa fa-aw fa-trash-o"></i>' +
            ' Delete' +
            '</button>' +
            '</td>' +
            '</tr>'
        $this.closest('table').find('tbody').append(newTr);

    })

});


$.fn.bootStrapModal = function (options) {

    var settings = $.extend({
        url: ''
    })

}

function parseBool(val) {
    if ((typeof val === 'string' && (val.toLowerCase() === 'true' || val.toLowerCase() === 'yes')) || val === 1)
        return true;
    else if ((typeof val === 'string' && (val.toLowerCase() === 'false' || val.toLowerCase() === 'no')) || val === 0)
        return false;
    return null;
}

function ajaxCall(url, type, dataType, data, success_method, error_method, selector, additionalData) {

    $.ajax({
        url: url,
        type: type,
        dataType: dataType,
        data: data,
        async: false,
        success: function (response) {
            success_method(response, selector, additionalData);
        },
        error: function (response) {
            error_method(response);
        },
        statusCode: {
            403: function (xhr) {
                error_method(xhr.responseText);
            }
        }
    });
}

//$("#addDocument").validateRequired();

$(function () {


    $(document.body).on("click", "#saveSelfPassword", function (e) {

        e.preventDefault();
        var url = baseUrl + "/changeSelfPassword",
            data = $("#changeSelfPassword").serialize();

        ajaxCall(url, "post", "json", data, function (response) {
            if (response.status == "success") {

                $("#changeSelfPassword").find("input").val('');
            }
            show_gritter(response.status, response.message);


        }, function (response) {
            show_gritter('error', response.responseText);
        });
    })


    $(document.body).on('click', '.changeUserDetail', function () {
        var $this = $(this),
            orgVal = $this.attr('data-orgValue'),
            html = "<input type='text' class='form-control' value='" + orgVal + "'/>";

        $(".data-container").html(html);
        $this.addClass('saveNow').html("<i class='fa fa-aw fa-save'></i> Save");
        $this.removeClass('changeUserDetail');
    });

});

function createModal(response, $this, modalId) {
    createBootstrapModel(modalId, response);
}

function searchMapInitialize(mapCanvas, searchId, zoom, lat, lng) {
    if (zoom == '' || zoom == undefined) {
        zoom = 12;
    }

    if (lat == '' || lat == undefined) {
        lat = 40.7112926;
    }

    if (lng == '' || lng == undefined) {
        lng = -73.98331250000001;
    }
    var mapOptions = {
        center: new google.maps.LatLng(lat, lng),
        zoom: zoom
    };
    var map = new google.maps.Map(document.getElementById(mapCanvas),
        mapOptions);

    var input = /** @type {HTMLInputElement} */(
        document.getElementById(searchId));

    var autocomplete = new google.maps.places.Autocomplete(input);
    autocomplete.bindTo('bounds', map);

    var infowindow = new google.maps.InfoWindow();
    var marker = new google.maps.Marker({
        map: map,
        position: mapOptions.center,
        draggable: true
    });

    google.maps.event.addListener(marker, "dragend", function (event) {
        var lat = event.latLng.lat();
        var lng = event.latLng.lng();
        codeLatLng(lat, lng);
    });

    google.maps.event.addListener(autocomplete, 'place_changed', function () {
        infowindow.close();
        marker.setVisible(false);
        var place = autocomplete.getPlace();

        if (!place.geometry) {
            return;
        }
        var lat = place.geometry.location.lat();
        var lng = place.geometry.location.lng();
        $('.map-li').css('display', 'block');

        codeLatLng(lat, lng);
        if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
        } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);
        }
        marker.setIcon(/** @type {google.maps.Icon} */({
            url: place.icon,
            size: new google.maps.Size(71, 71),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(17, 34),
            scaledSize: new google.maps.Size(35, 35)
        }));
        marker.setPosition(place.geometry.location);
        marker.setVisible(true);

        var address = '';
        if (place.address_components) {
            address = [
                (place.address_components[0] && place.address_components[0].short_name || ''),
                (place.address_components[1] && place.address_components[1].short_name || ''),
                (place.address_components[2] && place.address_components[2].short_name || '')
            ].join(' ');
        }

        infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
        infowindow.open(map, marker);
    });

    $('#' + mapCanvas).ready(function () {
        setTimeout(function () {
            google.maps.event.trigger(map, 'resize');
        }, 1000);
    });
}


function codeLatLng(lat, lng) {
    var geocoder = new google.maps.Geocoder();
    var latlng = new google.maps.LatLng(lat, lng);
    $('#lat').val(lat);
    $('#lng').val(lng);
    geocoder.geocode({'latLng': latlng}, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            var street_name = '';
            var country_name = '';
            var address_components = results[0].address_components;

            console.log(results);

            $.each(address_components, function (i, v) {
                if (v.types[0] == 'postal_code') {
                    $('#postcode').val(v.long_name);
                } else if (v.types[0] == 'street_number') {
                    street_name += v.long_name;
                } else if (v.types[0] == 'route') {
                    street_name += ' - ' + v.long_name;
                } else if (v.types[0] == 'country') {
                    $('#country').val(v.long_name);
                } else if (v.types[0] == 'administrative_area_level_1') {
                    $('#state').val(v.long_name);
                } else if (v.types[0] == 'locality') {
                    $('#city').val(v.long_name);
                }
                $('#search-address').val(results[0].formatted_address);
            });
        } else {
            alert('Geocoder failed due to: ' + status);
        }
    });
}


function contactmap_initialize(name, address, lat, lng, mapId) {
    var mapOptions = {
        center: new google.maps.LatLng(lat, lng),
        zoom: 16
    };
    var map = new google.maps.Map(document.getElementById(mapId),
        mapOptions);

    var infowindow = new google.maps.InfoWindow();
    var marker = new google.maps.Marker({
        map: map,
        position: mapOptions.center,
        draggable: false
    });
    marker.setMap(map);
    marker.setVisible(true);
    infowindow.setContent('<div><strong>' + name + '</strong><br>' + address);
    infowindow.open(map, marker);
}