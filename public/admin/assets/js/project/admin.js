$(function () {

    $('div.alert').not('.alert-important').delay(4000).slideUp(300);

    $("li.program_gallery_image img").on('click', function () {
        $(this).closest('label').children().prop('checked', true);
    });


    //$('section.sidebar').find('a').each(function(i, v) {
    //
    //    var href = $(this).attr('href');
    //
    //    var url = window.location.href;
    //
    //    var result = url.match('^' + href);
    //
    //    if(result !== null) {
    //        $(this).parent('li').addClass('active').parents('li.treeview').addClass('active');
    //    }
    //
    //});


    //$('section.sidebar').find('a').each(function(i, v) {
    //
    //    var href = $(this).attr('href');
    //
    //    var url = window.location.href;
    //
    //    var hrefSlash = href + '/';
    //    var result = url.match('^' + hrefSlash);
    //
    //    if(url == href || result !== null) {
    //        $(this).parent('li').addClass('active').parents('li.treeview').addClass('active');
    //    }
    //
    //});

    $('section.sidebar').find('a').each(function(i, v) {

        var href = $(this).attr('href');

        var url = window.location.href;

        if(url == href || url.match(href+'/') || url.match(href+'#')) {
            $(this).parent('li').parents('li.treeview').find('li.active').removeClass('active');
            $(this).parent('li').addClass('active').parents('li.treeview').addClass('active');
        }

    });

    $(".image-delete-button").on('click', function (e) {
        e.preventDefault();

        $this = $(this);

        var url = baseurl + "/media/" + $this.data('mediaId');


        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this image!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel plx!",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function (isConfirm) {
            if (isConfirm) {

                $.ajax({
                    url: url,
                    method: 'post',
                    data: {
                        '_method': 'delete',
                        '_token': token
                    },
                    success: function (response) {
                        console.log(response);
                        if (response == 'success') {
                            $this.closest('.program_gallery_image').remove();
                            swal("Deleted!", "Your image has been deleted.", "success");
                        }
                    }
                })


            } else {
                swal("Cancelled", "Your image is safe :)", "error");
            }
        });




    });


    $(".file-delete-button").on('click', function (e) {
        e.preventDefault();

        $this = $(this);

        var url = baseurl + "/media/" + $this.data('mediaId');


        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this file!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel plx!",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function (isConfirm) {
            if (isConfirm) {

                $.ajax({
                    url: url,
                    method: 'post',
                    data: {
                        '_method': 'delete',
                        '_token': token
                    },
                    success: function (response) {
                        console.log(response);
                        if (response == 'success') {
                            $this.parents('div.btn-group').remove();
                            swal("Deleted!", "Your file has been deleted.", "success");
                        }
                    }
                })


            } else {
                swal("Cancelled", "Your image is safe :)", "error");
            }
        });

    });


    $('.deleteContentForm').on('submit', function (e) {
        e.preventDefault();

        $this = $(this);

        swal({
            title: "Are you sure?",
            text: "You will not be able to recover any files related to this item!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel plx!",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function (isConfirm) {
            if (isConfirm) {
                $this.unbind('submit').submit();
                //swal("Deleted!", "Your imaginary file has been deleted.", "success");
            } else {
                swal("Cancelled", "Your data is safe :)", "error");
            }
        });

        //var a = confirm('Are you sure you want to delete?');
        //console.log(a);
        //if (!a) {
        //    //return false;
        //    e.preventDefault();
        //}

    });


});

$(function(){
    $(document).on('click', ".create-bootstrap-modal", function (e) {

        e.preventDefault();
        var $this = $(this);
        var link = $this.attr('href');
        var datatosend = {};
        var modalId = $this.attr('modal-id');

        if (link.indexOf('|') != -1) {
            var link_array = link.split('|');
            link = link_array[0];
            datatosend = link_array[1];
        }

        $.get(link, datatosend, function (html) {
            createBootstrapModel(modalId, html);
            $("form").validate();
        });

    })

})
function createBootstrapModel(divId, content) {
    $("#" + divId).remove();
    $('body').append(content);
    $('#' + divId).modal();
    $('.datepicker').datepicker({
        'autoclose': true
    });
}