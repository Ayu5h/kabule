<?php namespace App;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Illuminate\Database\Eloquent\Model;

class Publication extends Model implements SluggableInterface
{

    protected $table = "publications";

    use SluggableTrait;

    protected $fillable = [
        'category'
    ];

    protected $sluggable = [
        'build_from' => 'category',
        'save_to' => 'slug',
    ];

    public function subPublications()
    {
        return $this->hasMany('App\SubPublication');
    }

    public function scopeOldest($query)
    {
        return $query->orderBy('id', 'asc');
    }

    public function delete()
    {
        $subPublications = $this->subPublications;

        foreach($subPublications as $subPublication) {
            $subPublication->delete();
        }

        parent::delete();
    }

}
