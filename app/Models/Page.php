<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class Page extends Model implements SluggableInterface{

    use SluggableTrait;

    protected $sluggable = [
        'build_from' => 'name',
        'save_to'    => 'slug',
    ];

    protected $fillable = ['name', 'title', 'is_core', 'featured_image', 'featured_thumbnail', 'type', 'description', 'meta_title', 'meta_description'];

    function photos() {
        return $this->morphMany('App\Media', 'imageable');
    }

    function scopeIsCore(){
        return $this->is_core == 1;
    }

    function scopeIsBirdPage($query) {
        return $query->whereType(1);
    }

    function scopeIsAboutPage($query) {
        return $query->whereType(0);
    }

}
