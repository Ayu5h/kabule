<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CitiesCategory extends Model {

    protected $table = 'cities_lists_categories';

	protected $fillable = [
        'name', 'description'
    ];

    function listedBirds() {
        return $this->hasMany('App\Models\CitiesListedBird', 'cities_category_id');
    }

}
