<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CitiesListedBird extends Model {

	protected $table = 'cities_lists_birds';

    protected $fillable = ['name', 'appendix'];

    function category() {
        return $this->belongsTo('App\Models\CitiesCategory', 'cities_category_id');
    }

}
