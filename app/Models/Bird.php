<?php namespace App\Models;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Illuminate\Database\Eloquent\Model;

class Bird extends Model implements SluggableInterface{

    use SluggableTrait;

    protected $sluggable = [
        'build_from' => 'name',
        'save_to'    => 'slug',
    ];

    protected $fillable = ['name', 'kingdom', 'image', 'phylum', 'class', 'order', 'family', 'genus', 'species',
        'sub_species', 'synonym', 'distribution', 'short_description', 'description', 'local_name',
        'featured_thumbnail', 'featured_image'];

    public function photos() {
        return $this->morphMany('App\Media', 'imageable');
    }
}
