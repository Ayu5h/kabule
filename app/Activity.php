<?php namespace App;

use Carbon\Carbon;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Illuminate\Database\Eloquent\Model;

class Activity extends Model implements SluggableInterface {

    use SluggableTrait;

	protected $fillable = [
        'name',
        'date',
        'featured_image',
        'featured_thumbnail',
        'short_intro',
        'description',
        'program_id',
        'meta_title',
        'meta_description'
    ];

    protected $sluggable = [
        'build_from' => 'name',
        'save_to'    => 'slug',
    ];

    protected $dates = ['date'];

    public function program()
    {
        $this->belongsTo('App\Program');
    }

    public function getDateAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d');
    }

    public function getFormattedDateAttribute($date)
    {
        return Carbon::parse($date)->format('M d, Y');
    }

    public function photos()
    {
        return $this->morphMany('App\Media', 'imageable');
    }

    public function delete()
    {
        $medias = $this->photos;

        foreach($medias as $media) {
            $media->delete();
        }

        parent::delete();
    }

}
