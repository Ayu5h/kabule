<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model {

    protected $fillable = [
    ];

    public function photos()
    {
        return $this->morphMany('App\Media', 'imageable');
    }

}
