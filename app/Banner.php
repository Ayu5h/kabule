<?php namespace App;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Illuminate\Database\Eloquent\Model;

class Banner extends Model implements SluggableInterface {

    use SluggableTrait;

    protected $table = "banners";
    protected $fillable = ['title', 'description', 'meta_title', 'meta_description', 'links_to_page', 'link'];

    protected $sluggable = [
        'build_from' => 'title',
        'save_to'    => 'slug',
    ];

    public function photos()
    {
        return $this->morphMany('App\Media', 'imageable');
    }

    public function delete()
    {
        $medias = $this->photos;

        foreach($medias as $media) {
            $media->delete();
        }
        parent::delete();
 
    }

}
