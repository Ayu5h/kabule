<?php namespace App;

use Carbon\Carbon;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Illuminate\Database\Eloquent\Model;

class News extends Model implements SluggableInterface {

    use SluggableTrait;

	protected $fillable = [
        'name',
        'featured_image',
        'featured_thumbnail',
        'description',
        'meta_title',
        'meta_description',
        'program_id',
        'date'
    ];

    protected $sluggable = [
        'build_from' => 'name',
        'save_to'    => 'slug',
    ];

    protected $dates = ['date'];

    public function getDateAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d');
    }

    public function getFormattedDateAttribute($date)
    {
        return Carbon::parse($date)->format('F d, Y');
    }


    public function program()
    {
        return $this->belongsTo('App\Program');
    }

    public function photos()
    {
        return $this->morphMany('App\Media', 'imageable');
    }

    public function delete()
    {
        $medias = $this->photos;

        foreach($medias as $media) {
            $media->delete();
        }

        parent::delete();
    }



}
