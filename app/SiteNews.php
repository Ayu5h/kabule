<?php namespace App;

use Carbon\Carbon;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphMany;

class SiteNews extends Model implements SluggableInterface
{

    use SluggableTrait;



    protected $fillable = [
        'link',
        'name',
        'featured_image',
        'featured_thumbnail',
        'date',
        'type',
        'description',
        'meta_title',
        'meta_description',
        'description'
    ];

    protected $sluggable = [
        'build_from' => 'name',
        'save_to' => 'slug'
    ];

    protected $dates = ['date'];

    public function getDateAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d');
    }

    public function photos()
    {
        return $this->morphMany('App\Media', 'imageable');
    }

    function scopeLatest($query){
        return $query->orderBy('id', 'desc');
    }

    public function delete() {

        $medias = $this->photos;

        if(!empty($medias)) {
            foreach($medias as $media) {
                $media->delete();
            }
        }

        parent::delete();
    }

}
