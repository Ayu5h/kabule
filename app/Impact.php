<?php namespace App;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Illuminate\Database\Eloquent\Model;
class Impact extends Model implements SluggableInterface
{

    use SluggableTrait;

    protected $fillable = [
        'featured_image',
        'featured_thumbnail',
        'title',
        'show_on_home_page',
        'intro_text',
        'description',
        'meta_title',
        'meta_description',
        'sponsor'
    ];

    protected $sluggable = [
        'build_from' => 'title',
        'save_to'    => 'slug',
    ];

    protected $table = "impacts";

    public function photos()
    {
        return $this->morphMany('App\Media', 'imageable');
    }


    public function delete()
    {
        $this->deleteMedias();

        parent::delete();
    }

    private function deleteMedias()
    {
        $medias = $this->photos;

        foreach($medias as $media) {
            $media->delete();
        }

    }

    public function scopeLatest($query){
        return $query->orderBy('id', 'desc');
    }


}
