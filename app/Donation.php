<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Donation extends Model {

	protected $table = "donations";
    protected $fillable = [
      'name',
        'phone',
        'email',
        'amount',
        'donation_program_id'
    ];

    public function scopeDonationProgramId($query, $donationProgramId){
        return $query->whereDonationProgramId($donationProgramId);
    }

}
