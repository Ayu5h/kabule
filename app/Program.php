<?php namespace App;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Illuminate\Database\Eloquent\Model;
class Program extends Model implements SluggableInterface
{

    use SluggableTrait;

    protected $fillable = [
        'featured_image',
        'featured_thumbnail',
        'title',
        'show_on_home_page',
        'intro_text',
        'description',
        'meta_title',
        'meta_description',
        'sponsor'
    ];

    protected $sluggable = [
        'build_from' => 'title',
        'save_to'    => 'slug',
    ];

    protected $table = "programs";

    public function photos()
    {
        return $this->morphMany('App\Media', 'imageable');
    }

    public function news()
    {
        return $this->hasMany('App\News');
    }

    public function topics()
    {
        return $this->hasMany('App\Topic');
    }

    public function activities()
    {
        return $this->hasMany('App\Activity');
    }

    public function delete()
    {
        $this->deleteMedias();

        $this->deleteNews();

        $this->deleteTopics();

        $this->deleteActivities();

        parent::delete();
    }

    private function deleteMedias()
    {
        $medias = $this->photos;

        foreach($medias as $media) {
            $media->delete();
        }

    }

    private function deleteNews()
    {
        $allNews = $this->news;

        foreach($allNews as $news){
            $news->delete();
        }

    }

    public function deleteTopics()
    {
        $topics = $this->topics;

        foreach($topics as $topic) {
            $topic->delete();
        }
    }

    public function deleteActivities()
    {
        $activities = $this->activities;

        foreach($activities as $activity) {
            $activity->delete();
        }
    }

    public function scopeLatest($query){
        return $query->orderBy('id', 'desc');
    }


}
