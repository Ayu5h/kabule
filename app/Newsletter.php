<?php namespace App;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Illuminate\Database\Eloquent\Model;

class Newsletter extends Model implements SluggableInterface
{
    use SluggableTrait;

    protected $sluggable = [
        'build_from' => 'title',
        'save_to' => 'slug'
    ];

    protected $fillable = [
        'title',
        'file_name',
        'file_path',
        'image',
        'thumbnail'
    ];

    public function photos()
    {
        return $this->morphMany('App\Media', 'imageable');
    }

}
