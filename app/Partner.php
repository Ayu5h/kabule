<?php namespace App;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Illuminate\Database\Eloquent\Model;
class Partner extends Model implements SluggableInterface
{

    use SluggableTrait;

    protected $fillable = [
        'featured_image',
        'featured_thumbnail',
        'title',
        'link'
    ];

    protected $sluggable = [
        'build_from' => 'title',
        'save_to'    => 'slug',
    ];

    protected $table = "partners";

    public function photos()
    {
        return $this->morphMany('App\Media', 'imageable');
    }


    public function delete()
    {
        $this->deleteMedias();

        parent::delete();
    }

    private function deleteMedias()
    {
        $medias = $this->photos;

        foreach($medias as $media) {
            $media->delete();
        }

    }

    public function scopeLatest($query){
        return $query->orderBy('id', 'desc');
    }


}
