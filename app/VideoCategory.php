<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class VideoCategory extends Model {

	protected $fillable = ['name'];


    function videos(){
        return $this->hasMany('App\Video', 'category_id');
    }

    public function delete()
    {
        $this->deleteVideos();

        parent::delete();
    }

    private function deleteVideos(){
        $allVideos = $this->videos;

        foreach($allVideos as $video){
            $video->delete();
        }
    }
}
