<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class PageContent extends Model {

	protected $fillable = ['title', 'description', 'icon'];

    public function photos()
    {
        return $this->morphMany('App\Media', 'imageable');
    }

}
