<?php

//Route::get('/', ['as' => 'site', 'uses' => 'Site\SiteController@index']);
//
//Route::get('about-us/{slug?}', ['as' => 'aboutUs', 'uses' => 'Site\AboutController@index']);
////Route::get('about-us/committee/{slug}', ['as' => 'committeeDetail', 'uses' => 'Site\AboutController@committeeDetail']);
//Route::get('what-we-do/{whatWeDo}', ['as' => 'viewWhatWeDo', 'uses' => 'Site\WhatWeDoController@viewWhatWeDo']);
//
//Route::get('get-involved/{getInvolved}', ['as' => 'viewGetInvolved', 'uses' => 'Site\GetInvolvedController@viewGetInvolved']);
//
//Route::get('disaster-response/{disasterResponse}', ['as' => 'viewDisasterResponse', 'uses' => 'Site\DisasterResponseController@viewDisasterResponse']);
//
//Route::get('resources/{resourceSlug}', ['as' => 'viewResources', 'uses' => 'Site\PublicationController@show']);
//
////Route::get('our-impacts', ['as' => 'ourImpacts', 'uses' => 'Site\SiteController@impact']);
//Route::get('our-impacts/{ourImpacts}', ['as' => 'viewImpact', 'uses' => 'Site\SiteController@viewImpact']);
//
//Route::get('news/{news}', ['as' => 'viewNews', 'uses' => 'Site\SiteController@viewNews']);
//
//Route::get('contact', ['as' => 'contact', 'uses' => 'Site\ContactController@index']);
//
//Route::post('contact/post', 'Site\ContactController@postContact');


//Route::get('/doied', ['as' => 'home', 'uses' => 'Front\HomeController@index']);
//
//Route::get('banner-detail/{bannerSlug}', ['as' => 'bannerDetail', 'uses' => 'Front\HomeController@bannerDetail']);
//
////Route::get('about-us/{slug?}', ['as' => 'aboutUs', 'uses' => 'Front\AboutController@index']);
////Route::get('about-us/committee/{slug}', ['as' => 'committeeDetail', 'uses' => 'Front\AboutController@committeeDetail']);
//
//
//Route::get('birds/{page}', ['as' => 'birdPage', 'uses' => 'Front\BirdController@showPage']);
//Route::get('cities-listed-birds-nepal', ['as' => 'citiesListedBirds', 'uses' => 'Front\BirdController@citiesListedBirds']);
//Route::get('protected-birds-nepal', ['as' => 'protectedBirds', 'uses' => 'Front\BirdController@protectedBirds']);
//Route::get('protected-bird/{slug}', ['as' => 'viewBird', 'uses' => 'Front\BirdController@viewBird']);
//
//Route::get('videos', ['as' => 'videos', 'uses' => 'Front\MediaController@videos']);
//Route::get('videos/{link}', ['as' => 'videoPage', 'uses' => 'Front\MediaController@videoDetail']);
//Route::get('albums', ['as' => 'albums', 'uses' => 'Front\MediaController@albums']);
//Route::get('albums/{slug}', ['as' => 'albumPhotos', 'uses' => 'Front\MediaController@photos']);
//


Route::controllers(['auth' => 'Auth\AuthController', 'password' => 'Auth\PasswordController',]);

Route::group(['prefix' => 'admin'], function () {
    Route::resource('cities-categories', 'Cities\CitiesCategoryController');
    Route::any('cities-categories/{id}/listed-birds/multi-update', ['as' => 'admin.cities-categories.{id}.listed-birds.multi-update', 'uses' => 'Cities\CitiesListedBirdsController@multiUpdate']);
    Route::get('cities-categories/{id}/listed-birds/multi-edit', ['as' => 'admin.cities-categories.{id}.listed-birds.multi-edit', 'uses' => 'Cities\CitiesListedBirdsController@multiEdit']);
    Route::delete('cities-categories/{id}/listed-birds/delete/{listId}/{multiEdit}', ['as' => 'admin.cities-categories.{id}.listed-birds.multi-edit.delete', 'uses' => 'Cities\CitiesListedBirdsController@destroy']);
    Route::resource('cities-categories/{id}/listed-birds', 'Cities\CitiesListedBirdsController');

    Route::resource('birds', 'Bird\BirdController');
    Route::post('birds/addPhoto/{id}', ['as' => 'admin.birds.storePhoto', 'uses' => 'Bird\BirdController@addPhoto']);

    Route::resource('pages', 'Page\PageController');
    Route::post('pages/addPhoto/{id}', ['as' => 'admin.pages.storePhoto', 'uses' => 'Page\PageController@addPhoto']);


    Route::resource('albums', 'AlbumController');
    Route::resource('albums/{id}/galleries', 'GalleryController');
    Route::any('albums/{id}/upload', ['as' => 'admin.albums.{id}.upload', 'uses' => 'GalleryController@upload']);

    Route::resource('page-contents', 'Page\PageContentController');

    Route::resource('video-categories', 'VideoCategoryController');
    Route::resource('video-categories/{id}/videos', 'VideoController');
});