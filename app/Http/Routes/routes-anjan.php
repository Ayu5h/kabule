<?php
/**
 * Created by PhpStorm.
 * User: anjan
 * Date: 12/27/2015
 * Time: 2:40 PM
 */



/*********** Committee Member Route *************/
Route::resource('admin/committee','CommitteeController');

Route::resource('admin/committee/{committeeId}/member', 'MemberController');

Route::post('add-member-photo/{memberId}', ['as' => 'addMemberPhoto', 'uses' => 'MemberController@addMemberPhoto']);





/************************* Banner Route *****************************/
Route::resource('banner','BannerController');

Route::post('add-banner-image/{bannerImageId}',['as' => 'addBannerImage', 'uses' => 'BannerController@addBannerImage']);

/************* Partner Route ****************/

/*************** Front Publication Route ***************/
Route::get('publications', ['as' => 'index', 'uses' => 'Front\PublicationController@index']);
Route::get('publication-detail/{publicationId}', ['as' => 'publicationDetail', 'uses' => 'Front\PublicationController@publicationDetail']);


Route::get('all-project-news', ['as' => 'allProjectNews', 'uses' => 'Front\ProjectController@allProjectNews']);