<?php

//Route::group(['prefix' => 'admin'], function()
//{
//    Route::resource('project', 'ProgramController');
//});

Route::resource('admin/project', 'ProgramController');
Route::post('program/addPhoto/{id}', ['as' => 'store_program_photo', 'uses' => 'ProgramController@addPhoto']);

Route::resource('admin/disaster', 'DisasterController');
Route::post('disaster/addPhoto/{id}', ['as' => 'store_disaster_photo', 'uses' => 'DisasterController@addPhoto']);

Route::resource('admin/getinvolve', 'GetinvolveController');
Route::post('getinvolve/addPhoto/{id}', ['as' => 'store_getinvolve_photo', 'uses' => 'GetinvolveController@addPhoto']);

Route::resource('admin/whatWeDo', 'WhatWeDoController');
Route::post('whatWeDo/addPhoto/{id}', ['as' => 'store_whatWeDo_photo', 'uses' => 'WhatWeDoController@addPhoto']);

Route::resource('admin/impact', 'ImpactController');
Route::post('impact/addPhoto/{id}', ['as' => 'store_impact_photo', 'uses' => 'ImpactController@addPhoto']);

Route::resource('admin/partner', 'PartnerController');
Route::post('partner/addPhoto/{id}', ['as' => 'store_partner_photo', 'uses' => 'PartnerController@addPhoto']);


Route::resource('admin/project/{id}/news', 'NewsController');
Route::post('news/addPhoto/{id}', ['as' => 'news.addPhoto', 'uses' => 'NewsController@addPhoto']);



Route::resource('admin/project/{id}/topic', 'TopicController');
Route::post('topic/addPhoto/{id}', ['as' => 'topic.addPhoto', 'uses' => 'TopicController@addPhoto']);



Route::resource('admin/project/{id}/activity', 'ActivityController');
Route::post('activity/addPhoto/{id}', ['as' => 'activity.addPhoto', 'uses' => 'ActivityController@addPhoto']);


Route::resource('media', 'MediaController');
Route::get('media/download/{id}', ['as'=> 'media.download', 'uses' => 'MediaController@download']);

Route::get('/home', function() {
    return 'Welcome! This is just a temporary home page! Please '.link_to('/auth/login', 'Login') . ' to continue.';
});




Route::get('admin/contact-settings', ['as' => 'contactSettingsEdit', 'uses' => 'ContactController@edit']);

Route::patch('admin/contact-settings', ['as' => 'contactSettingsUpdate', 'uses' => 'ContactController@update']);


//Route::resource('admin/contact-messages', 'ContactMessageController');
Route::get('admin/contact-messages', ['as' => 'contactMessagesIndex', 'uses' => 'ContactMessageController@index']);
Route::get('admin/contact-messages/{id}', ['as' => 'contactMessagesShow', 'uses' => 'contactMessageController@show']);
Route::delete('admin/contact-messages/{id}', ['as' => 'contactMessagesDelete', 'uses' => 'contactMessageController@destroy']);


Route::resource('admin/news', 'SiteNewsController');
Route::post('admin/news/addPhoto/{id}', ['as' => 'admin.news.addPhoto', 'uses' => 'SiteNewsController@addPhoto']);


Route::resource('admin/event', 'EventController');
Route::post('admin/event/addPhoto/{id}', ['as' => 'admin.event.addPhoto', 'uses' => 'EventController@addPhoto']);

Route::resource('admin/newsletter', 'NewsletterController');
Route::post('admin/newsletter/{id}', ['as' => 'admin.newsletter.addPhoto', 'uses' => 'NewsletterController@addPhoto']);

//Route::resource('front/test', 'Front\TestController');
Route::get('events', 'Front\EventController@index');
Route::get('events/{slug}', 'Front\EventController@show');

//Site News
Route::get('news', 'Front\SiteNewsController@index');
Route::get('news/{slug}', 'Front\SiteNewsController@show');

//contact
//Route::get('contact', 'Front\ContactController@index');
//Route::post('contact', ['as' => 'storeContactMessage', 'uses' => 'Front\ContactController@store']);
//

Route::get('link', 'Front\LinkController@index');


Route::resource('admin/link', 'LinkController');
Route::post('admin/link/addPhoto/{id}', ['as' => 'admin.link.addPhoto', 'uses' => 'LinkController@addPhoto']);


Route::get('auth/reset', ['as' => 'getResetPassword', 'uses' => 'PasswordResetController@getResetPassword']);
Route::post('auth/reset', ['as' => 'postResetPassword', 'uses' => 'PasswordResetController@postResetPassword']);


Route::resource('admin/notice', 'NoticeController');

Route::post('admin/page-contents/{id}/addPhoto', ['as' => 'pageContent.addPhoto', 'uses' => 'Page\PageContentController@addPhoto']);


Route::resource('admin/publication', 'PublicationController');
//Route::get('admin/publication', '')


Route::post('admin/subPublication/{id}/addPhoto', ['as' => 'admin.subPublication.addPhoto', 'uses' => 'SubPublicationController@addPhoto']);

Route::get('admin/publication/{publicationSlug}/create',['as' => 'createSubPublication', 'uses' => 'SubPublicationController@create']);
Route::post('admin/publication/{publicationSlug}', 'SubPublicationController@store');

Route::get('admin/publication/{publicationSlug}/{subPublicationSlug}/edit', ['as' => 'editSubPublication', 'uses' => 'SubPublicationController@edit']);
Route::patch('admin/publication/{publicationSlug}/{subPublicationSlug}', 'SubPublicationController@update');

Route::delete('admin/publication/{publicationSlug}/{subPublicationSlug}', 'SubPublicationController@destroy');



Route::get('programs', ['as' => 'programs', 'uses' => 'Front\ProgramController@index']);
Route::get('programs/{program}', ['as' => 'viewProgram', 'uses' => 'Front\ProgramController@viewProgram']);
Route::get('programs/{program}/news/{news}', ['as' => 'programNews', 'uses' => 'Front\ProgramController@programNews']);
Route::get('programs/{program}/activity/{activity}', ['as' => 'programActivity', 'uses' => 'Front\ProgramController@programActivity']);



//Route::get('publications/{publicationSlug}', ['as' => 'viewPublications', 'uses' => 'Front\PublicationController@show']);
//Route::get('publications/{publicationSlug}/{subPublicationSlug}', ['as' => 'viewSubPublication', 'uses' => 'Front\PublicationController@viewSubPublication']);


Route::get('notices', ['as' => 'notices.index', 'uses' => 'Front\NoticeController@index']);
Route::get('notices/{slug}', ['as' => 'notices.show', 'uses' => 'Front\NoticeController@show']);


Route::get('links', ['as' => 'links.index', 'uses' => 'Front\LinkController@index']);


Route::post('admin/contact/addPhoto/{id}', ['as' => 'admin.contact.addPhoto', 'uses' => 'ContactController@addPhoto']);

