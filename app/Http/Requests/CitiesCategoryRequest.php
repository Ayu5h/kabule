<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class CitiesCategoryRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
        //write custom if needed for authentication
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'name' => 'required',
            'description' => 'required'
		];
	}

}
