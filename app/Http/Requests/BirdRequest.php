<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class BirdRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'name' => 'required',
            'kingdom' => 'required',
            'image' => '',
            'phylum' => 'required',
            'class' => 'required',
            'order' => 'required',
            'family' => 'required',
            'genus' => 'required',
            'species' => 'required',
            'sub_species' => 'required',
            'synonym' => '',
            'local_name' => 'required',
            'distribution' => 'required',
            'short_description' => 'required',
            'description' => 'required'
		];
	}

}
