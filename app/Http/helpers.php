<?php

function delete_form($url, $label = 'Delete') {
    $form = Form::open(['method' => 'DELETE', 'url' => $url, 'class' => 'deleteContentForm']);
    $form .= Form::submit($label, ['class' => 'deleteContent']);
    return $form .= Form::close();
}

function deleteButton($url, $label = 'Delete', $class = 'btn-red') {
    $form = Form::open(['method' => 'DELETE', 'url' => $url]);
    $form .= '<button class="btn btn-danger btn-sm" type="submit">' . '<i class="fa fa-aw fa-trash-o"></i> ' . $label . '</button>';
    return $form .= Form::close();
}

if(!function_exists('display_image'))
{
    function display_image($path, $width, $height) {

        if($path == '' || !file_exists($path))
            return sprintf('http://placehold.it/%sx%s', $width, $height);
        else
            return asset($path);

    }

}