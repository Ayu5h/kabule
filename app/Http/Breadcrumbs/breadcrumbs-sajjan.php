<?php
/********** Project Breadcrumb ************/

Breadcrumbs::register('admin.project.index', function($breadcrumbs)
{
    $breadcrumbs->push('Project', route('admin.project.index'));
});


Breadcrumbs::register('admin.project.create', function($breadcrumbs)
{
    $breadcrumbs->parent('admin.project.index');
    $breadcrumbs->push('Create', route('admin.project.create'));
});

Breadcrumbs::register('admin.project.edit', function($breadcrumbs, $projectId)
{
    //    $breadcrumbs->parent('admin.project.show', $projectId);
    $breadcrumbs->parent('admin.project.index');
    $breadcrumbs->push('Edit', route('admin.project.edit', $projectId));
});

//Breadcrumbs::register('admin.project.show', function($breadcrumbs, $projectId)
//{
//    $project = \App\Program::findOrFail($projectId);
//    $breadcrumbs->parent('admin.project.index');
//    $breadcrumbs->push($project->title, route('admin.project.show', $projectId));
//});

/********** Disaster Breadcrumb ************/

Breadcrumbs::register('admin.disaster.index', function($breadcrumbs)
{
    $breadcrumbs->push('Disaster', route('admin.disaster.index'));
});


Breadcrumbs::register('admin.disaster.create', function($breadcrumbs)
{
    $breadcrumbs->parent('admin.disaster.index');
    $breadcrumbs->push('Create', route('admin.disaster.create'));
});

Breadcrumbs::register('admin.disaster.edit', function($breadcrumbs, $disasterId)
{
    //    $breadcrumbs->parent('admin.disaster.show', $disasterId);
    $breadcrumbs->parent('admin.disaster.index');
    $breadcrumbs->push('Edit', route('admin.disaster.edit', $disasterId));
});


/********** Getinvolve Breadcrumb ************/

Breadcrumbs::register('admin.getinvolve.index', function($breadcrumbs)
{
    $breadcrumbs->push('Get Involve', route('admin.getinvolve.index'));
});


Breadcrumbs::register('admin.getinvolve.create', function($breadcrumbs)
{
    $breadcrumbs->parent('admin.getinvolve.index');
    $breadcrumbs->push('Create', route('admin.getinvolve.create'));
});

Breadcrumbs::register('admin.getinvolve.edit', function($breadcrumbs, $getinvolveId)
{
    //    $breadcrumbs->parent('admin.getinvolve.show', $getinvolveId);
    $breadcrumbs->parent('admin.getinvolve.index');
    $breadcrumbs->push('Edit', route('admin.getinvolve.edit', $getinvolveId));
});

/********** What We Do Breadcrumb ************/

Breadcrumbs::register('admin.whatWeDo.index', function($breadcrumbs)
{
    $breadcrumbs->push('What We Do', route('admin.whatWeDo.index'));
});


Breadcrumbs::register('admin.whatWeDo.create', function($breadcrumbs)
{
    $breadcrumbs->parent('admin.whatWeDo.index');
    $breadcrumbs->push('Create', route('admin.whatWeDo.create'));
});

Breadcrumbs::register('admin.whatWeDo.edit', function($breadcrumbs, $whatWeDoId)
{
    //    $breadcrumbs->parent('admin.whatWeDo.show', $whatWeDoId);
    $breadcrumbs->parent('admin.whatWeDo.index');
    $breadcrumbs->push('Edit', route('admin.whatWeDo.edit', $whatWeDoId));
});

/********** Partner Breadcrumb ************/

Breadcrumbs::register('admin.partner.index', function($breadcrumbs)
{
    $breadcrumbs->push('Partner', route('admin.partner.index'));
});


Breadcrumbs::register('admin.partner.create', function($breadcrumbs)
{
    $breadcrumbs->parent('admin.partner.index');
    $breadcrumbs->push('Create', route('admin.partner.create'));
});

Breadcrumbs::register('admin.partner.edit', function($breadcrumbs, $partnerId)
{
    //    $breadcrumbs->parent('admin.partner.show', $partnerId);
    $breadcrumbs->parent('admin.partner.index');
    $breadcrumbs->push('Edit', route('admin.partner.edit', $partnerId));
});


/********** News Breadcrumb ************/

Breadcrumbs::register('admin.project.{id}.news.index', function($breadcrumbs, $projectId)
{
    $breadcrumbs->parent('admin.project.index');
    $breadcrumbs->push('News', route('admin.project.{id}.news.index', [$projectId]));
});

Breadcrumbs::register('admin.project.{id}.news.create', function($breadcrumbs,  $projectId)
{
    $breadcrumbs->parent('admin.project.index');
    $breadcrumbs->push('News', route('admin.project.{id}.news.index', $projectId));
    $breadcrumbs->push('Create', route('admin.project.{id}.news.create', [$projectId]));
});

Breadcrumbs::register('admin.project.{id}.news.edit', function($breadcrumbs,  $projectId, $newsId)
{
    $breadcrumbs->parent('admin.project.index');
    $breadcrumbs->push('News', route('admin.project.{id}.news.index', $projectId));
    $breadcrumbs->push('Edit', route('admin.project.{id}.news.edit', [$projectId, $newsId]));
});


/********** Topic Breadcrumb ************/

Breadcrumbs::register('admin.project.{id}.topic.index', function($breadcrumbs, $projectId)
{
    $breadcrumbs->parent('admin.project.index');
    $breadcrumbs->push('Topic', route('admin.project.{id}.topic.index', [$projectId]));
});

Breadcrumbs::register('admin.project.{id}.topic.create', function($breadcrumbs,  $projectId)
{
    $breadcrumbs->parent('admin.project.index');
    $breadcrumbs->push('Topic', route('admin.project.{id}.topic.index', $projectId));
    $breadcrumbs->push('Create', route('admin.project.{id}.topic.create', [$projectId]));
});

Breadcrumbs::register('admin.project.{id}.topic.edit', function($breadcrumbs,  $projectId, $topicId)
{
    $breadcrumbs->parent('admin.project.index');
    $breadcrumbs->push('Topic', route('admin.project.{id}.topic.index', $projectId));
    $breadcrumbs->push('Edit', route('admin.project.{id}.topic.edit', [$projectId, $topicId]));
});


/********** Activity Breadcrumb ************/

Breadcrumbs::register('admin.project.{id}.activity.index', function($breadcrumbs, $projectId)
{
    $breadcrumbs->parent('admin.project.index');
    $breadcrumbs->push('Activity', route('admin.project.{id}.activity.index', [$projectId]));
});

Breadcrumbs::register('admin.project.{id}.activity.create', function($breadcrumbs,  $projectId)
{
    $breadcrumbs->parent('admin.project.index');
    $breadcrumbs->push('Activity', route('admin.project.{id}.activity.index', $projectId));
    $breadcrumbs->push('Create', route('admin.project.{id}.activity.create', [$projectId]));
});

Breadcrumbs::register('admin.project.{id}.activity.edit', function($breadcrumbs,  $projectId, $activityId)
{
    $breadcrumbs->parent('admin.project.index');
    $breadcrumbs->push('Activity', route('admin.project.{id}.activity.index', $projectId));
    $breadcrumbs->push('Edit', route('admin.project.{id}.activity.edit', [$projectId, $activityId]));
});


/********** Committee Member Breadcrumb ************/

Breadcrumbs::register('admin.committee.{committeeId}.member.index', function($breadcrumbs, $committeeId)
{
    $breadcrumbs->parent('admin.committee.index');
    $breadcrumbs->push('Member', route('admin.committee.{committeeId}.member.index', [$committeeId]));
});

Breadcrumbs::register('admin.committee.{committeeId}.member.create', function($breadcrumbs,  $committeeId)
{
    $breadcrumbs->parent('admin.committee.index');
    $breadcrumbs->push('Member', route('admin.committee.{committeeId}.member.index', $committeeId));
    $breadcrumbs->push('Create', route('admin.committee.{committeeId}.member.create', [$committeeId]));
});

Breadcrumbs::register('admin.committee.{committeeId}.member.edit', function($breadcrumbs,  $committeeId, $memberId)
{
    $breadcrumbs->parent('admin.committee.index');
    $breadcrumbs->push('Member', route('admin.committee.{committeeId}.member.index', $committeeId));
    $breadcrumbs->push('Edit', route('admin.committee.{committeeId}.member.edit', [$committeeId, $memberId]));
});


//Contacts BreadCrumb

Breadcrumbs::register('contactSettingsEdit', function($breadcrumbs)
{
    $breadcrumbs->push('Contact Settings', route('contactSettingsEdit'));
});

Breadcrumbs::register('contactMessagesIndex', function($breadcrumbs)
{
    $breadcrumbs->push('Contact Settings', route('contactMessagesIndex'));
});

//Pages BreadCrumb


Breadcrumbs::register('admin.pages.edit', function($breadcrumbs, $page)
{
    $breadcrumbs->push('Pages', '#');
    $pageName = ucfirst(str_replace('-', ' ', $page));
    $breadcrumbs->push($pageName, route('admin.pages.edit', [$page]));
});


//Cities Categories

Breadcrumbs::register('admin.cities-categories.index', function($breadcrumbs)
{
    $breadcrumbs->push('Cities Categories', route('admin.cities-categories.index'));
});

Breadcrumbs::register('admin.cities-categories.create', function($breadcrumbs)
{
    $breadcrumbs->parent('admin.cities-categories.index');
    $breadcrumbs->push('Create', route('admin.cities-categories.create'));
});

Breadcrumbs::register('admin.cities-categories.edit', function($breadcrumbs)
{
    $breadcrumbs->parent('admin.cities-categories.index');
    $breadcrumbs->push('Edit', route('admin.cities-categories.edit'));
});


Breadcrumbs::register('admin.cities-categories.{id}.listed-birds.index', function($breadcrumbs, $categoryId)
{
    $breadcrumbs->parent('admin.cities-categories.index');
    $breadcrumbs->push('Listed Birds', route('admin.cities-categories.{id}.listed-birds.index', [$categoryId]));
});

Breadcrumbs::register('admin.cities-categories.{id}.listed-birds.create', function($breadcrumbs, $categoryId)
{
    $breadcrumbs->parent('admin.cities-categories.{id}.listed-birds.index', $categoryId);
    $breadcrumbs->push('Create', route('admin.cities-categories.{id}.listed-birds.create', [$categoryId]));
});

Breadcrumbs::register('admin.cities-categories.{id}.listed-birds.edit', function($breadcrumbs, $categoryId, $birdId)
{
    $breadcrumbs->parent('admin.cities-categories.{id}.listed-birds.index', $categoryId);
    $breadcrumbs->push('Edit', route('admin.cities-categories.{id}.listed-birds.edit', [$categoryId, $birdId]));
});

Breadcrumbs::register('admin.cities-categories.{id}.listed-birds.create', function($breadcrumbs, $categoryId)
{
    $breadcrumbs->parent('admin.cities-categories.{id}.listed-birds.index', $categoryId);
    $breadcrumbs->push('Create', route('admin.cities-categories.{id}.listed-birds.create', [$categoryId]));
});

Breadcrumbs::register('admin.cities-categories.{id}.listed-birds.multi-edit', function($breadcrumbs, $categoryId)
{
    $breadcrumbs->parent('admin.cities-categories.{id}.listed-birds.index', $categoryId);
    $breadcrumbs->push('Multi Edit', route('admin.cities-categories.{id}.listed-birds.multi-edit', [$categoryId]));
});

//Notice Breadcrumbs

Breadcrumbs::register('admin.notice.index', function($breadcrumbs)
{
    $breadcrumbs->push('Notice', route('admin.notice.index'));
});


Breadcrumbs::register('admin.notice.create', function($breadcrumbs)
{
    $breadcrumbs->parent('admin.notice.index');
    $breadcrumbs->push('Create', route('admin.notice.create'));
});

Breadcrumbs::register('admin.notice.edit', function($breadcrumbs, $birdId)
{
    $breadcrumbs->parent('admin.notice.index');
    $breadcrumbs->push('Edit', route('admin.notice.edit', $birdId));
});


//Publication Breadcrumbs

Breadcrumbs::register('admin.publication.index', function($breadcrumbs)
{
    $breadcrumbs->push('Resources', route('admin.publication.index'));
});


Breadcrumbs::register('admin.publication.create', function($breadcrumbs)
{
    $breadcrumbs->parent('admin.publication.index');
    $breadcrumbs->push('Create', route('admin.publication.create'));
});

Breadcrumbs::register('admin.publication.edit', function($breadcrumbs, $publicationSlug)
{
    $breadcrumbs->parent('admin.publication.index');
    $breadcrumbs->push('Edit', route('admin.publication.edit', $publicationSlug));
});


//SubPublication BreadCrumbs
Breadcrumbs::register('admin.publication.show', function($breadcrumbs, $publicationSlug)
{
    $breadcrumbs->parent('admin.publication.index');
    $publication = \App\Publication::findBySlugOrFail($publicationSlug);
    $breadcrumbs->push("$publication->category", route('admin.publication.show', $publicationSlug));
});


Breadcrumbs::register('createSubPublication', function($breadcrumbs, $publicationSlug)
{
    $breadcrumbs->parent('admin.publication.show', $publicationSlug);
    $breadcrumbs->push("Create", route('createSubPublication', $publicationSlug));
});


Breadcrumbs::register('createSubPublication', function($breadcrumbs, $publicationSlug)
{
    $breadcrumbs->parent('admin.publication.show', $publicationSlug);
    $breadcrumbs->push("Create", route('createSubPublication', $publicationSlug));
});

Breadcrumbs::register('editSubPublication', function($breadcrumbs, $publicationSlug)
{
    $breadcrumbs->parent('admin.publication.show', $publicationSlug);
    $breadcrumbs->push("Edit", route('editSubPublication', $publicationSlug));
});