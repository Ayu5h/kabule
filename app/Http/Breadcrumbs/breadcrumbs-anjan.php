<?php


/********* Banner Breadcrumb ************/
Breadcrumbs::register('banner', function($breadcrumbs)
{
    $breadcrumbs->push('Manage Banner', route('banner.index'));
});

Breadcrumbs::register('create-banner', function($breadcrumbs)
{
    $breadcrumbs->parent('banner');
    $breadcrumbs->push('Create Banner');
});

Breadcrumbs::register('edit-banner', function($breadcrumbs)
{
    $breadcrumbs->parent('banner');
    $breadcrumbs->push('Edit Banner');
});

/************* Partner Breadcrumb **************/


Breadcrumbs::register('partner', function($breadcrumbs)
{
    $breadcrumbs->push('Manage Partner', route('partner.index'));
});

Breadcrumbs::register('create-partner', function($breadcrumbs){
    $breadcrumbs->parent('partner');
    $breadcrumbs->push('Create Partner');
});

Breadcrumbs::register('edit-partner', function($breadcrumbs){
    $breadcrumbs->parent('partner');
    $breadcrumbs->push('Edit Partner');
});
/******** Newsletter *************/

Breadcrumbs::register('news-letter', function($breadcrumbs){
    $breadcrumbs->push('Manage Newsletter', route('admin.newsletter.index'));
});

Breadcrumbs::register('create-newsletter', function($breadcrumbs){
    $breadcrumbs->parent('news-letter');
    $breadcrumbs->push('Create Newsletter');
});

Breadcrumbs::register('edit-newsletter', function($breadcrumbs){
    $breadcrumbs->parent('news-letter');
    $breadcrumbs->push('Edit Newsletter');
});



/************* Publication Breadcrumb *************/

Breadcrumbs::register('publication', function($breadcrumbs){
    $breadcrumbs->push('Manage Publication', route('admin.publication.index'));
});

Breadcrumbs::register('create-publication', function($breadcrumbs){
    $breadcrumbs->parent('publication');
    $breadcrumbs->push('Create Publication');
});

Breadcrumbs::register('edit-publication', function($breadcrumbs){
    $breadcrumbs->parent('publication');
    $breadcrumbs->push('Edit Publication');
});

/*************** Merchandise Breadcrumb ****************/

Breadcrumbs::register('merchandise', function($breadcrumbs){
    $breadcrumbs->push('Manage Merchandise', route('admin.merchandise.index'));
});

Breadcrumbs::register('create-merchandise', function($breadcrumbs){
    $breadcrumbs->parent('merchandise');
    $breadcrumbs->push('Create Merchandise');
});

Breadcrumbs::register('edit-merchandise', function($breadcrumbs){
    $breadcrumbs->parent('merchandise');
    $breadcrumbs->push('Edit Merchandise');
});



/********* Donation Program Breadcrumb *************/
Breadcrumbs::register('donation-program', function($breadcrumbs){
    $breadcrumbs->push('Donation Program', action('DonationProgramController@index'));
});

Breadcrumbs::register('create-donation-program', function($breadcrumbs){
    $breadcrumbs->parent('donation-program');
    $breadcrumbs->push('Create New Donation Program');
});
Breadcrumbs::register('edit-donation-program', function($breadcrumbs, $donationProgram){
    $breadcrumbs->parent('donation-program');
    $breadcrumbs->push('Edit'.' '.$donationProgram->name);
});

/*********** News Breadcrumb **************/
Breadcrumbs::register('admin.news.index', function($breadcrumbs){
    $breadcrumbs->push('Manage News', route('admin.news.index'));
});

Breadcrumbs::register('admin.news.create', function($breadcrumbs){
   $breadcrumbs->parent('admin.news.index');
    $breadcrumbs->push('Create News');
});

Breadcrumbs::register('admin.news.edit', function($breadcrumbs){
    $breadcrumbs->parent('admin.news.index');
    $breadcrumbs->push('Edit News');
});

/************* Events Breadcrumb *************/
Breadcrumbs::register('admin.event.index', function($breadcrumbs){
   $breadcrumbs->push('Manage Events', route('admin.event.index'));
});

Breadcrumbs::register('admin.event.create', function($breadcrumbs){
   $breadcrumbs->parent('admin.event.index');
    $breadcrumbs->push('Create Events');
});

Breadcrumbs::register('admin.event.edit', function($breadcrumbs){
    $breadcrumbs->parent('admin.event.index');
    $breadcrumbs->push('Edit Events');
});

/************ Advertisement Breadcrumb ************/
Breadcrumbs::register('admin.advertisement.index', function($breadcrumbs){
    $breadcrumbs->push('Manage Advertisement');
});

Breadcrumbs::register('admin.advertisement.create', function($breadcrumbs){
    $breadcrumbs->parent('admin.advertisement.index');
    $breadcrumbs->push('Create Advertisement');
});

Breadcrumbs::register('admin.advertisement.edit', function($breadcrumbs){
    $breadcrumbs->parent('admin.advertisement.index');
    $breadcrumbs->push('Edit Advertisement');
});

/************ Link Breadcrumb *************/
Breadcrumbs::register('admin.link.index', function($breadcrumbs){
    $breadcrumbs->push('Link', route('admin.link.index'));
});

Breadcrumbs::register('admin.link.create', function($breadcrumbs){
    $breadcrumbs->parent('admin.link.index');
    $breadcrumbs->push('Create Link');
});

Breadcrumbs::register('admin.link.edit', function($breadcrumbs){
    $breadcrumbs->parent('admin.link.index');
    $breadcrumbs->push('Edit Link');
});

/************** Committee Breadcrumb **********/
Breadcrumbs::register('admin.committee.index', function($breadcrumbs){
    $breadcrumbs->push('Committee', route('admin.committee.index'));
});

Breadcrumbs::register('admin.committee.create', function($breadcrumbs){
    $breadcrumbs->parent('admin.committee.index');
    $breadcrumbs->push('Create Committee');
});

Breadcrumbs::register('admin.committee.edit', function($breadcrumbs){
    $breadcrumbs->parent('admin.committee.index');
    $breadcrumbs->push('Edit Committee');
});