<?php namespace App\Http\Controllers;

use App\Album;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class AlbumController extends AdminController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$albums = Album::all();

        return view('admin.media.albums.index', compact('albums'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('admin.media.albums.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
        $this->validate($request, [
            'name' => 'required',
            'created_date' => 'required'
        ]);

        Album::create($request->all());

        flash()->success('Album has been created.');

        return Redirect::Route('admin.albums.index');
	}
	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $album = Album::findOrFail($id);
        return view('admin.media.albums.edit', compact('album'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, Request $request)
	{
//        $this->validate($request, [
//            'name' => 'required'
//        ]);

        $album = Album::findOrFail($id);
        $album->update($request->all());

        flash()->success("Album has been updated.");
        return Redirect::route('admin.albums.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$album =  Album::findOrFail($id);
		$album->photos()->delete();
		
		$album->delete();

                return Redirect::route('admin.albums.index');
	}

}
