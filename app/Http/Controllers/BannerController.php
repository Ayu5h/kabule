<?php namespace App\Http\Controllers;

use App\Media;
use App\Banner;
use App\Http\Requests\BannerRequest;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
class BannerController extends AdminController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $breadcrumb = 'banner';
		$bannerImages = Banner::get();
        return view('admin.banner.manage-banner-images',compact('bannerImages','breadcrumb'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        $breadcrumb = 'create-banner';
		return view('admin.banner.create-banner-image',compact('breadcrumb'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(BannerRequest $request)
	{
		$bannerImages = Banner::create($request->all());

        flash()->success('Your Banner has been created');

        return Redirect::route('banner.edit',[$bannerImages->id]);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$bannerImage = Banner::findOrFail($id);
        $breadcrumb = 'edit-banner';
        return view('admin.banner.edit-banner-image',compact('bannerImage','breadcrumb'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, BannerRequest $request)
	{
        $bannerImage = Banner::findOrFail($id);
        $bannerImage->update($request->all());

        flash()->success('Your Banner has been updated');
        return redirect('banner');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$bannerImage = Banner::findOrFail($id);
        $bannerImage->delete();

        flash()->success('Your Banner has been deleted');
        return redirect('banner');
	}

    public function addBannerImage($bannerImageId, Request $request){
        $this->validate($request, ['photo' => 'sometimes|mimes:jpg,jpeg,png,bmp']);
        $bannerImage = Banner::findorFail($bannerImageId);

        foreach($bannerImage->photos as $photo) {
            $photo->delete();
        }


        $file = $request->file('photo');

        $photo = Media::named($file->getClientOriginalName())->move($file, 200, 200)->resizeImage(620, 310);
        $bannerImage->image = $photo->path;
        $bannerImage->image_thumbnail = $photo->thumbnail_path;
        $bannerImage->save();

        return $bannerImage->photos()->save($photo);
    }

}
