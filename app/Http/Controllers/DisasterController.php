<?php namespace App\Http\Controllers;

use App\Disaster;
use App\Media;
use App\Http\Requests\DisasterRequest;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class DisasterController extends AdminController
{



    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $disasters = Disaster::All();

        return view('admin.disaster.index', compact('disasters'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {

        return view('admin.disaster.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(DisasterRequest $request)
    {

        $disaster_id = Disaster::create($request->all());

        flash()->success('Your Disaster Response has been created');

        return Redirect::route('admin.disaster.edit', [$disaster_id]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $disaster = Disaster::findOrFail($id);

        return view('admin.disaster.edit', compact('disaster', 'breadcrumb'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id, DisasterRequest $request)
    {
        $disaster = Disaster::findorFail($id);

        $disaster->update($request->all());

        flash()->success('Your Disaster Response has been updated');

        return redirect('admin/disaster');
    }

    public function addPhoto($disaster_id, Request $request)
    {
        $this->validate($request, [
            'photo' => 'required|mimes:jpg,jpeg,png,bmp'
        ]);

        $disaster = Disaster::findorFail($disaster_id);

        $file = $request->file('photo');
        //85, 63
        $photo = Media::named($file->getClientOriginalName())->move($file, 192, 145)->resizeImage(620, 310);

        return $disaster->photos()->save($photo);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        Disaster::findOrFail($id)->delete();

        flash()->success('Your Disaster Response has been deleted');

        return redirect()->back();
    }

}
