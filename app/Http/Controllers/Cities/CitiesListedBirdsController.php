<?php namespace App\Http\Controllers\Cities;

use App\Http\Controllers\AdminController;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\CitiesBird;
use App\Models\CitiesCategory;
use App\Models\CitiesListedBird;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class CitiesListedBirdsController extends AdminController
{

    function __construct()
    {
        parent::__construct();
        $this->module = 'cities';
        $this->currentPage = 'cities-category';
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index($categoryId)
    {
        $category = CitiesCategory::findOrFail($categoryId);
        $listedBirds = $category->listedBirds;
        $this->title = 'Cities Listed Birds for ' . $category->name;

        $this->_shareViews();

        return view('admin.cities.listed-birds.index', compact('category', 'listedBirds'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create($categoryId)
    {
        $category = CitiesCategory::findorFail($categoryId);
        $this->title = 'Add Listed Birds for ' . $category->name;

        $this->_shareViews();

        return view('admin.cities.listed-birds.create', compact('category'));
    }

    /**
     * @param $categoryId
     * @param Request $request
     * @return mixed
     */
    public function store($categoryId, Request $request)
    {

        $listedBirds = [];
        $category = CitiesCategory::findOrFail($categoryId);

        $names = $request->input('name');
        $appendixes = $request->input('appendix');

        foreach ($names as $k => $nm) {
            if (!empty($nm) && !empty($appendixes[$k])) {
                flash()->success('Listed birds has been added.');
                $listedBirds[] = new CitiesListedBird(['name' => $nm, 'appendix' => $appendixes[$k]]);
            } else if (empty($nm) || empty($appendixes[$k])) {
                flash()->warning('Some of the names or appendix were missing, so those records were ignored');
            }
        }

        $category->listedBirds()->saveMany($listedBirds);

        return Redirect::Route('admin.cities-categories.{id}.listed-birds.index', [$categoryId]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($categoryId, $id)
    {
        $category = CitiesCategory::findOrFail($categoryId);

        $listedBird = CitiesListedBird::find($id);
        $this->title = 'Edit Listed Bird ' . $listedBird->name . ' Of Category ' . $category->name;

        $this->_shareViews();

        return view('admin.cities.listed-birds.edit', compact('category', 'listedBird'));

    }

    /**
     * @param $categoryId
     * @return \Illuminate\View\View
     */
    function multiEdit($categoryId)
    {
        $category = CitiesCategory::findOrFail($categoryId);
        $title = 'Edit Listed Birds for ' . $category->name;
        $listedBirds = $category->listedBirds;

        return view('admin.cities.listed-birds.multi-edit', compact('category', 'listedBirds', 'title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($categoryId, $id, Requests\CitiesListedBirdRequest $request)
    {

        $category = CitiesCategory::findorFail($categoryId);

        $listedBird = CitiesListedBird::findOrFail($id);
        $listedBird->update($request->all());

        flash()->success('Listed birds updated.');
        return Redirect::Route('admin.cities-categories.{id}.listed-birds.index', [$categoryId]);
    }


    /**
     * @param $categoryId
     * @param Request $request
     * @return mixed
     */
    public function multiUpdate($categoryId, Request $request)
    {

        $listedBirds = [];
        $category = CitiesCategory::findOrFail($categoryId);

        $category->listedBirds()->delete();

        $names = $request->input('name');
        $appendixes = $request->input('appendix');

        foreach ($names as $k => $nm) {
            if (!empty($nm) && !empty($appendixes[$k])) {
                flash()->success('Listed birds updated.');
                $listedBirds[] = new CitiesListedBird(['name' => $nm, 'appendix' => $appendixes[$k]]);
            } else if (empty($nm) || empty($appendixes[$k])) {
                flash()->warning('Some of the names or appendix were missing, so those records were ignored')->important();
            }
        }

        $category->listedBirds()->saveMany($listedBirds);

        return Redirect::Route('admin.cities-categories.{id}.listed-birds.index', $categoryId);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($categoryId, $id, $multiEdit = false)
    {
        $citiesCategory = CitiesCategory::findOrFail($categoryId);
        $listedBird = CitiesListedBird::findOrFail($id);
        $listedBird->delete();

        flash()->success('Listed birds deleted.');

        if ($multiEdit) {
            return Redirect::Route('admin.cities-categories.{id}.listed-birds.multi-edit', $categoryId);
        } else {
            return Redirect::Route('admin.cities-categories.{id}.listed-birds.index', $categoryId);
        }
    }

}
