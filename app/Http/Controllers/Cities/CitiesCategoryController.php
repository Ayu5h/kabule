<?php namespace App\Http\Controllers\Cities;

use App\Http\Controllers\AdminController;
use App\Http\Controllers\Controller;

use App\Http\Requests\CitiesCategoryRequest;
use App\Models\CitiesCategory;
use Illuminate\Support\Facades\Redirect;

class CitiesCategoryController extends AdminController
{
    function __construct(){

        parent::__construct();
        $this->module = 'cities';
        $this->currentPage = 'cities-category';

    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $categories = CitiesCategory::all();
        $this->title = 'Cities Bird Categories';

        $this->_shareViews();

        return view('admin.cities.categories.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $this->title = 'Create CITIES Category';
        $this->_shareViews();
        return view('admin.cities.categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(CitiesCategoryRequest $request)
    {
        CitiesCategory::create($request->all());

        return Redirect::Route('admin.cities-categories.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $category = CitiesCategory::findOrFail($id);

        $this->title = 'Create CITIES Category';
        $this->_shareViews();

        return view('admin.cities.categories.edit', compact('category', 'module'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id, CitiesCategoryRequest $request)
    {
        $category = CitiesCategory::findorFail($id);

        $category->update($request->all());

        return Redirect::Route('admin.cities-categories.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        $citiesCategory = CitiesCategory::findOrFail($id);

        $citiesCategory->delete();

        return Redirect::Route('admin.cities-categories.index');
    }

}
