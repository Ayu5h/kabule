<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PasswordResetController extends AdminController
{

    public function getResetPassword()
    {
        return view('admin.auth.reset');
    }

    public function postResetPassword(Request $request)
    {
        $this->validate($request, [
            'password' => 'required|confirmed'
        ]);

        $user = Auth::user();

        $user->password = bcrypt($request->input('password'));
        $user->save();

        flash()->success('Your password has been changed');

        return redirect()->back();

    }

}
