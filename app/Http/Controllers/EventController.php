<?php namespace App\Http\Controllers;

use App\Event;
use App\Http\Requests\EventRequest;
use App\Http\Controllers\Controller;

use App\Media;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Route;

class EventController extends AdminController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $events = Event::all();
		return view('admin.event.index', compact('events'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('admin.event.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(EventRequest $request)
	{
        $event = Event::create($request->all());

        flash()->success('Your Event has been created');

        return Redirect::Route('admin.event.edit', [$event]);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$event = Event::findOrFail($id);

        return view('admin.event.edit', compact('event'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, EventRequest $request)
	{
		Event::findOrFail($id)->update($request->all());

        flash()->success('Your Event has been updated');

        return Redirect::Route('admin.event.index');
	}


    public function addPhoto($event_id, Request $request)
    {
        $this->validate($request, [
            'photo' => 'required|mimes:jpg,jpeg,png,bmp'
        ]);

        $event = Event::findorFail($event_id);

        $file = $request->file('photo');

        $photo = Media::named($file->getClientOriginalName())->move($file, 190, 145)->resizeImage(620, 310);

        return $event->photos()->save($photo);

    }


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Event::findOrFail($id)->delete();

        flash()->success('Your Event has been deleted');

        return redirect()->back();
	}

}
