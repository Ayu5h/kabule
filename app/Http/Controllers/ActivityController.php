<?php namespace App\Http\Controllers;

use App\Activity;
use App\Http\Requests\ActivityRequest;
use App\Http\Controllers\Controller;

use App\Media;
use App\Program;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class ActivityController extends AdminController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index($programId)
	{
        $program = Program::findOrFail($programId);

        $activities = $program->activities;
//        dd($activities);

        return view('admin.activity.index', compact('activities', 'programId'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create($programId)
	{
        return view('admin.activity.create', compact('programId'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store($programId, ActivityRequest $request)
	{
        $program = Program::findOrFail($programId);

        $activityId = $program->activities()->create($request->all());

        flash()->success('Your Activity has been created');

        return Redirect::route('admin.project.{id}.activity.edit', [$programId, $activityId]);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($programId, $activityId)
	{
        $activity = Activity::findOrFail($activityId);

        return view('admin.activity.edit', compact('programId', 'activity'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($programId, $activityId, ActivityRequest $request)
	{
        $activity = Activity::findOrFail($activityId);

        $activity->update($request->all());

        flash()->success('Your Activity has been updated');

        return Redirect::route('admin.project.{id}.activity.index', [$programId]);
	}

    public function addPhoto($activityId, Request $request)
    {
        $this->validate($request, [
            'photo' => 'required|mimes:jpg,jpeg,png,bmp'
        ]);

        $activity = Activity::findorFail($activityId);

        $file = $request->file('photo');

        $photo = Media::named($file->getClientOriginalName())->move($file, 192, 145)->resizeImage(620, 310);

        return $activity->photos()->save($photo);

    }


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($programId, $activityId)
	{
		Activity::findOrFail($activityId)->delete();

        flash()->success('Your Activity has been deleted');

        return redirect()->back();
	}

}
