<?php namespace App\Http\Controllers;

use App\Impact;
use App\Media;
use App\Http\Requests\ImpactRequest;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class ImpactController extends AdminController
{



    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $impacts = Impact::All();

        return view('admin.impact.index', compact('impacts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {

        return view('admin.impact.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(ImpactRequest $request)
    {

        $impact_id = Impact::create($request->all());

        flash()->success('Our Impact has been created');

        return Redirect::route('admin.impact.edit', [$impact_id]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $impact = Impact::findOrFail($id);

        return view('admin.impact.edit', compact('impact', 'breadcrumb'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id, ImpactRequest $request)
    {
        $impact = Impact::findorFail($id);

        $impact->update($request->all());

        flash()->success('Our Impact has been updated');

        return redirect('admin/impact');
    }

    public function addPhoto($impact_id, Request $request)
    {
        $this->validate($request, [
            'photo' => 'required|mimes:jpg,jpeg,png,bmp'
        ]);

        $impact = Impact::findorFail($impact_id);

        $file = $request->file('photo');
        //85, 63
        $photo = Media::named($file->getClientOriginalName())->move($file, 192, 145)->resizeImage(620, 310);

        return $impact->photos()->save($photo);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        Impact::findOrFail($id)->delete();

        flash()->success('Our Impact has been deleted');

        return redirect()->back();
    }

}
