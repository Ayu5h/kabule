<?php namespace App\Http\Controllers\Bird;

use App\Http\Controllers\AdminController;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Media;
use App\Models\Bird;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class BirdController extends AdminController {

    function __construct(){
        $this->module = 'cities';
        $this->currentPage = 'birds';
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $birds = Bird::all();
        $this->title = 'All Birds';

        $this->_shareViews();

        return view('admin.birds.index', compact('birds'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        $title = 'Add Bird To List';
        $module = $this->module;

        $this->_shareViews();

		return view('admin.birds.create', compact('title'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Requests\BirdRequest $request)
	{
        $birdId = Bird::create($request->all());

        $this->_shareViews();

        return Redirect::route('admin.birds.edit', $birdId);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$bird = Bird::findOrFail($id);
        $title = $bird->name . ' Detail';

        return view('admin.bird.show', compact('bird', 'title'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$bird = Bird::whereId($id)->with('photos')->first();
        $title = 'Edit Bird "'. $bird->name . '"';
        $module = $this->module;

        $this->_shareViews();

        return view('admin.birds.edit', compact('bird', 'title', 'module'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, Requests\BirdRequest $request)
	{
		$bird = Bird::find($id);

        $bird->update( $request->all() );

        return Redirect::Route('admin.birds.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$bird = Bird::findOrFail($id);

        $bird->delete();
        Session::flash('');

        return Redirect::Route('admin.birds.index');
	}

    /**
     * @param $birdId
     * @param Request $request
     * @return mixed
     */
    public function addPhoto($birdId, Request $request)
    {
        $this->validate($request, [
            'photo' => 'required|mimes:jpg,jpeg,png,bmp'
        ]);

        $bird = Bird::findorFail($birdId);

        $file = $request->file('photo');

        $photo = Media::named($file->getClientOriginalName())->move($file);

        return $bird->photos()->save($photo);

    }

}
