<?php namespace App\Http\Controllers;

use App\Media;
use App\News;
use App\Http\Requests\NewsRequest;
use App\Http\Controllers\Controller;

use App\Program;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class NewsController extends AdminController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index($programId)
	{
//        $allNews = News::where('program_id', $programId);
        $program = Program::findOrFail($programId);

        $allNews = $program->news;
        $breadcrumb = 'project-news';
        return view('admin.news.index', compact('allNews','programId', 'breadcrumb'));

	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create($programId)
	{
        $breadcrumb = 'create-project-news';
		return view('admin.news.create', compact('programId','breadcrumb'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store($programId, NewsRequest $request)
	{
        $program = Program::findOrFail($programId);

        $newsId = $program->news()->create($request->all());

        flash()->success('Your News has been created');

        return Redirect::route('admin.project.{id}.news.edit', [$programId,$newsId]);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($programId, $newsId)
	{
        $news = News::findorFail($newsId);
        return view('admin.news.edit', compact('news', 'programId'));
	}



	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($programId,$newsId, NewsRequest $request)
	{
        $news = News::findorFail($newsId);

        $news->update($request->all());

        flash()->success('Your News has been updated');

        return Redirect::route('admin.project.{id}.news.index', [$programId]);
	}

    public function addPhoto($newsId,Request $request)
    {
        $this->validate($request, ['photo' => 'required|mimes:jpg,jpeg,png,bmp']);

        $news = News::findOrFail($newsId);

        $file = $request->file('photo');
        $photo = Media::named($file->getClientOriginalName())->move($file, 192, 145)->resizeImage(620, 310);

//        $news->featured_image = $photo->path;
//        $news->featured_thumbnail = $photo->thumbnail_path;
//        $news->save();

        return $news->photos()->save($photo);

    }

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($programId, $newsId)
	{
        $news = News::findOrFail($newsId);

        $news->delete();

        flash()->success('Your News has been deleted');

        return redirect()->back();
	}

}
