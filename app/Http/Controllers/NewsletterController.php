<?php namespace App\Http\Controllers;

use App\Http\Requests\NewsletterRequest;
use App\Http\Controllers\Controller;

use App\Media;
use App\Newsletter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class NewsletterController extends AdminController
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $newsletters = Newsletter::all();

        return view('admin.newsletter.index', compact('newsletters'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {

        return view('admin.newsletter.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(NewsletterRequest $request)
    {
//        dd($parameters);
        $parameters = $this->uploadFile($request);

        $newsletter = Newsletter::create($parameters);

        flash()->success('Your Newsletter has been created');

        return Redirect::Route('admin.newsletter.edit', [$newsletter]);
    }

    private function uploadFile(Request $request)
    {
        $baseDir = 'media_uploads/files';
        $file = $request->file('file');

        $originalName = $file->getClientOriginalName();
        $name = sprintf("%s-%s", time(), $originalName);
        $file->move($baseDir, $name);

        $parameters = $request->only('title');
        $parameters['file_name'] = $originalName;
        $parameters['file_path'] = sprintf('%s/%s', $baseDir, $name);

        return $parameters;

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $newsletter = Newsletter::findOrFail($id);

        return view('admin.newsletter.edit', compact('newsletter'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id, Request $request)
    {

        $this->validate($request, [
            'title' => 'required'
        ]);

        $newsletter = Newsletter::findOrFail($id);

        $parameters = $request->only('title');
        if(!is_null($request->file('file'))) {
            $parameters = $this->uploadFile($request);

            if($newsletter->file_path) {
                \File::delete([
                    $newsletter->file_path
                ]);
            }
        }

        $newsletter->update($parameters);

        flash()->success('Your Newsletter has been updated');

        return Redirect::Route('admin.newsletter.index');
    }

    public function addPhoto($newsletterId, Request $request)
    {
        $this->validate($request, [
            'photo' => 'required|mimes:jpg,jpeg,png,bmp'
        ]);

        $newsletter = Newsletter::findorFail($newsletterId);

        if($newsletter->image) {
            \File::delete([
                $newsletter->image,
                $newsletter->thumbnail
            ]);
        }

        $file = $request->file('photo');

        $photo = Media::named($file->getClientOriginalName())->move($file, 110, 144);

        $newsletter->image = $photo->path;
        $newsletter->thumbnail = $photo->thumbnail_path;
        $newsletter->save();

        return 'done';

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        Newsletter::findOrFail($id)->delete();

        flash()->success('Your Newsletter has been deleted');

        return redirect()->back();
    }

}
