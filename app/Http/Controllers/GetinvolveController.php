<?php namespace App\Http\Controllers;

use App\Getinvolve;
use App\Media;
use App\Http\Requests\GetinvolveRequest;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class GetinvolveController extends AdminController
{



    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $getinvolved = Getinvolve::All();

        return view('admin.getinvolve.index', compact('getinvolved'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {

        return view('admin.getinvolve.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(GetinvolveRequest $request)
    {

        $getinvolve_id = Getinvolve::create($request->all());

        flash()->success('Your Get Involved has been created');

        return Redirect::route('admin.getinvolve.edit', [$getinvolve_id]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $getinvolve = Getinvolve::findOrFail($id);

        return view('admin.getinvolve.edit', compact('getinvolve', 'breadcrumb'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id, GetinvolveRequest $request)
    {
        $getinvolve = Getinvolve::findorFail($id);

        $getinvolve->update($request->all());

        flash()->success('Your Get Involve has been updated');

        return redirect('admin/getinvolve');
    }

    public function addPhoto($getinvolve_id, Request $request)
    {
        $this->validate($request, [
            'photo' => 'required|mimes:jpg,jpeg,png,bmp'
        ]);

        $getinvolve = Getinvolve::findorFail($getinvolve_id);

        $file = $request->file('photo');
        //85, 63
        $photo = Media::named($file->getClientOriginalName())->move($file, 192, 145)->resizeImage(620, 310);

        return $getinvolve->photos()->save($photo);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        Getinvolve::findOrFail($id)->delete();

        flash()->success('Your Get Involve has been deleted');

        return redirect()->back();
    }

}
