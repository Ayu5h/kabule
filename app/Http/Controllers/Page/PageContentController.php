<?php namespace App\Http\Controllers\Page;

use App\Http\Controllers\AdminController;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Media;
use App\PageContent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class PageContentController extends AdminController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$pageContents = PageContent::all();

        return view('admin.page-content.index', compact('pageContents'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('admin.page-content.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Requests\PageContentRequest $request)
	{

        $pageContent = PageContent::all( $request->all() );

        return Redirect::route('admin.page-contents.index');

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$pageContent = PageContent::findOrFail($id);

        return view('admin.page-content.edit', compact('pageContent'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, Requests\PageContentRequest $request)
	{
		$pageContent = PageContent::findOrFail($id);

        $pageContent->update($request->all());

        return Redirect::Route('admin.page-contents.index');
	}


    public function addPhoto($pageContentId, Request $request)
    {
        $this->validate($request, [
            'photo' => 'required|mimes:jpg,jpeg,png,bmp'
        ]);

        $pageContent = PageContent::findorFail($pageContentId);

        foreach($pageContent->photos as $photo){
            $photo->delete();
        }

        $file = $request->file('photo');

        $photo = Media::named($file->getClientOriginalName())->move($file, 150, 150);

        return $pageContent->photos()->save($photo);

    }

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$pageContent = PageContent::findOrFail($id);
        $pageContent->delete();

        return Redirect::route('admin.page-contents.index');
	}

}
