<?php namespace App\Http\Controllers\Page;

use App\Http\Controllers\AdminController;
use App\Http\Requests\PageRequest;
use App\Http\Controllers\Controller;

use App\Media;
use App\Models\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

//use Illuminate\Support\Facades\Redirect;

class PageController extends AdminController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{

	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        $this->title = 'Create New Page';
        $this->module = 'pages';

        $options = [
            '0' => 'About Us',
            '1' => 'Birds'
        ];

        return view('admin.pages.create', compact('options'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(PageRequest $request)
	{
        $customArr = ['is_core' => '1'];
        $page = Page::create( array_merge($customArr,$request->all()) );

        return Redirect::route('admin.pages.edit', $page->slug);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

    /**
     * Show the form for editing the specified resource.
     *
     * @param $slug
     * @return \Illuminate\View\View
     */
	public function edit($slug)
	{
        $page = Page::whereSlug($slug)->with('photos')->first();
        $this->title = 'Edit '. ucwords($page->name) . ' Page';

        if( $page->isCore() && $page->isBirdPage() ){
            $this->module = 'cities';
        }else if( $page->isCore() && $page->isAboutPage() ){
            $this->module = 'about';
        }else{
            $this->module = 'pages';
        }

        $this->_shareViews();
        return view('admin.pages.edit', compact('page'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, PageRequest $request)
	{
        $page = Page::findOrFail($id);
//        dd($page);
        $page->update($request->all());

//        $pageName = $page->name;
        flash()->success("Page updated Successfully");
        return Redirect::Route('admin.pages.edit', [$page->slug]);
//	    return redirect()->route('admin/page.edit', [$page->slug]);
    }

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$page = Page::findOrFail($id);

		$page->delete();

		flash()->success('Page Deleted Successfully');

		return redirect('banner');
	}

    /**
     * @param $pageId
     * @param Request $request
     * @return mixed
     */
    public function addPhoto($pageId, Request $request)
    {
        $this->validate($request, [
            'photo' => 'required|mimes:jpg,jpeg,png,bmp'
        ]);

        $page = Page::findorFail($pageId);

        $file = $request->file('photo');

        $photo = Media::named($file->getClientOriginalName())->move($file);

        return $page->photos()->save($photo);

    }

}
