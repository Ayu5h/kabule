<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Media;
use Illuminate\Http\Request;
use Response;

class MediaController extends Controller {


    public function __construct()
    {
        $this->middleware('auth', ['only' => ['destroy']]);
    }

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id, Request $request)
	{
        $media = Media::findOrFail($id);

        $bearer = $media->imageable;

        if( isset($bearer->featured_image) && $media->path == $bearer->featured_image ) {
            $bearer->featured_image = '';
            $bearer->featured_thumbnail = '';
            $bearer->save();
        }

        $media->delete();

        if ($request->ajax()) {
            return 'success';
        } else {
            flash()->success("$media->name deleted successfully!");
            return redirect()->back();
        }


	}

    public function download($id)
    {
        $media = Media::findOrFail($id);

//        $headers = array(
//            'Content-Type: application/pdf',
//        );

        return Response::download($media->path, $media->name);
    }


}
