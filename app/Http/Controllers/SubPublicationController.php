<?php namespace App\Http\Controllers;

use App\Http\Requests\SubPublicationRequest;
use App\Http\Controllers\Controller;

use App\Media;
use App\Publication;
use App\SubPublication;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class SubPublicationController extends AdminController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index($slug)
	{
        dd($slug);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create($publicationSlug)
	{
        $publication = Publication::findBySlugOrFail($publicationSlug);
		return view('admin.publication.sub-publication.create', compact('publication'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store($publicationSlug, SubPublicationRequest $request)
	{
        $publication = Publication::findBySlugOrFail($publicationSlug);

        $subPublication = $publication->subPublications()->create($request->all());

//        return Redirect::Route('admin.publication.show', $publicationSlug);
        return Redirect::Route('editSubPublication', [$publicationSlug, $subPublication->slug]);


	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($publicationSlug, $subPublicationSlug)
	{
        $publication = Publication::findBySlugOrFail($publicationSlug);

		$subPublication = SubPublication::findBySlugOrFail($subPublicationSlug);

        return view('admin.publication.sub-publication.edit', compact('subPublication', 'publication'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($publicationSlug, $subPublicationSlug, SubPublicationRequest $request)
	{
		$subPublication = SubPublication::findBySlugOrFail($subPublicationSlug);

        $subPublication->update($request->all());

        return Redirect::Route('admin.publication.show', $publicationSlug);
	}

    public function addPhoto($id, Request $request)
    {
        $this->validate($request, [
            'file' => 'required'
        ]);

        $subPublication = SubPublication::findorFail($id);

        $file = $request->file('file');

        $attachment = Media::named($file->getClientOriginalName(), 'others')->move($file, 0);

        return $subPublication->attachments()->save($attachment);

    }

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($publicationSlug, $subPublicationSlug)
	{
        $publication = Publication::findBySlugOrFail($publicationSlug);
		SubPublication::findBySlugOrFail($subPublicationSlug)->delete();

        flash()->success("Your $publication->category has been deleted.");

        return redirect()->back();

	}

}
