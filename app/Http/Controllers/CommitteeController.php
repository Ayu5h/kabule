<?php namespace App\Http\Controllers;

use App\Committee;
use App\Media;
use App\Member;
use App\Http\Requests\CommitteeRequest;
use App\Http\Requests\MemberRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class CommitteeController extends AdminController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $committees = Committee::get();
		return view('admin.committee.manage-committee',compact('committees'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('admin.committee.create-committee');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(CommitteeRequest $request)
	{
        Committee::create($request->all());

        flash()->success('Committee has been added.');

        return redirect('admin/committee');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$committee = Committee::find($id);
        return view('admin.committee.edit-committee', compact('committee'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, CommitteeRequest $request)
	{
		$committee = Committee::findorFail($id);
        $committee->update($request->all());

        flash()->success('Committee has been updated.');
        return redirect('admin/committee');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$committee = Committee::find($id);
        $res = $committee->delete();

        if($res){
            flash()->success('Committee has been deleted.');
            return redirect('admin/committee');
        }
	}



}
