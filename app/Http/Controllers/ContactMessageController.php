<?php namespace App\Http\Controllers;

use App\ContactMessage;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class ContactMessageController extends AdminController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$contactMessages = ContactMessage::all();
        return view('admin.contact-messages.index', compact('contactMessages'));
	}



	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        $contactMessage = ContactMessage::findOrFail($id);
		return view('admin.contact-messages.show', compact('contactMessage'));
	}



	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		ContactMessage::findOrFail($id)->delete();

        flash()->success('Your Contact Message has been deleted');

        return redirect()->back();
	}

}
