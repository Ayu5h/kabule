<?php namespace App\Http\Controllers;

use App\Partner;
use App\Media;
use App\Http\Requests\PartnerRequest;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class PartnerController extends AdminController
{



    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $partners = Partner::All();

        return view('admin.partner.index', compact('partners'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {

        return view('admin.partner.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(PartnerRequest $request)
    {

        $partner_id = Partner::create($request->all());

        flash()->success('Your Partner has been created');

        return Redirect::route('admin.partner.edit', [$partner_id]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $partner = Partner::findOrFail($id);

        return view('admin.partner.edit', compact('partner', 'breadcrumb'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id, PartnerRequest $request)
    {
        $partner = Partner::findorFail($id);

        $partner->update($request->all());

        flash()->success('Your Partner has been updated');

        return redirect('admin/partner');
    }

    public function addPhoto($partner_id, Request $request)
    {
        $this->validate($request, [
            'photo' => 'required|mimes:jpg,jpeg,png,bmp'
        ]);

        $partner = Partner::findorFail($partner_id);

        $file = $request->file('photo');
        //85, 63
        $photo = Media::named($file->getClientOriginalName())->move($file, 192, 145)->resizeImage(620, 310);

        return $partner->photos()->save($photo);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        Partner::findOrFail($id)->delete();

        flash()->success('Your Partner has been deleted');

        return redirect()->back();
    }

}
