<?php namespace App\Http\Controllers;

use App\Http\Requests\SiteNewsRequest;
use App\Http\Controllers\Controller;

use App\Media;
use App\SiteNews;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class SiteNewsController extends AdminController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $allNews = SiteNews::all();

        return view('admin.site-news.index', compact('allNews'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('admin.site-news.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(SiteNewsRequest $request)
	{
		$news = SiteNews::create($request->all());
//        $news->resluggify()->save();
        flash()->success('Your News has been created');

        return Redirect::Route('admin.news.edit', [$news]);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$news = SiteNews::findOrFail($id);

        return view('admin.site-news.edit', compact('news'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, SiteNewsRequest $request)
	{
		$news = SiteNews::findOrFail($id);

        $news->update($request->all());

        flash()->success('Your News has been updated');

        return Redirect::Route('admin.news.index');
	}

    public function addPhoto($news_id, Request $request)
    {
        $this->validate($request, [
            'photo' => 'required|mimes:jpg,jpeg,png,bmp'
        ]);

        $program = SiteNews::findorFail($news_id);

        $file = $request->file('photo');

        $photo = Media::named($file->getClientOriginalName())->move($file, 190, 145)->resizeImage(620, 310);

        return $program->photos()->save($photo);

    }


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		SiteNews::findOrFail($id)->delete();

        flash()->success('Your News has been deleted');

        return redirect()->back();
	}

}
