<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Media;
use App\Notice;
use Illuminate\Http\Request;

class NoticeController extends AdminController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $notices = Notice::with('attachments')->get();

        return view('admin.notice.index', compact('notices'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        $notice = new Notice;

		return view('admin.notice.create', compact('notice'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
        //validation

        $this->validate($request, [
            'title' => 'required',
            'date' => 'required'
        ]);

        $parameters = $request->only('title', 'description', 'date');

        $file = $request->file('file');

        if($file) {
            $attachment = Media::named($file->getClientOriginalName(), 'others')->move($file);
            $parameters['file_name'] = $attachment->name;
            $parameters['file_path'] = $attachment->path;
        }

		$notice = Notice::create($parameters);

        if($file) {
            $notice->attachments()->save($attachment);
        }

        flash()->success('Your Notice has been created');
        return redirect('admin/notice');
	}

//    private function uploadFile(Request $request)
//    {
//        $parameters = $request->only('title', 'description', 'date');
//        $file = $request->file('file');
//
//        if($file) {
//            $originalFileName = $file->getClientOriginalName();
//
//            $name = sprintf('%s-%s', time(), $originalFileName);
//            $baseDir = 'media_uploads/files';
//
//            $parameters['file_name'] = $originalFileName;
//            $parameters['file_path'] = sprintf('%s/%s', $baseDir, $name);
//            $file->move($baseDir, $name);
//        }
//
//        return $parameters;
//    }

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$notice = Notice::findOrFail($id);

        return view('admin.notice.edit', compact('notice'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, Request $request)
	{
        $this->validate($request, [
            'title' => 'required',
            'date' => 'required'
        ]);

        $notice = Notice::findOrFail($id);

        $parameters = $request->only('title', 'description', 'date');

        $file = $request->file('file');

        if($file) {
            $attachment = Media::named($file->getClientOriginalName(), 'others')->move($file);
            $parameters['file_name'] = $attachment->name;
            $parameters['file_path'] = $attachment->path;

            \File::delete([
                $notice->file_path
            ]);

            $notice->attachments()->save($attachment);

        }

        $notice->update($parameters);

        flash()->success('Your Notice has been updated.');

        return redirect('admin/notice');

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$notice = Notice::findOrFail($id);

        $notice->delete();

        flash()->success('Your Notice has been deleted.');
        return redirect()->back();


	}

}
