<?php namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesCommands;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Validator;

abstract class Controller extends BaseController {

	use DispatchesCommands, ValidatesRequests;

    protected $module = 'dashboard';
    protected $title = 'Dashboard';
    protected $currentPage = 'dashboard';

    /**
     * Setup the layout used by the controller.
     *
     * @return void
     */
    public $data = array();


    function __construct() {

        //get contact setting
//        $currentUrl = url(Route::getCurrentRoute()->getPath());
//        view()->share('currentUrl', $currentUrl);
    }

    protected function _shareViews() {
        view()->share('module', $this->module);
        view()->share('title', $this->title);
        view()->share('currentPage', $this->currentPage);
    }

    protected function setupLayout()
    {
        if ( ! is_null($this->layout))
        {
            $this->layout = View::make($this->layout);
            $this->data['assetUrl'] = 'public/site/assets/';
        }
    }
    protected function _validateArr($validationArr, $messages = array()) {

        $nameArr = array();
        $valueArr = array();
        //printr( $messages, 1 );

        foreach ($validationArr as $field => $rule) {

            if (Request::hasFile($field)) {
                $nameArr[$field] = Request::file($field);
            } else {
                $nameArr[$field] = Request::get($field);
            }
            $valueArr[$field] = $rule;
        }
        $validator = !empty($messages) ?
            Validator::make($nameArr, $valueArr, $messages) :
            Validator::make($nameArr, $valueArr);

        $validator->validationPassed = $validator->passes() ? true : false;

        $validator->getMessageBag()->setFormat('<label class="control-label inputError"><i class="fa fa-times-circle-o"></i> :message</label>');


        return $validator;
    }
}
