<?php namespace App\Http\Controllers;

use App\Album;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Media;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class GalleryController extends AdminController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
    function index($id) {
        $album = Album::findOrFail($id);

        return view('admin.media.index', compact('album'));
    }

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$media = Media::findOrFail($id);

        $type = $media->imageable;
        if( !empty($type->featured_image) && $type->featured_image == $media->path ){
            $type->featured_image = '';
            $type->featured_thumbnail = '';

        }else if( !empty($type->image) && $type->image == $media->path ){
            $type->image = '';
            $type->image_thumbnail = '';
        }

        $type->save();
        $media->delete();

        flash()->success('Media and image associated with module has been deleted.');

        return Redirect::route('admin.gallery.index');
	}


    public function upload($albumId, Request $request)
    {
        $this->validate($request, [
            'photo' => 'required|mimes:jpg,jpeg,png,bmp'
        ]);

        $album = Album::findorFail($albumId);

        $file = $request->file('photo');

        $photo = Media::named($file->getClientOriginalName())->move($file, 190, 145)->resizeImage(620, 310);

        return $album->photos()->save($photo);

    }

}
