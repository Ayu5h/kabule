<?php namespace App\Http\Controllers;

use App\Committee;
use App\Http\Requests\MemberRequest;
use App\Http\Controllers\Controller;

use App\Media;
use App\Member;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class MemberController extends AdminController {

    function index($committeeId){
        $committee = Committee::findOrFail($committeeId);

        $committeeMembers = $committee->members;

        return view('admin.committee.members.manage-members', compact('committeeMembers', 'committee', 'committeeId'));
    }

    function create($committeeId){
        $committee = Committee::find($committeeId);

        return view('admin.committee.members.create-committee-members',compact('committeeId', 'committee'));
    }

    function store($committeeId, MemberRequest $request){

        $committee = Committee::findOrFail($committeeId);

        $member = $committee->members()->create($request->all());
//        $member = Member::create($request->all());
//        $committeeId = $member->committee_id;

        flash()->success('Committee member has been added.');
        return Redirect::route('admin.committee.{committeeId}.member.edit',[$committeeId, $member]);
    }

    function edit($committeeId, $committeeMemberId){
        $committee = Committee::findOrFail($committeeId);
        $committeeMember = Member::findOrFail($committeeMemberId);
        return view('admin.committee.members.edit-committee-member', compact('committeeMember', 'committee', 'committeeId'));
    }

    function update($committeeId, $committeeMemberId, MemberRequest $request){

        $committeeMember = Member::findOrFail($committeeMemberId);

        $committeeMember->update($request->all());

        flash()->success('Committee member has been updated.');
        return Redirect::Route('admin.committee.{committeeId}.member.index', $committeeId);
    }

    function destroy($committeeId, $committeeMemberId){

        $committeeMember = Member::findOrFail($committeeMemberId);
        $res = $committeeMember->delete();
//        $committeeMembers = Member::committeeId($committeeId)->get();
        if($res){
            flash()->success('Committee member has been deleted.');
//            return Redirect('admin.committee.members.manage-members', compact('committeeMembers','committeeId'));
//            return Redirect::Route('manageMembers', [$committeeId]);
            return redirect()->back();
        }
    }

    public function addMemberPhoto($memberId, Request $request){

        $this->validate($request, ['photo' => 'sometimes|mimes:jpg,jpeg,png,bmp']);
        $member = Member::findorFail($memberId);

        $file = $request->file('photo');

        $photo = Media::named($file->getClientOriginalName())->move($file, 190, 145);
        $member->image = $photo->path;
        $member->image_thumbnail = $photo->thumbnail_path;
        $member->save();

        return $member->photos()->save($photo);
    }

}
