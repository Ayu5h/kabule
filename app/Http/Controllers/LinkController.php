<?php namespace App\Http\Controllers;

use App\Http\Requests\LinkRequest;
use App\Http\Controllers\Controller;

use App\Link;
use App\Media;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class LinkController extends AdminController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$links = Link::all();

        return view('admin.link.index', compact('links'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('admin.link.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(LinkRequest $request)
	{
		$link = Link::create($request->all());

        flash()->success('Your Link has been created');

        return Redirect::Route('admin.link.edit', [$link]);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $link = Link::findOrFail($id);

        return view('admin.link.edit', compact('link'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, LinkRequest $request)
	{
		Link::findOrFail($id)->update($request->all());

        flash()->success('Your Link has been updated');

        return Redirect::Route('admin.link.index');
	}

    public function addPhoto($linkId, Request $request)
    {
        $this->validate($request, [
            'photo' => 'required|mimes:jpg,jpeg,png,bmp'
        ]);

        $link = Link::findorFail($linkId);

        \File::delete([
            $link->image,
            $link->thumbnail
        ]);

        $file = $request->file('photo');

        $photo = Media::named($file->getClientOriginalName())->move($file, 200, 98);

        $link->image = $photo->path;
        $link->thumbnail = $photo->thumbnail_path;
        $link->save();

        return 'done';

    }

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Link::findOrFail($id)->delete();

        flash()->success('Your Link has been deleted');

        return redirect()->back();
	}

}
