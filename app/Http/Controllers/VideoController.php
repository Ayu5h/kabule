<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Media;
use App\Video;
use App\VideoCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class VideoController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index( $categoryId ) {

        $category = VideoCategory::findOrfail($categoryId);

        $videos = $category->videos()->with('photos')->get();
        return view('admin.media.videos.index', compact('videos', 'category'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create( $id ) {
        $category = VideoCategory::find($id);
        return view('admin.media.videos.create', compact('category'));
    }

    /**
     * @param $link
     * @return mixed
     */
    function splitLink($link) {

        if(strstr($link, 'https://www.youtube.com/watch?v=')) {
            return str_replace('https://www.youtube.com/watch?v=', '', $link);
        } else if(strstr($link, 'http://www.youtube.com/watch?v=')) {
            return str_replace('http://www.youtube.com/watch?v=', '', $link);
        } else {
            return $link;
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store($categoryId, Requests\VideoRequest $request) {

        $category = VideoCategory::findOrFail($categoryId);

        $link = $request->get('link');
        $vlink = $this->splitLink($link);

        $request['vlink'] = $vlink;

        $video = $category->videos()->create( $request->all() );

        $photo = Media::named($vlink.'.jpg')->moveYoutubeImage($vlink);
        $video->photos()->save($photo);

        flash()->success('Video has been added.');

        return Redirect::route('admin.video-categories.{id}.videos.index', $categoryId);

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($categoryId, $id) {

        $category = VideoCategory::findOrFail($categoryId);
        $video = Video::findOrFail($id);

        return view('admin.media.videos.edit', compact('video', 'category'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($categoryId, $id, Requests\VideoRequest $request) {

        $category = VideoCategory::findOrFail($categoryId);
        $video = Video::findOrFail($id)->deleteMedias();

        $link = $request->get('link');
        $vlink = $this->splitLink($link);

        $request['vlink'] = $vlink;

        $video->update( $request->all() );
        $photo = Media::named($vlink.'.jpg')->moveYoutubeImage($vlink);
        $video->photos()->save($photo);

        flash()->success('Video has been updated.');

        return Redirect::route('admin.video-categories.{id}.videos.index', $categoryId);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($categoryId, $id) {

        VideoCategory::findOrFail($categoryId);
        Video::findOrFail($id)->delete();
        flash()->success('Video has been deleted.');

        return redirect()->back();
    }

}
