<?php namespace App\Http\Controllers;

use App\Contact;
use App\Http\Requests\ContactRequest;
use App\Http\Controllers\Controller;

use App\Media;
use Illuminate\Http\Request;

class ContactController extends AdminController {

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit()
	{

		$contactSettings = Contact::firstOrCreate([]);

        return view('admin.contact.edit', compact('contactSettings'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(ContactRequest $request)
	{
        $contactSettings = Contact::firstOrFail();

        $contactSettings->update($request->all());

        flash()->success('Your Contact Settings has been updated.');

        return redirect()->back();
	}


    public function addPhoto($id, Request $request){
        $this->validate($request, ['photo' => 'sometimes|mimes:jpg,jpeg,png,bmp']);
        $contactSettings = Contact::findorFail($id);

        foreach($contactSettings->photos as $photo) {
            $photo->delete();
        }


        $file = $request->file('photo');

        $photo = Media::named($file->getClientOriginalName())->move($file, 250, 200);

        return $contactSettings->photos()->save($photo);
    }

}
