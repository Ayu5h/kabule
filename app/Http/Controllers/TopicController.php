<?php namespace App\Http\Controllers;

use App\Media;
use App\Program;
use App\Http\Requests\TopicRequest;
use App\Http\Controllers\Controller;

use App\Topic;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class TopicController extends AdminController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index($programId)
	{
        $program = Program::findOrFail($programId);

        $topics = $program->topics;

        return view('admin.topic.index', compact('topics','programId'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create($programId)
	{
		return view('admin.topic.create', compact('programId'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store($programId, TopicRequest $request)
	{
		$program = Program::findOrFail($programId);

        $topicId = $program->topics()->create($request->all());

        flash()->success('Your Topic has been created');

        return Redirect::route('admin.project.{id}.topic.edit', [$programId, $topicId]);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($programId, $topicId)
	{
        $topic = Topic::findOrFail($topicId);

        return view('admin.topic.edit', compact('topic', 'programId'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($programId, $topicId, TopicRequest $request)
	{
        $topic = Topic::findorFail($topicId);

        $topic->update($request->all());

        flash()->success('Your Topic has been updated');

        return Redirect::route('admin.project.{id}.topic.index', [$programId]);
	}


    public function addPhoto($topicId, Request $request)
    {
        $this->validate($request, [
            'photo' => 'required|mimes:jpg,jpeg,png,bmp'
        ]);

        $topic = Topic::findorFail($topicId);

        $file = $request->file('photo');

        $photo = Media::named($file->getClientOriginalName())->move($file);

        return $topic->photos()->save($photo);

    }


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($programId,$topicId)
	{
		Topic::findOrFail($topicId)->delete();

        flash()->success('Your Topic has been deleted');

        return redirect()->back();
	}

}
