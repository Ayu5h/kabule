<?php namespace App\Http\Controllers;

use App\WhatWeDo;
use App\Media;
use App\Http\Requests\WhatWeDoRequest;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class WhatWeDoController extends AdminController
{



    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $whatWeDos = WhatWeDo::All();

        return view('admin.whatWeDo.index', compact('whatWeDos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {

        return view('admin.whatWeDo.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(WhatWeDoRequest $request)
    {

        $whatWeDo_id = WhatWeDo::create($request->all());

        flash()->success('Your What We Do file has been created');

        return Redirect::route('admin.whatWeDo.edit', [$whatWeDo_id]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $whatWeDo = WhatWeDo::findOrFail($id);

        return view('admin.whatWeDo.edit', compact('whatWeDo', 'breadcrumb'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id, WhatWeDoRequest $request)
    {
        $whatWeDo = WhatWeDo::findorFail($id);

        $whatWeDo->update($request->all());

        flash()->success('Your What We Do file has been updated');

        return redirect('admin/whatWeDo');
    }

    public function addPhoto($whatWeDo_id, Request $request)
    {
        $this->validate($request, [
            'photo' => 'required|mimes:jpg,jpeg,png,bmp'
        ]);

        $whatWeDo = WhatWeDo::findorFail($whatWeDo_id);

        $file = $request->file('photo');
        //85, 63
        $photo = Media::named($file->getClientOriginalName())->move($file, 192, 145)->resizeImage(620, 310);

        return $whatWeDo->photos()->save($photo);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        WhatWeDo::findOrFail($id)->delete();

        flash()->success('Your What We Do file has been deleted');

        return redirect()->back();
    }

}
