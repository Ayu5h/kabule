<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\SubPublication;
use Illuminate\Http\Request;

use App\Publication;
use Illuminate\Support\Facades\Redirect;

class PublicationController extends AdminController {


	public function index()
	{
		$publications = Publication::all();

        return view('admin.publication.manage-publication',compact('publications'));
	}


	public function create()
	{
		return view('admin.publication.create-publication');
	}


	public function store(Request $request)
	{
        $this->validate($request, [
            'category' => 'required'

        ]);

        $publication = Publication::create($request->all());

        flash()->success('Your Publication has been created.');

        return Redirect::Route('admin.publication.index');
//        return Redirect::route('admin.publication.edit',[$publication->id]);
	}



	public function show($slug)
	{
        $publication = Publication::findBySlugOrFail($slug);
        $subPublications = $publication->subPublications;

        return view('admin.publication.sub-publication.index', compact('subPublications', 'publication'));
	}


	public function edit($slug)
	{
        $publication = Publication::findBySlugOrFail($slug);

		return view('admin.publication.edit-publication', compact('publication'));
	}





	public function update($id, Request $request)
	{
        $this->validate($request, [
            'category' => 'required'
        ]);

        $publication = Publication::findOrFail($id);
        $publication->update($request->all());

        flash()->success("Your Publication has been updated.");
        return redirect('admin/publication');
	}


	public function destroy($publicationId)
	{
        $publication = Publication::findOrFail($publicationId);
        $publication->delete();
        flash()->success('Your Publication has been deleted.');

        return redirect('admin/publication');
	}

}
