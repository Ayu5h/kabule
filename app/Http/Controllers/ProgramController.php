<?php namespace App\Http\Controllers;

use App\Program;
use App\Media;
use App\Http\Requests\ProgramRequest;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class ProgramController extends AdminController
{



    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $programs = Program::All();

        return view('admin.program.index', compact('programs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {

        return view('admin.program.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(ProgramRequest $request)
    {

        $program_id = Program::create($request->all());

        flash()->success('Your Project has been created');

        return Redirect::route('admin.project.edit', [$program_id]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $program = Program::findOrFail($id);

        return view('admin.program.edit', compact('program', 'breadcrumb'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id, ProgramRequest $request)
    {
        $program = Program::findorFail($id);

        $program->update($request->all());

        flash()->success('Your Project has been updated');

        return redirect('admin/project');
    }

    public function addPhoto($program_id, Request $request)
    {
        $this->validate($request, [
            'photo' => 'required|mimes:jpg,jpeg,png,bmp'
        ]);

        $program = Program::findorFail($program_id);

        $file = $request->file('photo');
        //85, 63
        $photo = Media::named($file->getClientOriginalName())->move($file, 192, 145)->resizeImage(620, 310);

        return $program->photos()->save($photo);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        Program::findOrFail($id)->delete();

        flash()->success('Your Project has been deleted');

        return redirect()->back();
    }

}
