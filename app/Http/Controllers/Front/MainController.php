<?php

namespace app\Http\Controllers\Front;

use App\Banner;
use App\Http\Controllers\Controller;
use App\Media;
use App\PageContent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class MainController extends Controller
{
    protected $banner;
    
    public function home()
    {
        $banners= Banner::all();
        
        return view('front.index', compact('banners'));
    }
}