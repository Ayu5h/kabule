<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Video;
use App\VideoCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class VideoCategoryController extends Controller {

    public function index()
    {
        $categories = VideoCategory::all();

        return view('admin.media.video-categories.index',compact('categories'));
    }


    public function create()
    {
        return view('admin.media.video-categories.create');
    }


    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);

        $category = VideoCategory::create($request->all());

        flash()->success('Category has been created.');

        return Redirect::Route('admin.video-categories.index');
    }


    public function edit($slug)
    {
        $category = VideoCategory::findOrFail($slug);

        return view('admin.media.video-categories.edit', compact('category'));
    }


    public function update($id, Request $request)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);

        $videoCategory = VideoCategory::findOrFail($id);
        $videoCategory->update($request->all());

        flash()->success("Category has been updated.");
        return Redirect::route('admin.video-categories.index');
    }


    public function destroy($videoId)
    {
        VideoCategory::findOrFail($videoId)->delete();
        flash()->success('Category has been deleted.');

        return Redirect::route('admin.video-categories.index');
    }

}
