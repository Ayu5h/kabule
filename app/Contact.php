<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model {

	protected $fillable = [
        'address',
        'phone',
        'hotline',
        'email',
        'fax_number',
        'skype_handle',
        'description',
        'lat',
        'lng',
        'location'
    ];

    public function photos()
    {
        return $this->morphMany('App\Media', 'imageable');
    }

}
