<?php namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Notice extends Model {

	protected $fillable= [
        'title',
        'description',
        'file_name',
        'file_path',
        'date'
    ];

    protected $date = ['date'];

    public function getDateAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d');
    }

    public function getFormattedDateAttribute($date)
    {
        return Carbon::parse($date)->format('M d, Y');
    }

    public function attachments()
    {
        return $this->morphMany('App\Media', 'imageable');
    }

    public function delete()
    {
        $attachments = $this->attachments;

        foreach($attachments as $attachment) {
            $attachment->delete();
        }

        parent::delete();
    }

}
