<?php namespace App;

use Carbon\Carbon;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Illuminate\Database\Eloquent\Model;

class SubPublication extends Model implements SluggableInterface{

    protected $date = 'date';

    use SluggableTrait;

    protected $fillable = [
        'title',
        'description',
        'date'
    ];

    protected $sluggable = [
        'build_from' => 'title',
        'save_to'    => 'slug',
    ];

    public function publication()
    {
        return $this->belongsTo('App\Publication');
    }

    public function attachments(){
        return $this->morphMany('App\Media','imageable');
    }

    public function getDateAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d');
    }

    public function getFormattedDateAttribute($date)
    {
        return Carbon::parse($date)->format('M d, Y');
    }

    public function delete()
    {
        $attachments = $this->attachments;

        foreach($attachments as $attachment){
            $attachment->delete();
        }

        parent::delete();
    }

}
