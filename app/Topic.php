<?php namespace App;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Illuminate\Database\Eloquent\Model;

class Topic extends Model implements SluggableInterface {

    use SluggableTrait;

	protected $fillable = [
        'name',
        'description',
        'featured_image',
        'featured_thumbnail'
    ];

    protected $sluggable = [
        'build_from' => 'name',
        'save_to'    => 'slug',
    ];

    public function program()
    {
        $this->belongsTo('App\Program');
    }

    public function photos()
    {
        return $this->morphMany('App\Media', 'imageable');
    }

    public function delete()
    {
        $medias = $this->photos;

        foreach($medias as $media) {
            $media->delete();
        }

        parent::delete();
    }


}
