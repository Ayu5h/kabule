<?php namespace App;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Illuminate\Database\Eloquent\Model;

class Event extends Model implements SluggableInterface
{

    use SluggableTrait;

    protected $fillable = [
        'featured_image',
        'featured_thumbnail',
        'title',
        'slug',
        'description',
        'short_intro',
        'location',
        'start_date',
        'end_date',
        'start_time',
        'end_time',
        'lat',
        'lng',
        'contact_phone',
        'contact_email',
        'contact_name',
    ];

    protected $sluggable = [
        'build_from' => 'title',
        'save_to' => 'slug'
    ];

    public function photos()
    {
        return $this->morphMany('App\Media', 'imageable');
    }

    function scopeLatest($query) {
        return $query->orderBy('id', 'desc');
    }

    function date( ) {

        if ( $this->start_date == $this->end_date ) {
            $duration = date('jS F Y',  strtotime($this->start_date) )
                . " " .
                date('h:i A', strtotime(  $this->start_date ))
                . " - " .
                date('h:i A', strtotime(  $this->end_date )) ;

        } else {
            $duration = date('jS F Y',  strtotime($this->start_date) )
                . " " .
                date('h:i A', strtotime(  $this->start_date ))
                . " - " .
                date('jS F Y',  strtotime($this->end_date) ).
                " ".
                date('h:i A', strtotime(  $this->end_date )) ;
        }

        return $duration;

    }

    public function delete() {

        $medias = $this->photos;

        if(!empty($medias)) {
            foreach($medias as $media) {
                $media->delete();
            }
        }

        parent::delete();
    }

    public function scopeUpcoming($query) {
        return $query->where('start_date', '>', date('Y-m-d', time()));
    }

}
