<?php namespace App;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Illuminate\Database\Eloquent\Model;

class Album extends Model implements SluggableInterface{

	use SluggableTrait;

    protected $fillable = [
        'name',
        'created_date',
        'featured_image',
        'featured_thumbnail'
    ];

    protected $sluggable = [
        'build_from' => 'name',
        'save_to' => 'slug'
    ];

    public function photos()
    {
        return $this->morphMany('App\Media', 'imageable');
    }

    function createdDate() {
        return date('F d, Y', strtotime($this->created_date));
    }

}
