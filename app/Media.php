<?php namespace App;

use Illuminate\Support\Facades\File;
use Image;

use Illuminate\Database\Eloquent\Model;

use Symfony\Component\HttpFoundation\File\UploadedFile;

class Media extends Model {

    protected $fillable = ['path', 'name', 'thumbnail_path', 'details'];

    protected $baseDir = 'media_uploads/photos';

    //weird problem: when I uncomment below, $type property of the return object won't be set
    //protected $type = 'photos';

    protected $spoofedName = '';

    protected function setType($type) {
        $this->type = $type;

        $this->setBaseDir($type);

        return $this;
    }

    protected function setBaseDir($type) {
        $this->baseDir = str_replace('photos', $type, $this->baseDir);
        return $this;
    }

    public function imageable() {
        return $this->morphTo();
    }


    public static function named($name, $type = 'photos') {
        return (new static)->setType($type)->saveAs($name);
    }

    protected function saveAs($name) {
        $this->name = $name;
        $this->spoofedName = sprintf("%s-%s", time(), $name);

        $this->path = sprintf("%s/%s", $this->baseDir, $this->spoofedName);

        return $this;
    }

    public function move(UploadedFile $file, $width = 200, $height = 200) {
        $file->move($this->baseDir, $this->spoofedName);

        if($this->type == 'photos') $this->makeThumbnail($width, $height);


        return $this;
    }

    public function makeThumbnail($width, $height) {
        if($width != 0 && $height != 0) {
            $path = sprintf("%s/%s", $this->baseDir, $this->spoofedName);
            $this->thumbnail_path = sprintf("%s/tn-%s", $this->baseDir, $this->spoofedName);

            try {
                $resizeObj = new Resize($path);
                $resizeObj->resizeImage($width, $height);
                $resizeObj->saveImage($this->thumbnail_path, 100);

            } catch (Exception $e) {

            }

//            Image::make($this->path)->fit($width, $height)->save($this->thumbnail_path);
        }

        return $this;
    }

    //this is weird again
    public function resizeImage($width, $height) {
//        $oldPath = $this->path;

//        $this->saveAs($this->name);

//        Image::make($this->path)
//            ->fit($width, $height)
//            ->save($this->path);

//        \File::delete([
//            $oldPath
//        ]);


        try {
            $resizeObj = new Resize($this->path);
            $resizeObj->resizeImage($width, $height, 'crop');
            $resizeObj->saveImage($this->path, 100);

        } catch (Exception $e) {

        }

        return $this;
    }

    public function delete() {
        \File::delete([$this->path, $this->thumbnail_path]);

        parent::delete();
    }


    function moveYoutubeImage($vlink) {

        //large image
        @file_put_contents($this->baseDir . '/' . $this->spoofedName, fopen('http://img.youtube.com/vi/' . $vlink . '/0.jpg', 'r'));

        //small image
        $this->thumbnail_path = sprintf("%s/tn-%s", $this->baseDir, $this->spoofedName);
        @file_put_contents($this->thumbnail_path, fopen('http://img.youtube.com/vi/' . $vlink . '/1.jpg', 'r'));

        return $this;

    }


    function scopeNotVideos($query) {
        return $query->where('imageable_type', '!=', 'App\Video');
    }

    function scopePhotos($query) {
        return $query->whereType('photos');
    }

    function scopeType($query, $type) {
        return $query->whereImageableType($type);
    }

    function scopeNotTypes($query, $types) {
        return $query->whereNotIn('imageable_type', $types);
    }

    public function getSizeAttribute() {
        return round((\File::size($this->path)) / 1024, 2);
    }


    //This didn't work. Instead I declared the property spoofedName so that it wont get returned.
//    public function __destruct()
//    {
//        if(isset($this->spoofedName))
//            unset($this->spoofedName);
//    }

}
