<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Member extends Model {

    protected $fillable = ['name', 'designation', 'email', 'description', 'committee_id'];
	protected $table = "members";

    function scopeCommitteeId($query, $committeeId){
        return $query->whereCommitteeId($committeeId);
    }

    function committee(){
        return $this->belongsTo('Committee', 'committee_id');
    }

    public function photos(){
        return $this->morphMany('App\Media','imageable');
    }

}
