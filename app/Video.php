<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Video extends Model {

    protected $fillable = ['name', 'link', 'description', 'vlink'];

    public function photos()
    {
        return $this->morphMany('App\Media', 'imageable');
    }

    public function delete()
    {
        $this->deleteMedias();

        parent::delete();
    }

    function deleteMedias()
    {
        $medias = $this->photos;

        foreach($medias as $media) {
            $media->delete();
        }

        return $this;

    }

    function createdAt(){
        return date('F d, Y', strtotime($this->created_at));
    }

}
