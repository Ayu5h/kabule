<?php namespace App;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Illuminate\Database\Eloquent\Model;

class Committee extends Model implements SluggableInterface{

    use SluggableTrait;

	protected $fillable = ['name', 'has_designations', 'meta_title', 'meta_description'];
    protected $table = "committees";

    protected $sluggable = [
        'build_from' => 'name',
        'save_to'    => 'slug',
    ];

    public function members(){
        return $this->hasMany('App\Member','committee_id');
    }

    function hasDesignation() {
        return $this->has_designations == 1 ? 'YES' : 'NO';
    }

}
