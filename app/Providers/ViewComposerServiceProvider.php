<?php namespace App\Providers;

use App\Committee;
use App\Contact;
use App\Event;
use App\Media;
use App\Models\Page;
use App\Notice;
use App\Publication;
use App\SiteNews;
use App\Video;
use Illuminate\Support\ServiceProvider;
use App\WhatWeDo;
use App\Disaster;
use App\Getinvolve;
class ViewComposerServiceProvider extends ServiceProvider {

	/**
	 * Bootstrap the application services.
	 *
	 * @return void
	 */
	public function boot()
	{

        $this->composePublications();

        $this->composeNotices();
        $this->composeNewsAndEvents();
        $this->composeGallery();
        $this->composeMembers();
        $this->composeFooter();

        $this->composeMenu();

        $this->composeAdminSideBar();

	}

	/**
	 * Register the application services.
	 *
	 * @return void
	 */
	public function register()
	{
		//
	}

    private function composeAdminSideBar(){
        view()->composer('partials.admin.sidebar', function($view)
        {
            $view->with([
                'publications' => Publication::all(),
                'aboutPages' => Page::isAboutPage()->get()
            ]);

        });
    }

    private function composePublications()
    {

        view()->composer('layouts.site.partials.header', function($view)
        {
            $view->with([
                'publications' => Publication::Oldest()->get(),
            ]);
        });
    }

    private function composeNotices()
    {
        view()->composer('layouts.front', function($view)
        {
            $view->with('notices', Notice::all());
        });
    }

    private function composeNewsAndEvents()
    {
        view()->composer('layouts.front', function($view)
        {
            $view->with([
                'allNews' => SiteNews::Latest()->get(),
                'events' => Event::Latest()->limit(6)->get()
            ]);
        });
    }

    private function composeGallery()
    {
        view()->composer('front.home.gallery', function ($view){
            $view->with([
                'photos' => Media::type('App\Album')->photos()->take(7)->get()
            ]);
        });

        view()->composer('layouts.site.partials.right_sidebar.video', function ($view){
            $view->with([
                'videos' => Video::all()
            ]);
        });
    }


    private function composeMembers()
    {
        view()->composer('layouts.site.partials.right_sidebar.staff', function ($view){
            $staff = Committee::findBySlug('staff');
            $view->with([
                'members' => $staff->members
            ]);
        });
    }

    private function composeFooter()
    {
        view()->composer('layouts.site.partials.foot', function ($view){
            $view->with([
                'contact' => Contact::first(),
                'whatWeDo' => WhatWeDo::all(),
                'pages' => Page::get(['name', 'slug']),

            ]);
        });
    }

    private function composeMenu()
    {
        view()->composer('layouts.site.partials.header', function ($view){
            $view->with([
                'pages' => Page::get(['name', 'slug']),
                'whatWeDo' => WhatWeDo::all(),
                'disasters' => Disaster::all(),
                'getInvolves' => Getinvolve::all()
//                'committees' => Committee::get(['name', 'slug'])
            ]);
        });
    }

}
