<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\Page;

class PagesTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Page::truncate();

        //create pages with slug, introduction-birds, status-of-birds, important-bird-areas, about-us
        $page  = [
            'name' => 'Introduction',
            'slug' => 'introduction-birds',
            'type' => 1,
            'title' => 'Introduction to birds'
        ];

        Page::create($page);

        $page  = [
            'name' => 'Status of Birds',
            'slug' => 'status-of-birds',
            'type' => 1,
            'title' => 'Status Of Birds In Nepal'
        ];

        Page::create($page);

        $page  = [
            'name' => 'Important Bird Areas',
            'slug' => 'important-birds-areas',
            'type' => 1,
            'title' => 'Important Bird Areas of Nepal (IBAs)'
        ];

        Page::create($page);


        $page  = [
            'name' => 'About Us',
            'slug' => 'about-us',
            'type' => 0,
            'title' => 'About Bird Life Nepal'
        ];

        Page::create($page);
    }

}
