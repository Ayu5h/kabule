<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class PageContentTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\PageContent::truncate();

        //create pages with slug, introduction-birds, status-of-birds, important-bird-areas, about-us
        $page  = [
            'title' => 'Join',
            'icon' => 'fa-globe'
        ];

        \App\PageContent::create($page);

        $page  = [
            'title' => 'Act Now',
            'icon' => 'fa-group'
        ];

        \App\PageContent::create($page);

        $page  = [
            'title' => 'Donate',
            'icon' => 'fa-hand-o-right',
        ];

        \App\PageContent::create($page);

        $page  = [
            'title' => 'Welcome to Department of Agriculture',
            'icon' => '',
        ];

        \App\PageContent::create($page);
    }

}
