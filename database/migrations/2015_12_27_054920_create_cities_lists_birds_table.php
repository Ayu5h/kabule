<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCitiesListsBirdsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        Schema::create('cities_lists_birds', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('appendix');
            $table->integer('cities_category_id')->unsigned()->index();
            $table->foreign('cities_category_id')->references('id')->on('cities_lists_categories')->onDelete('cascade')->onUpdate('no action');
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {

        Schema::drop('cities_lists_birds');

    }

}
