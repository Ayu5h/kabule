<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMembersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('members', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->string('designation');
			$table->string('image');
            $table->string('image_thumbnail');
			$table->string('email');
            $table->text('description');
			$table->integer('committee_id')->unsigned()->index();
            $table->foreign('committee_id')->references('id')->on('committees')->onDelete('cascade')->onUpdate('no action');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('members');
	}

}
