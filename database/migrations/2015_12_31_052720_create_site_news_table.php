<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSiteNewsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('site_news', function(Blueprint $table)
		{
			$table->increments('id');
            $table->string('name');
            $table->string('slug');
            $table->string('featured_image');
            $table->string('featured_thumbnail');
            $table->timestamp('date');
            $table->boolean('type')->comment('0 = local, 1 = international');
            $table->string('link')->nullable();
            $table->text('description');
            $table->string('meta_title');
            $table->string('meta_description');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('site_news');
	}

}
