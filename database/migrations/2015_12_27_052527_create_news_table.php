<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('news', function(Blueprint $table)
		{
			$table->increments('id');
            $table->string('name');
            $table->timestamp('date');
            $table->string('slug');
            $table->string('featured_image');
            $table->string('featured_thumbnail');
            $table->text('description');
            $table->string('meta_title');
            $table->string('meta_description');
            $table->integer('program_id');

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('news');
	}

}
