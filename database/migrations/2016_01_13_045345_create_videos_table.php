<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVideosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('videos', function(Blueprint $table)
		{
			$table->increments('id');

            $table->string('name');
            $table->string('link');
            $table->string('vlink');
            $table->integer('category_id')->unsigned()->index();
            $table->foreign('category_id')->references('id')->on('video_categories')->onDelete('no action')->onUpdate('no action');
            $table->text('description');

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('videos');
	}

}
