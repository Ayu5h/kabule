<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubPublicationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sub_publications', function(Blueprint $table)
		{
			$table->increments('id');

            $table->integer('publication_id')->unsigned();

            $table->string('title');

            $table->text('description');

            $table->string('slug');

            $table->timestamp('date');

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('sub_publications');
	}

}
