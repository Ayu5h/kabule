<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProgramsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('programs', function(Blueprint $table)
		{
			$table->increments('id');
            $table->string('title');
            $table->string('slug');
            $table->string('featured_image');
            $table->string('featured_thumbnail');
            $table->boolean('show_on_home_page');
            $table->string('intro_text');
            $table->text('description');
            $table->string('sponsor');
            $table->string('meta_title');
            $table->string('meta_description');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('programs');
	}

}
