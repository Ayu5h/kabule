<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBirdsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('birds', function(Blueprint $table)
		{
			$table->increments('id');

            $table->string('name');
            $table->string('kingdom');
            $table->string('slug');

            $table->string('featured_image');
            $table->string('featured_thumbnail');

            $table->string('phylum');
            $table->string('class');
            $table->string('order');
            $table->string('family');
            $table->string('genus');
            $table->string('species');
            $table->string('sub_species');
            $table->string('synonym');
            $table->string('local_name');
            $table->text('distribution');
            $table->text('short_description');
            $table->text('description');
            $table->string('meta_title')->nullable();
            $table->string('meta_description')->nullable();

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('birds');
	}

}
