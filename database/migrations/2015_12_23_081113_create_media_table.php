<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMediaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('media', function(Blueprint $table)
		{
			$table->increments('id');
            $table->string('path');
            $table->string('thumbnail_path');
            $table->string('name');
            $table->string('details');
            $table->string('type')->comment('type of file(eg: photo, video, others)');
            $table->integer('imageable_id');
            $table->string('imageable_type');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('media');
	}

}
