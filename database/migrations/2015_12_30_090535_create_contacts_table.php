<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('contacts', function(Blueprint $table)
		{
			$table->increments('id');
            $table->string('address');
            $table->string('phone');
            $table->string('hotline');
            $table->string('email');
            $table->string('fax_number');
            $table->string('skype_handle');
            $table->string('location');
            $table->string('lat');
            $table->string('lng');

            $table->text('description');
            $table->string('lag');



			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('contacts');
	}

}
