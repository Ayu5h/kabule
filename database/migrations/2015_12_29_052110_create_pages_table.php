<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pages', function(Blueprint $table)
		{
			$table->increments('id');

            $table->string('name');
            $table->tinyInteger('type')->comment('0 = About Us Page, 1 = Birds Page');
            $table->boolean('is_core')->comment('0 = Core, 1 = User Made')->default(0);
            $table->string('featured_thumbnail');
            $table->string('featured_image');
            $table->string('title');
            $table->string('slug');
            $table->text('description');
            $table->string('meta_title');
            $table->string('meta_description');

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('pages');
	}

}
