<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | API Keys
    |--------------------------------------------------------------------------
    |
    | Set the public and private API keys as provided by reCAPTCHA.
    |
    | In version 2 of reCAPTCHA, public_key is the Site key,
    | and private_key is the Secret key.
    |
    */
//    'public_key'    => '6LddgxQTAAAAAPkg0YDwQ6br8Nu280yw0ja8Vuc8',
//    'private_key'    => '6LddgxQTAAAAAMTm8NdMjj1RX4T-T9ijFe_qBvl2',

    'public_key' => '6Lf-hBQTAAAAAPFq77pS1JzLd9rb9d-mTIA0zZ90',
    'private_key' => '6Lf-hBQTAAAAAMRqoAVfmaZF5k2fsxk_m0QN5-cb',

    
    /*
    |--------------------------------------------------------------------------
    | Template
    |--------------------------------------------------------------------------
    |
    | Set a template to use if you don't want to use the standard one.
    |
    */
    'template'        => '',

    /*
    |--------------------------------------------------------------------------
    | Driver
    |--------------------------------------------------------------------------
    |
    | Determine how to call out to get response; values are 'curl' or 'native'.
    | Only applies to v2.
    |    
    */    
    'driver'       => 'curl',

    /*
    |--------------------------------------------------------------------------
    | Options
    |--------------------------------------------------------------------------
    |
    | Various options for the driver
    |    
    */    
    'options'       => array(
        
        'curl_timeout' => 1,
        
    ),

    /*
    |--------------------------------------------------------------------------
    | Version
    |--------------------------------------------------------------------------
    |
    | Set which version of ReCaptcha to use.
    |    
    */    
    'version'       => 2,

);