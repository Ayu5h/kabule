<?php

return [
    'menu' => [
        'home' => 'गृह पृष्ठ',
        'aboutUs' => 'हाम्रो बारे',
        'introduction' => 'परिचय',
        'members' => 'सदस्यहरु',
        'historicalBackground' => 'ऐतिहासिक पृष्ठभूमि',
        'organizationalStructure' => 'Organizational Structure'
    ]
];

?>