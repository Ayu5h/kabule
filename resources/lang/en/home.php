<?php

return [
    'menu' => [
        'home' => 'Home Page',
        'aboutUs' => 'About Us',
        'introduction' => 'Introduction',
        'members' => 'Members',
        'historicalBackground' => 'Historical Background',
        'organizationalStructure' => 'Organizational Structure'
    ]
];

?>