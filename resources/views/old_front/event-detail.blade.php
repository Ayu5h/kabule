@extends('layouts.front')

@section('content')
    <div class="page-header">
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <h2 class="page-title">{{ $event->name }}</h2>
            </div>
            <!-- /.col-md-6 -->
            <div class="col-md-6 col-sm-6 hidden-xs back-home">
                <a href="{{ route('home') }}">&larr; Go back Home</a>
            </div>
            <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
    </div> <!-- /.page-header -->

    <div class="row">

        <div class="col-md-8">
            <div class="event-single">
                <div class="event-image">
                    @if($event->featured_image)
                        <img src="{{ asset($event->featured_image) }}" width="770" height="400" alt="">
                    @else
                        <img src="http://fakeimg.pl/770x400/?text=No Image">
                    @endif

                    <div class="img-date">
                        <span class="month">Jul</span>
                        <span class="day">16</span>
                        <span class="year">2015</span>
                    </div>
                    <!-- /.img-date -->
                </div>
                <!-- /.event-image -->
                <div class="event-content">
                    <h3>{{ $event->title }}</h3>

                    <p>{!! $event->description !!}</p>

                    <div class="space-s"></div>
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <div class="event-map" style="height: 260px;" id="event-map"></div>
                            <!-- /.event-map -->
                        </div>
                        <!-- /.col-md-6 -->
                        <div class="col-md-6 col-sm-6">
                            <ul class="event-info">
                                <li><span>Date:</span>{{ $event->start_date }}</li>
                                <li><span>Time:</span>{{ $event->start_time }} - {{ $event->end_time }}</li>
                                <li><span>Phone:</span>{{ $event->contact_phone }}</li>
                                <li><span>Emai:</span><a
                                            href="mailto:youremail@info.com">{{ $event->contact_email }}</a></li>
                            </ul>
                        </div>
                        <!-- /.col-md-6 -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.event-content -->
            </div>
            <!-- /.event-single -->
        </div>
        <!-- /.col-md-8 -->

        <div class="col-md-4">
            @if( $upcomingEvents->count() > 0 )
                <div class="box-content">
                    <h4 class="widget-title"><span>Our events</span></h4>

                    <div class="events-sidebar">
                        <ul>
                            @foreach( $upcomingEvents as $event )
                                <li class="event-item">
                                    <div class="event-thumb">
                                        @if( empty($event->featured_thumbnail) )
                                            <img src="http://placehold.it/80x80" alt="">
                                        @else
                                            <img src="{{ asset($event->featured_thumbnail) }}" alt="">
                                        @endif
                                    </div>
                                    <div class="event-content">
                                        <h5 class="event-title"><a href="{{ url('event/'.$event->slug) }}">{{ $event->title }}</a></h5>

                                        <p class="event-meta">{{ date('jS F Y', strtotime($event->start_date)) }}</p>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                        <a href="{{ url('event') }}" class="read-more">More Events &rarr;</a>
                    </div>
                    <!-- /.events-sidebar -->
                </div>
                <!-- /.box-content -->
            @endif

            @include('front.partials.advertisement-sidebar')
        </div>
        <!-- /.col-md-4 -->

    </div> <!-- /.row -->

@stop

@section('scripts.footer')
    <script src="http://maps.google.com/maps/api/js?sensor=false&libraries=places"></script>

    <script>

        function contactMapInitialize(name, address, lat, lng, mapId) {
            var mapOptions = {
                center: new google.maps.LatLng(lat, lng),
                zoom: 16
            };
            var map = new google.maps.Map(document.getElementById(mapId),
                    mapOptions);

            var infowindow = new google.maps.InfoWindow();
            var marker = new google.maps.Marker({
                map: map,
                position: mapOptions.center,
                draggable: false
            });
            marker.setMap(map);
            marker.setVisible(true);
            infowindow.setContent('<div><strong>' + name + '</strong><br>' + address);
            infowindow.open(map, marker);
        }

        $(function () {

            contactMapInitialize("{{ $event->name }}", "{{ $event->location }}", "{{ $event->lat }}", "{{ $event->lng }}", "event-map")

        });
    </script>
@stop