@extends('layouts.front')

@section('content')



    <div class="page-header">
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <h2 class="page-title">Contact Page</h2>
            </div>
            <!-- /.col-md-6 -->
            <div class="col-md-6 col-sm-6 hidden-xs back-home">
                <a href="index.html">&larr; Go back Home</a>
            </div>
            <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
    </div> <!-- /.page-header -->

    <div class="row">

        {!! Form::open(['url' => action('Front\ContactController@store'), 'method' => 'POST']) !!}
        <div class="col-md-8 contact-page">
            <div class="contact-map" id="contact-map" style="height: 380px;"></div>
            <div id="contact" class="contactForm clearfix">
                <div id="result"></div>
                <fieldset>
                    <label for="name">Name:<span>Put your name here</span></label>
                    <input type="text" id="name" name="name" value="{{ old('name') }}">
                    <span id="helpBlock2" class="help-block error">{{ $errors->first('name') }} </span>
                </fieldset>

                <fieldset>
                    <label for="email">E-mail:<span>Type email address</span></label>
                    <input type="text" id="email" name="email" value="{{ old('email') }}">
                    <span id="helpBlock2" class="help-block error">{{ $errors->first('email') }} </span>
                </fieldset>

                <fieldset>
                    <label for="subject">Subject:<span>Put your subject here</span></label>
                    <input type="text" id="subject" name="subject" value="{{ old('subject') }}">
                    <span id="helpBlock2" class="help-block error">{{ $errors->first('subject') }} </span>
                </fieldset>

                <fieldset>
                    {!! Recaptcha::render() !!}
                    <span id="helpBlock2" class="help-block error">{{ $errors->first('g-recaptcha-response') }} </span>
                </fieldset>

                <fieldset>
                    <label for="description">Message:<span>Type your message here</span></label>
                    <textarea name="description" id="description" rows="6">{{ old('description') }}</textarea>
                    <span id="helpBlock2" class="help-block error">{{ $errors->first('description') }} </span>
                </fieldset>
                <fieldset>
                    <button id="submit_btn" class="btn main-btn" type="submit" name="">Send Message</button>
                </fieldset>
            </div>
        </div>
        <!-- /.col-md-8 -->
        {!! Form::close() !!}

        <div class="col-md-4">
            <div class="box-content">
                <h4 class="widget-title"><span>Our Gallery</span></h4>

                <div class="gallery-wrapper">
                    <div class="gallery-thumb">
                        <a href="images/gallery-placeholder.png" class="fancybox" data-fancybox-group="group1">
                            <img src="http://placehold.it/80x80" alt="">
                        </a>
                    </div>
                    <!-- /.gallery-thumb -->
                    <div class="gallery-thumb">
                        <a href="images/gallery-placeholder.png" class="fancybox" data-fancybox-group="group1">
                            <img src="http://placehold.it/80x80" alt="">
                        </a>
                    </div>
                    <!-- /.gallery-thumb -->
                    <div class="gallery-thumb">
                        <a href="images/gallery-placeholder.png" class="fancybox" data-fancybox-group="group1">
                            <img src="http://placehold.it/80x80" alt="">
                        </a>
                    </div>
                    <!-- /.gallery-thumb -->
                    <div class="gallery-thumb">
                        <a href="images/gallery-placeholder.png" class="fancybox" data-fancybox-group="group1">
                            <img src="http://placehold.it/80x80" alt="">
                        </a>
                    </div>
                    <!-- /.gallery-thumb -->
                    <div class="gallery-thumb">
                        <a href="images/gallery-placeholder.png" class="fancybox" data-fancybox-group="group1">
                            <img src="http://placehold.it/80x80" alt="">
                        </a>
                    </div>
                    <!-- /.gallery-thumb -->
                    <div class="gallery-thumb">
                        <a href="images/gallery-placeholder.png" class="fancybox" data-fancybox-group="group1">
                            <img src="http://placehold.it/80x80" alt="">
                        </a>
                    </div>
                    <!-- /.gallery-thumb -->
                </div>
                <!-- /.gallery-wrapper -->
            </div>
            <!-- /.box-content -->
            <div class="box-content">
                <h4 class="widget-title"><span>Upcoming events</span></h4>

                <div class="events-sidebar">
                    <ul>
                        <li class="event-item">
                            <div class="event-thumb">
                                <img src="http://placehold.it/80x80" alt="">
                            </div>
                            <div class="event-content">
                                <h5 class="event-title"><a href="event-detail.html">Memorial Day Sale: 30% All Day
                                        Monday May 26</a></h5>

                                <p class="event-meta">02 January 2015</p>
                            </div>
                        </li>
                        <li class="event-item">
                            <div class="event-thumb">
                                <img src="http://placehold.it/80x80" alt="">
                            </div>
                            <div class="event-content">
                                <h5 class="event-title"><a href="event-detail.html">Fashion Figures Online Sale: Sizes
                                        10 - 3X</a></h5>

                                <p class="event-meta">02 January 2015</p>
                            </div>
                        </li>
                        <li class="event-item">
                            <div class="event-thumb">
                                <img src="http://placehold.it/80x80" alt="">
                            </div>
                            <div class="event-content">
                                <h5 class="event-title"><a href="event-detail.html">Speak Up! National Summit for Women
                                        Living with HIV</a></h5>

                                <p class="event-meta">02 January 2015</p>
                            </div>
                        </li>
                    </ul>
                    <a href="events-list.html" class="read-more">More Events &rarr;</a>
                </div>
                <!-- /.events-sidebar -->
            </div>
            <!-- /.box-content -->
        </div>
        <!-- /.col-md-4 -->

    </div> <!-- /.row -->


@stop

@section('scripts.footer')
    <script src="http://maps.google.com/maps/api/js?sensor=false&libraries=places"></script>

    <script>

        function contactMapInitialize(name, address, lat, lng, mapId) {
            var mapOptions = {
                center: new google.maps.LatLng(lat, lng),
                zoom: 16
            };
            var map = new google.maps.Map(document.getElementById(mapId),
                    mapOptions);

            var infowindow = new google.maps.InfoWindow();
            var marker = new google.maps.Marker({
                map: map,
                position: mapOptions.center,
                draggable: false
            });
            marker.setMap(map);
            marker.setVisible(true);
            infowindow.setContent('<div><strong>' + name + '</strong><br>' + address);
            infowindow.open(map, marker);
        }

        $(function () {

            contactMapInitialize("BCN Nepal", "{{ $contactSettings->location }}", "{{ $contactSettings->lat }}", "{{ $contactSettings->lng }}", "contact-map")

        });
    </script>
@stop