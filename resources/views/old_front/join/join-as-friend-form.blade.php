
                <div class="box-content donate-form">
                    <h4 class="widget-title"><span>Membership Option</span></h4>
                    @foreach( $friendsMembershipCategories as $friendsMembershipCategory)
                        <div class="radio">
                            <label>
                                {!! Form::radio('friends_category_id',  $friendsMembershipCategory->id , null) !!}
                                {!! $friendsMembershipCategory->name !!}
                            </label>
                        </div>
                    @endforeach

                    <h4 class="widget-title"><span>Personal Information</span></h4>
                    <fieldset>
                        <label for="name">Name: <span>Put your name here</span></label>
                        {!! Form::text('name', null, ['class' => 'form-input']) !!}
                    </fieldset>

                    <fieldset>
                        <label for="name">Date of Birth: <span>Put your date of birth here</span></label>
                        {!! Form::text('date_of_birth', null, ['class' => 'form-input']) !!}
                    </fieldset>

                    <fieldset>
                        <label for="name">Nationality: <span>Put your nationality here</span></label>
                        {!! Form::text('nationality', null, ['class' => 'form-input']) !!}
                    </fieldset>

                    <fieldset>
                        <label for="name">Profession: <span>Put your profession here</span></label>
                        {!! Form::text('profession', null, ['class' => 'form-input']) !!}
                    </fieldset>

                    <fieldset>
                        <label for="name">Designation <span>Put your designation here</span></label>
                        {!! Form::text('designation', null, ['class' => 'form-input']) !!}
                    </fieldset>

                    <fieldset>
                        <label for="name">Institution <span>Put your institution here</span></label>
                        {!! Form::text('institution', null, ['class' => 'form-input']) !!}
                    </fieldset>

                    <fieldset>
                        <label for="name">Interest <span>Put your interest here</span></label>
                        {!! Form::text('institution', null, ['class' => 'form-input']) !!}
                    </fieldset>

                    <h4 class="widget-title"><span>Contact Information</span></h4>

                    <fieldset>
                        <label for="email">Address: <span>Put your address here</span></label>
                        {!! Form::text('address', null, ['class' => 'form-input']) !!}
                    </fieldset>

                    <fieldset>
                        <label for="email">P.O Box No: <span>Put your P.O Box No here</span></label>
                        {!! Form::text('po_box', null, ['class' => 'form-input']) !!}
                    </fieldset>

                    <fieldset>
                        <label for="email">Contact Tel No: <span>Put your contact here</span></label>
                        {!! Form::text('contact_tel_no', null, ['class' => 'form-input']) !!}
                    </fieldset>

                    <fieldset>
                        <label for="email">Email: <span>Put your email here</span></label>
                        {!! Form::text('email', null, ['class' => 'form-input']) !!}
                    </fieldset>
                </div> <!-- /.donate-form -->



