@extends('layouts.front')

@section('content')
    <div class="page-header">
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <h2 class="page-title">Upcoming Events</h2>
            </div>
            <!-- /.col-md-6 -->
            <div class="col-md-6 col-sm-6 hidden-xs back-home">
                <a href="index.html">&larr; Go back Home</a>
            </div>
            <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
    </div> <!-- /.page-header -->

    <div class="row">

        <div class="col-md-8">

            @if($events->count() == 0)
                <div class="page-header">
                    <h2 class="page-title">Sorry, We don't have any events yet!</h2>
                </div>
            @else
                @foreach($events as $event)
                    <div class="event-list">
                        <div class="event-thumb">
                            @if($event->featured_thumbnail)
                                <img src="{{ asset($event->featured_thumbnail) }}" alt="">
                            @else
                                <img src="http://fakeimg.pl/200x200/?text=No Image">
                            @endif
                        </div>
                        <!-- /.event-thumb -->
                        <div class="event-content">
                            <h4 class="event-title"><a href="{{ action('Front\EventController@show', [$event->slug]) }}">{{ $event->title }}</a></h4>

                            <p>{!! nl2br($event->short_intro) !!}</p>

                            <div class="event-location">
                                <span><i class="fa fa-map-marker"></i>{{ $event->location }}</span>
                            </div>
                            <div class="event-time">
                                <span><i class="fa fa-calendar"></i>{{ $event->start_date }}</span>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <!-- /.event-content -->
                    </div>
                    <!-- /.event-list -->
                @endforeach
            @endif

            {!! $events->render() !!}

        </div>
        <!-- /.col-md-8 -->

        <div class="col-md-4">

            <div class="box-content">
                <h4 class="widget-title"><span>Upcoming News</span></h4>

                <div class="events-sidebar">
                    <ul>
                        @foreach( $allNews as $news)
                            <li class="event-item">
                                <div class="event-thumb">
                                    @if( !empty( $news->featured_thumbnail ) )
                                        <img src="{{ asset( $news->featured_thumbnail ) }}" alt="" style="height: 80px; width: 80px;">
                                    @else
                                        <img src="http://placehold.it/80x80" alt="" class="left-image">
                                    @endif
                                </div>
                                <div class="event-content">
                                    <h5 class="event-title"><a href="{{ action('Front\SiteNewsController@show', [$news->slug]) }}">{{ $news->name }}</a></h5>

                                    <p class="event-meta">{{ $news->date }}</p>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                    <a href="{{ action('Front\SiteNewsController@index') }}" class="read-more">More News &rarr;</a>
                </div>
                <!-- /.events-sidebar -->
            </div>
            <!-- /.box-content -->
            @include('front.partials.advertisement-sidebar')
        </div>
        <!-- /.col-md-4 -->

    </div> <!-- /.row -->
@stop