@extends('layouts.front')

@section('content')
    <div class="container">

        <div class="page-header">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <h2 class="page-title">{{ $page->title }}</h2>
                </div>
                <!-- /.col-md-6 -->
                <div class="col-md-6 col-sm-6 hidden-xs back-home">
                    <a href="{{ route('home') }}">&larr; Go back Home</a>
                </div>
                <!-- /.col-md-6 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.page-header -->

        <div class="row">

            <div class="col-md-8">
                <div class="box-content">
                    <img src="{{ asset($page->featured_image) }}" alt=""
                         style="float: left; margin: 0px 20px 10px 0px"/>
                    {!! $page->description !!}

                    <div class="gallery-wrapper">

                        @foreach( $images as $image )
                            @if( $image->thumbnail_path != $page->featured_thumbnail )
                                <div class="gallery-thumb">
                                    <a href="{{ $image->path }}" class="fancybox" data-fancybox-group="group-about">
                                        <img src="{{ $image->thumbnail_path }}" alt="">
                                    </a>
                                </div>
                            @endif
                        @endforeach

                    </div>
                </div>
            </div>
            <!-- /.col-md-8 -->

            <div class="col-md-4">
                <div class="box-content">
                    <h4 class="widget-title"><span>Our Gallery</span></h4>

                    <div class="gallery-wrapper">
                        <div class="gallery-thumb">
                            <a href="images/gallery-placeholder.png" class="fancybox" data-fancybox-group="group1">
                                <img src="http://placehold.it/80x80" alt="">
                            </a>
                        </div>
                        <!-- /.gallery-thumb -->
                        <div class="gallery-thumb">
                            <a href="images/gallery-placeholder.png" class="fancybox" data-fancybox-group="group1">
                                <img src="http://placehold.it/80x80" alt="">
                            </a>
                        </div>
                        <!-- /.gallery-thumb -->
                        <div class="gallery-thumb">
                            <a href="images/gallery-placeholder.png" class="fancybox" data-fancybox-group="group1">
                                <img src="http://placehold.it/80x80" alt="">
                            </a>
                        </div>
                        <!-- /.gallery-thumb -->
                        <div class="gallery-thumb">
                            <a href="images/gallery-placeholder.png" class="fancybox" data-fancybox-group="group1">
                                <img src="http://placehold.it/80x80" alt="">
                            </a>
                        </div>
                        <!-- /.gallery-thumb -->
                        <div class="gallery-thumb">
                            <a href="images/gallery-placeholder.png" class="fancybox" data-fancybox-group="group1">
                                <img src="http://placehold.it/80x80" alt="">
                            </a>
                        </div>
                        <!-- /.gallery-thumb -->
                        <div class="gallery-thumb">
                            <a href="images/gallery-placeholder.png" class="fancybox" data-fancybox-group="group1">
                                <img src="http://placehold.it/80x80" alt="">
                            </a>
                        </div>
                        <!-- /.gallery-thumb -->
                    </div>
                    <!-- /.gallery-wrapper -->
                </div>
                <!-- /.box-content -->
                <div class="box-content categories">
                    <h4 class="widget-title"><span>Related Links</span></h4>
                    <div class="row">
                        <div class="col-md-6">
                            <ul>
                                <li><a href="#">Awwards</a></li>
                                <li><a href="#">Books</a></li>
                                <li><a href="#">Branding</a></li>
                                <li><a href="#">Creativity</a></li>
                                <li><a href="#">Fundraising</a></li>
                            </ul>
                        </div>
                        <div class="col-md-6">
                            <ul>
                                <li><a href="#">Net Stuff</a></li>
                                <li><a href="#">Demographics</a></li>
                                <li><a href="#">Community</a></li>
                                <li><a href="#">Generosity</a></li>
                            </ul>
                        </div>
                    </div> <!-- /.row -->
                </div> <!-- /.box-content -->
                <!-- /.box-content -->
            </div>
            <!-- /.col-md-4 -->

        </div>
        <!-- /.row -->

    </div> <!-- /.container -->
@stop