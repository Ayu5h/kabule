@extends('layouts.front')

@section('content')
    <div class="container">

        <div class="page-header">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <h2 class="page-title">Cities Listed Birds Of Nepal</h2>
                </div>
                <!-- /.col-md-6 -->
                <div class="col-md-6 col-sm-6 hidden-xs back-home">
                    <a href="index.html">&larr; Go back Home</a>
                </div>
                <!-- /.col-md-6 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.page-header -->

        <div class="row">

            <div class="col-md-8">
                <div class="box-content table-responsive">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>Order/ Family/ Common name/ Scientific name</th>
                            <th>Cities Index</th>
                        </tr>
                        @foreach( $categories as $category )
                            <tr>
                                <th colspan="2">{{ $category->name }}</th>
                            </tr>
                            @foreach( $category->listedBirds as $bird )
                                <tr>
                                    <td>{{ $bird->name }}</td>
                                    <td>{{ $bird->appendix }}</td>
                                </tr>
                            @endforeach
                        @endforeach`
                    </table>
                </div>
            </div>
            <!-- /.col-md-8 -->

            <div class="col-md-4">
                <div class="box-content categories">
                    <h4 class="widget-title"><span>Categories</span></h4>

                    <div class="row">
                        <div class="col-md-6">
                            <ul>
                                <li><a href="#">Awwards</a></li>
                                <li><a href="#">Books</a></li>
                                <li><a href="#">Branding</a></li>
                                <li><a href="#">Creativity</a></li>
                                <li><a href="#">Fundraising</a></li>
                            </ul>
                        </div>
                        <div class="col-md-6">
                            <ul>
                                <li><a href="#">Net Stuff</a></li>
                                <li><a href="#">Demographics</a></li>
                                <li><a href="#">Community</a></li>
                                <li><a href="#">Generosity</a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.box-content -->
                <div class="box-content">
                    <h4 class="widget-title"><span>Our Gallery</span></h4>

                    <div class="gallery-wrapper">
                        <div class="gallery-thumb">
                            <a href="images/gallery-placeholder.png" class="fancybox" data-fancybox-group="group1">
                                <img src="http://placehold.it/80x80" alt="">
                            </a>
                        </div>
                        <!-- /.gallery-thumb -->
                        <div class="gallery-thumb">
                            <a href="images/gallery-placeholder.png" class="fancybox" data-fancybox-group="group1">
                                <img src="http://placehold.it/80x80" alt="">
                            </a>
                        </div>
                        <!-- /.gallery-thumb -->
                        <div class="gallery-thumb">
                            <a href="images/gallery-placeholder.png" class="fancybox" data-fancybox-group="group1">
                                <img src="http://placehold.it/80x80" alt="">
                            </a>
                        </div>
                        <!-- /.gallery-thumb -->
                        <div class="gallery-thumb">
                            <a href="images/gallery-placeholder.png" class="fancybox" data-fancybox-group="group1">
                                <img src="http://placehold.it/80x80" alt="">
                            </a>
                        </div>
                        <!-- /.gallery-thumb -->
                        <div class="gallery-thumb">
                            <a href="images/gallery-placeholder.png" class="fancybox" data-fancybox-group="group1">
                                <img src="http://placehold.it/80x80" alt="">
                            </a>
                        </div>
                        <!-- /.gallery-thumb -->
                        <div class="gallery-thumb">
                            <a href="images/gallery-placeholder.png" class="fancybox" data-fancybox-group="group1">
                                <img src="http://placehold.it/80x80" alt="">
                            </a>
                        </div>
                        <!-- /.gallery-thumb -->
                    </div>
                    <!-- /.gallery-wrapper -->
                </div>
                <!-- /.box-content -->
                <div class="box-content">
                    <h4 class="widget-title"><span>Upcoming events</span></h4>

                    <div class="events-sidebar">
                        <ul>
                            <li class="event-item">
                                <div class="event-thumb">
                                    <img src="http://placehold.it/80x80" alt="">
                                </div>
                                <div class="event-content">
                                    <h5 class="event-title"><a href="event-detail.html">Memorial Day Sale: 30% All Day
                                            Monday May 26</a></h5>

                                    <p class="event-meta">02 January 2015</p>
                                </div>
                            </li>
                            <li class="event-item">
                                <div class="event-thumb">
                                    <img src="http://placehold.it/80x80" alt="">
                                </div>
                                <div class="event-content">
                                    <h5 class="event-title"><a href="event-detail.html">Fashion Figures Online Sale:
                                            Sizes 10 - 3X</a></h5>

                                    <p class="event-meta">02 January 2015</p>
                                </div>
                            </li>
                            <li class="event-item">
                                <div class="event-thumb">
                                    <img src="http://placehold.it/80x80" alt="">
                                </div>
                                <div class="event-content">
                                    <h5 class="event-title"><a href="event-detail.html">Speak Up! National Summit for
                                            Women Living with HIV</a></h5>

                                    <p class="event-meta">02 January 2015</p>
                                </div>
                            </li>
                        </ul>
                        <a href="events-list.html" class="read-more">More Events &rarr;</a>
                    </div>
                    <!-- /.events-sidebar -->
                </div>
                <!-- /.box-content -->
            </div>
            <!-- /.col-md-4 -->

        </div>
        <!-- /.row -->

    </div> <!-- /.container -->
@stop