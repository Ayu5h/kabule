@extends('layouts.front')

@section('content')

    <div class="page-header">
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <h2 class="page-title">{{ $donationProgram->name }}</h2>
            </div> <!-- /.col-md-6 -->
            <div class="col-md-6 col-sm-6 hidden-xs back-home">
                <a href="#">&larr; Go back Home</a>
            </div> <!-- /.col-md-6 -->
        </div> <!-- /.row -->
    </div> <!-- /.page-header -->

    <div class="row">
        <div class="col-md-8">
            <div class="cause-single">
                <div class="cause-image">
                    <a href="donate.html" class="btn main-btn visible-xs">Donate Now</a>
                    <img src="{{ asset($donationProgram->featured_image) }}" alt="">
                    <div class="cause-overlay hidden-xs">
                        <div class="overlay-inner">
                            <span class="meta-date"><i class="fa fa-calendar-o"></i>24 April 2014</span>
                            <span class="meta-author"><i class="fa fa-user"></i>Esmet Hajrizi</span>
                            <span class="price">Needed Donation <em>$24,000</em></span>
                            <div class="text-center">
                                <a href="#" class="btn main-btn">Donate Now</a>
                            </div>
                        </div>
                    </div> <!-- /.cause-overlay -->
                </div> <!-- /.cause-image -->
                <div class="cause-content">
                    <div class="cause-holder-list clearfix">
                        <div class="progress">
                            <div class="progress-bar" role="progressbar" aria-valuenow="41" aria-valuemin="0" aria-valuemax="100" style="width: 41%;"></div>
                        </div>
                        <span class="raised pull-left">Raised: $2,400</span>
                        <span class="goal pull-right">Goal: $6,180</span>
                    </div> <!-- /.cause-holder-list -->
                    <span class="meta-cause visible-xs">24 January 2014 by Esmet Hajrizi</span>
                    <h3 class="cause-title">{{ $donationProgram->name }}</h3>
                    <p>{!! $donationProgram->description !!}</p>

                    @if( !empty($donations))
                        <h2>List of Donors</h2>
                        <table class="table table-bordered table-striped datatable">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Donated Amount</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach( $donations as $donation)
                                <tr>
                                    <th>{{ $donation->name }}</th>
                                    <td><strong>{{ $donation->amount }}</strong></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @endif

                </div> <!-- /.cause-content -->
            </div> <!-- /.cause-single -->

        </div> <!-- /.col-md-8 -->

        <div class="col-md-4">
            <div class="box-content">
                <h4 class="widget-title"><span>Our Gallery</span></h4>
                <div class="gallery-wrapper">
                    <div class="gallery-thumb">
                        <a href="images/gallery-placeholder.png" class="fancybox" data-fancybox-group="group1">
                            <img src="http://placehold.it/80x80" alt="">
                        </a>
                    </div> <!-- /.gallery-thumb -->
                    <div class="gallery-thumb">
                        <a href="images/gallery-placeholder.png" class="fancybox" data-fancybox-group="group1">
                            <img src="http://placehold.it/80x80" alt="">
                        </a>
                    </div> <!-- /.gallery-thumb -->
                    <div class="gallery-thumb">
                        <a href="images/gallery-placeholder.png" class="fancybox" data-fancybox-group="group1">
                            <img src="http://placehold.it/80x80" alt="">
                        </a>
                    </div> <!-- /.gallery-thumb -->
                    <div class="gallery-thumb">
                        <a href="images/gallery-placeholder.png" class="fancybox" data-fancybox-group="group1">
                            <img src="http://placehold.it/80x80" alt="">
                        </a>
                    </div> <!-- /.gallery-thumb -->
                    <div class="gallery-thumb">
                        <a href="images/gallery-placeholder.png" class="fancybox" data-fancybox-group="group1">
                            <img src="http://placehold.it/80x80" alt="">
                        </a>
                    </div> <!-- /.gallery-thumb -->
                    <div class="gallery-thumb">
                        <a href="images/gallery-placeholder.png" class="fancybox" data-fancybox-group="group1">
                            <img src="http://placehold.it/80x80" alt="">
                        </a>
                    </div> <!-- /.gallery-thumb -->
                </div> <!-- /.gallery-wrapper -->
            </div> <!-- /.box-content -->
            <div class="box-content">
                <h4 class="widget-title"><span>Upcoming events</span></h4>
                <div class="events-sidebar">
                    <ul>
                        <li class="event-item">
                            <div class="event-thumb">
                                <img src="http://placehold.it/80x80" alt="">
                            </div>
                            <div class="event-content">
                                <h5 class="event-title"><a href="event-detail.html">Memorial Day Sale: 30% All Day Monday May 26</a></h5>
                                <p class="event-meta">02 January 2015</p>
                            </div>
                        </li>
                        <li class="event-item">
                            <div class="event-thumb">
                                <img src="http://placehold.it/80x80" alt="">
                            </div>
                            <div class="event-content">
                                <h5 class="event-title"><a href="event-detail.html">Fashion Figures Online Sale: Sizes 10 - 3X</a></h5>
                                <p class="event-meta">02 January 2015</p>
                            </div>
                        </li>
                        <li class="event-item">
                            <div class="event-thumb">
                                <img src="http://placehold.it/80x80" alt="">
                            </div>
                            <div class="event-content">
                                <h5 class="event-title"><a href="event-detail.html">Speak Up! National Summit for Women Living with HIV</a></h5>
                                <p class="event-meta">02 January 2015</p>
                            </div>
                        </li>
                    </ul>
                    <a href="events-list.html" class="read-more">More Events &rarr;</a>
                </div> <!-- /.events-sidebar -->
            </div> <!-- /.box-content -->
        </div> <!-- /.col-md-4 -->

    </div> <!-- /.row -->

    </div> <!-- /.container -->

@stop

@section('scripts.footer')

    <script>
        $(function(){
            $('.datatable').dataTable({
                "pageLength": 2
            });
        });
    </script>

@stop