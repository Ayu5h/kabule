
        <div class="box-content donate-form">
            <h4 class="widget-title"><span>Personal Information</span></h4>

                <fieldset>
                    <label for="name">Name: <span>Put your name here</span></label>
                    {!! Form::text('name', null, ['class' => 'form-input']) !!}
                </fieldset>

                <fieldset>
                    <label for="email">Address: <span>Put your address here</span></label>
                    {!! Form::text('address', null, ['class' => 'form-input']) !!}
                </fieldset>

                <fieldset>
                    <label for="email">Contact: <span>Put your address here</span></label>
                    {!! Form::text('contact', null, ['class' => 'form-input']) !!}
                </fieldset>

                <fieldset>
                    <label for="email">Email: <span>Put your email here</span></label>
                    {!! Form::text('email', null, ['class' => 'form-input']) !!}
                </fieldset>

                <fieldset>
                    <label for="email">Amount: <span>Put your amount here</span></label>
                    {!! Form::text('amount', null, ['class' => 'form-input']) !!}
                </fieldset>



                <fieldset>
                    {!! Form::hidden('donation_program_id',$donationProgram->id, null, ['class' => 'form-input']) !!}
                </fieldset>

                <fieldset class="radio">
                    {!! Form::radio('account_type','Deposit in BCN bank account') !!}
                    <label for="showname" class="radio">{{ 'Deposit In BCN Bank Account' }}</label>
                </fieldset>

                <fieldset class="radio">
                    {!! Form::radio('account_type','Drop in BCN') !!}
                    <label for="showname" class="radio">{{ 'Drop in BCN' }}</label>
                </fieldset>
        </div> <!-- /.donate-form -->