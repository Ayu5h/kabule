@extends('layouts.front')

@section('content')

    <div class="container">

        <div class="page-header">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <h2 class="page-title">Donation</h2>
                </div> <!-- /.col-md-6 -->
                <div class="col-md-6 col-sm-6 hidden-xs back-home">
                    <a href="#">&larr; Go back Home</a>
                </div> <!-- /.col-md-6 -->
            </div> <!-- /.row -->
        </div> <!-- /.page-header -->

        <div class="row">

        </div> <!-- /.row -->
        <div class="row">
            @foreach( $donationPrograms as $donationProgram)
                <div class="col-md-4 col-sm-6 cause-grid">
                    <div class="cause-thumb">
                        <img src="{{ asset($donationProgram->featured_thumbnail) }}" alt="">
                        <div class="cause-hover">
                            <div class="cause-holder clearfix">
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="41" aria-valuemin="0" aria-valuemax="100" style="width: 41%;"></div>
                                </div>
                                <span class="raised pull-left">Raised: NRs0000</span>
                                <span class="goal pull-right">Goal: NRs0000</span>
                            </div>
                        </div>
                    </div> <!-- /.cause-thumb -->
                    <div class="cause-content">
                        <h4 class="cause-title"><a href="{{ route('donationProgramDetail',[$donationProgram->id]) }}">{{ $donationProgram->name }}</a></h4>
                        <p>{!! substr($donationProgram->description,0,120) !!}
                            <a href="{{ route('donationProgramDetail',[$donationProgram->id]) }}">Read More...</a>
                        </p>
                        <a href="{{ route('donateNow',[$donationProgram->id]) }}" class="btn main-btn">Donate Now</a>
                    </div> <!-- /.cause-content -->
                </div> <!-- /.col-md-4 -->
            @endforeach
        </div> <!-- /.row -->

        <div class="row">
            <div class="col-md-12">
                {!! $donationPrograms->render() !!}
            </div> <!-- /.col-md-12 -->
        </div> <!-- /.row -->
    </div> <!-- /.container -->

@stop