@extends('layouts.front')
@section('content')

        <div class="page-header">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <h2 class="page-title">Publication</h2>
                </div> <!-- /.col-md-6 -->
                <div class="col-md-6 col-sm-6 hidden-xs back-home">
                    <a href="#">&larr; Go back Home</a>
                </div> <!-- /.col-md-6 -->
            </div> <!-- /.row -->
        </div> <!-- /.page-header -->

            <div class="publication">
                @foreach( $publications as $publication )
                <ul style="margin:0px;">
                    <li>
                        <a href="{{ route('publicationDetail', [$publication->id]) }}">
                            <img src="{{ asset($publication->image_thumbnail) }}" width="110">
                        </a>

                        <p style="line-height:14px; text-align:left; margin-bottom:2px;">{{ $publication->title }}</p>
                        <p style="line-height:14px; text-align:left; margin-bottom:2px;">{{ 'Price:'.' '.'NRs'.' '.$publication->price }}</p>
                        <a href="{{ route('publicationDetail',[$publication->id]) }}">Read More</a>
                    </li>
                </ul>
                @endforeach
            </div>

@stop
