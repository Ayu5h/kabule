@extends('layouts.front')

@section('content')
    <div class="page-header">
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <h2 class="page-title">News Single</h2>
            </div>
            <!-- /.col-md-6 -->
            <div class="col-md-6 col-sm-6 hidden-xs back-home">
                <a href="{{ route('home') }}">&larr; Go back Home</a>
            </div>
            <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
    </div> <!-- /.page-header -->

    <div class="row">

        <div class="col-md-8">
            <div class="event-single">
                <div class="event-image">
                    @if($news->featured_image)
                        <img src="{{ asset($news->featured_image) }}" width="770" height="400" alt="">
                    @else
                        <img src="http://fakeimg.pl/770x400/?text=No Image">
                    @endif

                        <div class="img-date">
                            <span class="month">Jul</span>
                            <span class="day">16</span>
                            <span class="year">2015</span>
                        </div>
                        <!-- /.img-date -->
                </div>
                <!-- /.event-image -->
                <div class="event-content">
                    <h3>{{ $news->title }}</h3>

                    <p>{!! $news->description !!}</p>

                    {{--<div class="space-s"></div>--}}
                    {{--<div class="row">--}}
                    {{--<div class="col-md-6 col-sm-6">--}}
                    {{--<div class="event-map" style="height: 260px;"></div>--}}
                    {{--<!-- /.event-map -->--}}
                    {{--</div>--}}
                    {{--<!-- /.col-md-6 -->--}}
                    {{--<div class="col-md-6 col-sm-6">--}}
                    {{--<ul class="event-info">--}}
                    {{--<li><span>Date:</span>{{ $news->start_date }}</li>--}}
                    {{--<li><span>Time:</span>{{ $news->start_time }} - {{ $news->end_time }}</li>--}}
                    {{--<li><span>Phone:</span>{{ $news->contact_phone }}</li>--}}
                    {{--<li><span>Emai:</span><a href="mailto:youremail@info.com">{{ $news->contact_email }}</a></li>--}}
                    {{--</ul>--}}
                    {{--</div>--}}
                    {{--<!-- /.col-md-6 -->--}}
                    {{--</div>--}}
                    {{--<!-- /.row -->--}}
                </div>
                <!-- /.event-content -->
            </div>
            <!-- /.event-single -->
            <div class="row">
                <div class="col-md-12">
                    <a href="javascript:void(0);" class="go-prev"><i class="fa fa-long-arrow-left"></i>Prev Event</a>
                    <a href="javascript:void(0);" class="go-next">Next Event <i class="fa fa-long-arrow-right"></i></a>
                </div>
                <!-- /.col-md-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.col-md-8 -->

        <div class="col-md-4">
            @if( $upcomingNews->count() > 0 )
                <div class="box-content">
                    <h4 class="widget-title"><span>Our News</span></h4>

                    <div class="events-sidebar">
                        <ul>
                            @foreach( $upcomingNews as $news )
                                <li class="event-item">
                                    <div class="event-thumb">
                                        @if( empty($news->featured_thumbnail) )
                                            <img src="http://placehold.it/80x80" alt="">
                                        @else
                                            <img src="{{ asset($news->featured_thumbnail) }}" alt="">
                                        @endif
                                    </div>
                                    <div class="event-content">
                                        <h5 class="event-title"><a href="{{ url('news/'.$news->slug) }}">{{ $news->name }}</a></h5>

                                        {{--<p class="event-meta">{{ date('jS F Y', strtotime($event->start_date)) }}</p>--}}
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                        <a href="{{ action('Front\SiteNewsController@index') }}" class="read-more">More News &rarr;</a>
                    </div>
                    <!-- /.events-sidebar -->
                </div>
                <!-- /.box-content -->
            @endif

            @include('front.partials.advertisement-sidebar')
        </div>
        <!-- /.col-md-4 -->

    </div> <!-- /.row -->


@stop