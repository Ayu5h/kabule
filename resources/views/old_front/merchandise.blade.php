@extends('layouts.front')
@section('content')

    <div class="page-header">
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <h2 class="page-title">Merchandise</h2>
            </div> <!-- /.col-md-6 -->
            <div class="col-md-6 col-sm-6 hidden-xs back-home">
                <a href="#">&larr; Go back Home</a>
            </div> <!-- /.col-md-6 -->
        </div> <!-- /.row -->
    </div> <!-- /.page-header -->

    <div class="publication">
        @foreach( $merchandises as $merchandise )
            <ul style="margin:0px;">
                <li>
                    <a href="{{ route('merchandiseDetail', [$merchandise->id]) }}">
                        <img src="{{ asset($merchandise->image_thumbnail) }}" width="110">
                    </a>

                    <p style="line-height:14px; text-align:left; margin-bottom:2px;">{{ $merchandise->name }}</p>
                    <p style="line-height:14px; text-align:left; margin-bottom:2px;">{{ 'Price:'.' '.'NRs'.' '.$merchandise->price }}</p>
                    <a href="{{ route('merchandiseDetail',[$merchandise->id]) }}">Read More</a>
                </li>
            </ul>
        @endforeach
    </div>

@stop
