@extends('layouts.front')

@section('content')

    <div class="top-content">
        {{--<div class="row">--}}
            {{--<div class="col-md-12">--}}
                {{--<div class="u-cause-wrapper hidden-xs">--}}
			                {{--<span class="u-cause-title">--}}
			                	{{--<a href="#" class="close-u-cause">--}}
                                    {{--<i class="fa fa-times"></i>--}}
                                {{--</a>--}}
			                	{{--Urgent Cause--}}
			                {{--</span>--}}

                    {{--<div class="u-cause clearfix">--}}
                        {{--<dl class="inner-carousel clearfix">--}}
                            {{--<dt></dt>--}}
                            {{--<dd><a href="cause-detail.html">In non nunc nulla elementum metus</a> - Some other details--}}
                                {{--goes here--}}
                            {{--</dd>--}}
                            {{--<dt></dt>--}}
                            {{--<dd><a href="cause-detail.html">Ut ut tortor lobortis, auctor orcw diam</a> - Some other--}}
                                {{--details goes here--}}
                            {{--</dd>--}}
                            {{--<dt></dt>--}}
                            {{--<dd><a href="cause-detail.html">In at felis non nunc volutpat nec felis</a> - Some other--}}
                                {{--details goes here--}}
                            {{--</dd>--}}
                            {{--<dt></dt>--}}
                            {{--<dd><a href="cause-detail.html">In non nunc nulla elementum metus</a> - Some other details--}}
                                {{--goes here--}}
                            {{--</dd>--}}
                            {{--<dt></dt>--}}
                            {{--<dd><a href="cause-detail.html">Ut ut tortor lobortis, auctor orcw diam</a> - Some other--}}
                                {{--details goes here--}}
                            {{--</dd>--}}
                            {{--<dt></dt>--}}
                            {{--<dd><a href="cause-detail.html">In at felis non nunc volutpat nec felis</a> - Some other--}}
                                {{--details goes here--}}
                            {{--</dd>--}}
                        {{--</dl>--}}
                        {{--<!-- /.inner-carousel -->--}}
                    {{--</div>--}}
                    {{--<!-- /.u-cause -->--}}
                {{--</div>--}}
                {{--<!-- /.u-cause-wrapper -->--}}
            {{--</div>--}}
            {{--<!-- /.col-md-12 -->--}}
        {{--</div>--}}
        <!-- /.row -->

        <div class="slider-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <div class="flexslider">
                        <ul class="slides">
                            @foreach( $banners as $banner )
                                <li>
                                    <img src="{{ $banner->image }}" alt="" style="height: 400px;">

                                    <div class="flex-caption">
                                        <p> {{ $banner->title }}
                                            <a href="{{ $banner->has_link ? $banner->link : route('bannerDetail', $banner->slug) }}">Read More</a>
                                        </p>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    <!-- /.flexslider -->
                </div>
                <!-- /.col-md-8 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.slider-wrapper -->

    </div>
    <!-- /.top-content -->

    <div class="services box-content">
        <div class="row">
            <div class="col-md-offset-2 col-md-8 services-header">
                <h5 class="widget-title">{{ $welcomeContent->title }}</h5>
                <span></span>


                <p>{{ $welcomeContent->description }}.</p>
            </div>
            <!-- /.services-header -->
        </div>
        <!-- /.row -->
        <div class="row">
            @foreach($pageContents as $k => $pageContent)
                @if($pageContent->id != 4)
                    <div class="col-md-4 service-item">
                        <div class="service-icon">
                            <i class="fa {{ $pageContent->icon }}"></i>
                        </div>
                        <div class="service-content">
                            <h4 class="service-title">{{ $pageContent->title }}</h4>

                            <p>{{ $pageContent->description }}.</p>


                            <a style="color: #ffffff" href="{{ $k == 0 ? route('join') : ($k == 2 ? route('donation') : '') }}"
                                    class="btn {{ $k == 0 ? 'primary-btn-new' : ($k == 1 ? 'red-btn' : 'yellow-btn') }}">

                                {{ $pageContent->title }} &rarr;</a>
                        </div>

                    </div>
                @endif
            @endforeach
        </div>
        <!-- /.row -->
    </div>
    <!-- /.services -->

    <div class="row">
        <div class="col-md-9">
            <div class="last-causes box-content">
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="widget-title"><span>Program & Projects</span></h4>
                    </div>
                    <!-- /.col-md-12 -->
                </div>
                <!-- /.row -->
                <div class="row">
                    @foreach( $programs as $program )
                        <div class="col-md-4 col-sm-6 cause-grid">
                            <div class="cause-thumb">
                                <img src="{{ $program->featured_thumbnail }}"
                                     height="150px" alt="">

                                <div class="cause-hover">
                                    <div class="cause-holder clearfix">
                                        <div class="progress">
                                            <div class="progress-bar" role="progressbar" aria-valuenow="41"
                                                 aria-valuemin="0"
                                                 aria-valuemax="100" style="width: 41%;"></div>
                                        </div>
                                        <span class="raised pull-left">Support from </span>
                                        <span class="goal pull-right">{{ $program->sponsor }}</span>
                                    </div>
                                </div>
                            </div>
                            <!-- /.cause-thumb -->
                            <div class="cause-content">
                                <h4 class="cause-title"><a href="cause-detail.html">{{ $program->title }}</a></h4>

                                <p>{{ $program->intro_text }}.</p>
                                <a href="{{ route('viewProject', $program->slug) }}" class="btn main-btn">Read More</a>
                            </div>
                            <!-- /.cause-content -->
                        </div>
                        <!-- /.col-md-4 -->
                    @endforeach
                </div>
                <!-- /.row -->
            </div>
        </div>
        <div class="col-md-3">
            <div class="box-content">
                <h4 class="widget-title"><span>Upcoming events</span></h4>

                <div class="events-sidebar">
                    <ul>
                        @foreach( $events as $event )
                            <li class="event-item">
                                <div class="event-content">
                                    <h5 class="event-title">
                                        <a href="event-detail.html">{{ $event->title }}</a>
                                    </h5>

                                    <p class="event-meta">{{ $event->date() }}</p>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                    <a href="{{ route('events') }}" class="read-more">More Events &rarr;</a>
                </div>
                <!-- /.events-sidebar -->
            </div>
            <!-- /.box-content -->

        </div>
        <!-- /.col-md-4 -->
    </div>
    <!-- /.last-causes -->

    <div class="row">
        <div class="col-md-8">
            <div class="box-content">
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="widget-title"><span>Latest News</span></h4>
                    </div>
                </div>
                <div class="row">
                    @foreach( $siteNews as $siteNew )
                        <div class="col-md-6 col-sm-6 post-grid">
                            <div class="post-thumb">
                                <a href="blog-single.html">
                                    <img src="{{ $siteNew->featured_thumbnail }}"
                                         height="200px" alt="">
                                </a>
                            </div>
                            <!-- /.post-thumb -->
                            <div class="post-content">
                                <p class="post-meta"><a href="#">24 June 2014</a></p>
                                <h4 class="post-title" style="text-transform: inherit">
                                    <a href="blog-single.html"
                                       title="{{ $siteNew->name }}">
                                        {!! substr($siteNew->name, 0, 200) !!}</a>
                                </h4>
                                <a href="{{ action('Front\SiteNewsController@show', [$siteNew->slug]) }}" class="read-more">Continue Reading &rarr;</a>
                            </div>
                            <!-- /.post-content -->
                        </div>
                        @endforeach
                                <!-- /.post-grid -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.box-content -->
            <div class="box-content">
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="widget-title"><span>Our Partners</span></h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 testimonials-wrapper">
                        <ul class="testi-tabs clearfix">
                            @foreach( $partners as $k => $partner )
                                <li class="{{ $k == 0 ? 'active' : '' }}">
                                    <a href="#testi-content-0{{ ++$k }}">
                                        <img src="{{ $partner->image_thumbnail }}" alt="">
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                        <div class="testi-wrapper">
                            @foreach( $partners as $k => $partner )
                                <div class="testi-content {{ $k == 0 ? 'active' : '' }}" id="testi-content-0{{ ++$k }}">
                                    <div class="testi-author">
                                        <h6>{{ $partner->title }}</h6>
                                    </div>
                                    <p>
                                        {!! substr($partner->description, 0, 200) !!}
                                    </p>
                                </div>
                            @endforeach
                        </div>
                        <!-- //.testi-wrapper -->
                    </div>
                    <!-- /.col-md-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.box-content -->
        </div>
        <!-- /.col-md-8 -->
        <div class="col-md-4">
            <div class="box-content">
                <h4 class="widget-title"><span>Our Gallery</span></h4>

                <div class="gallery-wrapper">
                    @foreach( $images->chunk(3) as $imagesAll )
                        @foreach( $imagesAll as $image )
                            <div class="gallery-thumb">
                                <a href="{{ $image->path }}" class="fancybox" data-fancybox-group="group1">
                                    <img src="{{ $image->thumbnail_path }}" alt="">
                                </a>
                            </div>
                        @endforeach
                    @endforeach
                </div>
                <!-- /.gallery-wrapper -->
            </div>
            @include('front.partials.advertisement-sidebar')
        </div>
    </div>
    <!-- /.row -->

@stop