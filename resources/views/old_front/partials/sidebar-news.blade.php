<div class="box-content">
    <h4 class="widget-title"><span>Upcoming Project News</span></h4>

    <div class="events-sidebar">
        <ul>
            @foreach( $objects as $object)
                <li class="event-item">
                    <div class="event-thumb">
                        @if( !empty( $object->featured_thumbnail ) )
                        <img src="{{ asset( $object->featured_thumbnail ) }}" alt="" style="height: 80px; width: 80px;">
                        @else
                            <img src="http://placehold.it/80x80" alt="" class="left-image">
                        @endif
                    </div>
                    <div class="event-content">
                        <h5 class="event-title"><a href="{{ route('projectNews',[$object->program->slug, $object->slug]) }}">{{ $object->name }}</a></h5>
                    </div>
                </li>
            @endforeach
        </ul>
        <a href="{{ route('allProjectNews') }}" class="read-more">More Project News &rarr;</a>
    </div>
    <!-- /.events-sidebar -->
</div>
<!-- /.box-content -->