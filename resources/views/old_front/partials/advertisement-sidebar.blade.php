<div class="advertisement">
    <ul>
        <li><a href="{{ route('joinAsFriends') }}">
                <img width="365" src="http://www.birdlifenepal.org/images/friends-of-bcn.jpg"></a></li>
        <li><a target="_blank"
               href="http://www.birdlifenepal.org/_uploaded_image/_publication/_file/bibilography_of_bird_nepal.doc">
                <img width="180"
                     src="http://www.birdlifenepal.org/images/bibilography_of_bird_nepal.jpg"></a></li>
        <li><a target="_blank"
               href="http://www.birdlifenepal.org/_uploaded_image/_publication/_file/bibilography_of_danphe.doc">
                <img width="180" src="http://www.birdlifenepal.org/images/bibilography_of_denphe.jpg"></a>
        </li>
        @if( !empty($advertisement->image) )
            <li><a target="_blank" href="{{ $advertisement->link }}">
                    <img width="365" src="{{ asset($advertisement->image) }}"></a></li>
        @endif
    </ul>
</div>