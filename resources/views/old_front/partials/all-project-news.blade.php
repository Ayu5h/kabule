@extends('layouts.front')

@section('content')
    <div class="page-header">
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <h2 class="page-title">Upcoming News</h2>
            </div>
            <!-- /.col-md-6 -->
            <div class="col-md-6 col-sm-6 hidden-xs back-home">
                <a href="index.html">&larr; Go back Home</a>
            </div>
            <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
    </div> <!-- /.page-header -->

    <div class="row">

        <div class="col-md-8">

            @if($projectNews->count() == 0)
                <div class="page-header">
                    <h2 class="page-title">Sorry, We don't have any news yet!</h2>
                </div>
            @else
                @foreach($projectNews as $news)
                    <div class="event-list">
                        <div class="event-thumb">
                            @if($news->featured_thumbnail)
                                <img src="{{ asset($news->featured_thumbnail) }}" alt="">
                            @else
                                <img src="http://fakeimg.pl/200x200/?text=No Image">
                            @endif
                        </div>
                        <!-- /.event-thumb -->
                        <div class="event-content">
                            <h4 class="event-title"><a href="{{ action('Front\SiteNewsController@show', [$news->slug]) }}">{{ $news->name }}</a></h4>

                            <p>{!! substr($news->description,0, 200) !!}</p>

                            <div class="event-time">
                                <span><i class="fa fa-calendar"></i>{{ $news->created_at }}</span> <!--there were no date column in news table so $news->created_at -->
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <!-- /.event-content -->
                    </div>
                    <!-- /.event-list -->
                @endforeach

                {!! $projectNews->render() !!}

            @endif
        </div>
        <!-- /.col-md-8 -->

        <div class="col-md-4">
            <div class="box-content">
                <h4 class="widget-title"><span>Upcoming events</span></h4>

                <div class="events-sidebar">
                    <ul>
                        <li class="event-item">
                            <div class="event-thumb">
                                <img src="http://placehold.it/80x80" alt="">
                            </div>
                            <div class="event-content">
                                <h5 class="event-title"><a href="event-detail.html">Memorial Day Sale: 30% All Day Monday May 26</a></h5>

                                <p class="event-meta">02 January 2015</p>
                            </div>
                        </li>
                        <li class="event-item">
                            <div class="event-thumb">
                                <img src="http://placehold.it/80x80" alt="">
                            </div>
                            <div class="event-content">
                                <h5 class="event-title"><a href="event-detail.html">Fashion Figures Online Sale: Sizes 10 - 3X</a></h5>

                                <p class="event-meta">02 January 2015</p>
                            </div>
                        </li>
                        <li class="event-item">
                            <div class="event-thumb">
                                <img src="http://placehold.it/80x80" alt="">
                            </div>
                            <div class="event-content">
                                <h5 class="event-title"><a href="event-detail.html">Speak Up! National Summit for Women Living with HIV</a></h5>

                                <p class="event-meta">02 January 2015</p>
                            </div>
                        </li>
                    </ul>
                    <a href="events-list.html" class="read-more">More Events &rarr;</a>
                </div>
                <!-- /.events-sidebar -->
            </div>
            <!-- /.box-content -->

            @include('front.partials.advertisement-sidebar')
        </div>
        <!-- /.col-md-4 -->

    </div> <!-- /.row -->
@stop