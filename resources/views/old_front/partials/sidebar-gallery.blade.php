<div class="gallery-wrapper">
    @foreach( $photoObjects as $photoObject )
        <div class="gallery-thumb">
            @if( empty($photoObject->path) )
                <img src="http://placehold.it/240x192" alt="">
            @else
                <a href="{{ asset($photoObject->path) }}" class="fancybox"
                   data-fancybox-group="thumbnail-{{ $photoObject->id }}">
                    <img src="{{ asset($photoObject->thumbnail_path) }}" alt="">
                </a>
            @endif
        </div>
        @endforeach

</div>
