<div class="box-content">
    <h4 class="widget-title"><span>Upcoming events</span></h4>

    <div class="events-sidebar">
        <ul>
            <li class="event-item">
                <div class="event-thumb">
                    <img src="http://placehold.it/80x80" alt="">
                </div>
                <div class="event-content">
                    <h5 class="event-title"><a href="event-detail.html">Memorial Day Sale: 30% All Day
                            Monday May 26</a></h5>

                    <p class="event-meta">02 January 2015</p>
                </div>
            </li>
            <li class="event-item">
                <div class="event-thumb">
                    <img src="http://placehold.it/80x80" alt="">
                </div>
                <div class="event-content">
                    <h5 class="event-title"><a href="event-detail.html">Fashion Figures Online Sale:
                            Sizes 10 - 3X</a></h5>

                    <p class="event-meta">02 January 2015</p>
                </div>
            </li>
            <li class="event-item">
                <div class="event-thumb">
                    <img src="http://placehold.it/80x80" alt="">
                </div>
                <div class="event-content">
                    <h5 class="event-title"><a href="event-detail.html">Speak Up! National Summit for
                            Women Living with HIV</a></h5>

                    <p class="event-meta">02 January 2015</p>
                </div>
            </li>
        </ul>
        <a href="events-list.html" class="read-more">More Events &rarr;</a>
    </div>
    <!-- /.events-sidebar -->
</div>
<!-- /.box-content -->