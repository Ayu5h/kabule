@extends('layouts.front')

@section('content')

    <div class="container">

        <div class="page-header">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <h2 class="page-title">{{ $project->title }}</h2>
                </div>
                <!-- /.col-md-6 -->
                <div class="col-md-6 col-sm-6 hidden-xs back-home">
                    <a href="{{ route('home') }}">&larr; Go back Home</a>
                </div>
                <!-- /.col-md-6 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.page-header -->

        <div class="row">

            <div class="col-md-8">
                <div class="blog-single">
                    <div class="post-image">
                        <img src="{{ asset($project->featured_image) }}" alt="">

                        <div class="image-over">
                            <span>Supported By <i class="fa fa-user"></i>{{ $project->sponsor }}</span>
                        </div>
                    </div>
                    <!-- /.blog-thumb -->
                    <div class="post-content">
                        <h3 class="post-title">{{ $project->title }}</h3>

                        {!! $project->description !!}
                        @foreach( $project->topics as $topic )
                            <h3>{{ $topic->name }}</h3>

                            <div class="row">
                                @if(!empty($topic->featured_image))
                                    <div class="col-md-12" style="text-align: center">
                                        <img src="{{ asset($topic->featured_image) }}" alt=""
                                             style="height: 250px; width: 600px"/>
                                    </div>
                                @endif
                                <div class="col-md-12">
                                    {!! $topic->description !!}
                                </div>
                                <div class="col-md-12">
                                    <ul class="post-gallery">
                                        @foreach( $topic->photos as $photo )
                                            @if( $photo->thumbnail_path != $topic->featured_thumbnail )
                                                <li>
                                                    <a href="{{ asset($photo->path) }}" class="fancybox"
                                                       data-fancybox-group="thumbnail-{{ $topic->id }}">
                                                        <img src="{{ asset($photo->thumbnail_path) }}" alt="">
                                                    </a>
                                                </li>
                                            @endif
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <!-- /.blog-content -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.col-md-8 -->

            <div class="col-md-4">

                @include('front.projects.partials.sidebar-news')

                @include('front.projects.partials.sidebar-activity')

                @include('front.projects.partials.sidebar-gallery', ['name' => 'Project', 'type' => $project, 'object' => $project->photos])

                @include('front.partials.advertisement-sidebar')
            </div>
            <!-- /.col-md-4 -->

        </div>
        <!-- /.row -->

    </div> <!-- /.container -->

@stop