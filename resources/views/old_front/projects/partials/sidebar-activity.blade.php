@if( $project->activities->count() > 0 )
    <div class="box-content categories">
        <h4 class="widget-title"><span>Project Activities</span></h4>

        <div class="row">
            <div class="col-md-12">
                <ul>
                    @foreach( $project->activities as $activity )
                        <li>
                            <a href="{{ route('projectActivity', [$project->slug, $activity->slug]) }}">{{ $activity->name}}</a>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.box-content -->
@endif