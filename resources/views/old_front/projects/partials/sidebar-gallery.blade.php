@if( (empty($type->featured_thumbnail) && $object->count() > 0) || (!empty($type->featured_thumbnail) && $object->count() > 1) )
    <div class="box-content">
        <h4 class="widget-title"><span>{{ $name }} Gallery</span></h4>

        <div class="gallery-wrapper">
            @foreach( $object as $photo )
                @if( $type->featured_thumbnail != $photo->thumbnail_path )
                    <div class="gallery-thumb">
                        <a href="{{ asset($photo->path) }}" class="fancybox"
                           data-fancybox-group="group-{{ strtolower($name) }}">
                            <img src="{{ asset($photo->thumbnail_path) }}" alt="">
                        </a>
                    </div>
                @endif
            @endforeach
        </div>
        <!-- /.gallery-wrapper -->
    </div>
    <!-- /.box-content -->
@endif