@if( $project->news->count() > 0 )
    <div class="box-content">
        <h4 class="widget-title"><span>Program News</span></h4>

        <div class="events-sidebar custom-news-sidebar">
            <ul>
                @foreach( $project->news as $news )
                    <li class="event-item">
                        <div class="event-content">
                            @if( !empty($news->featured_thumbnail) )
                                <img src="{{ asset($news->featured_thumbnail) }}" alt="" class="left-image"
                                     style="height: 80px; width: 80px;">
                            @else
                                <img src="http://placehold.it/80x80" alt="" class="left-image">
                            @endif
                            <h5 class="event-title">
                                <a href="{{ route('projectNews', [$project->slug, $news->slug]) }}">{{ ucwords($news->name) }}</a>
                            </h5>
                            <a class="read-more"
                               href="{{ route('projectNews', [$project->slug, $news->slug]) }}">Continue
                                Reading
                                →</a>
                        </div>
                    </li>
                @endforeach
            </ul>
        </div>
        <!-- /.events-sidebar -->
    </div>
    <!-- /.box-content -->
@endif