@extends('layouts.front')

@section('content')
    <div class="container">

        <div class="page-header">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <h2 class="page-title">Projects/Programs</h2>
                </div>
                <!-- /.col-md-6 -->
                <div class="col-md-6 col-sm-6 hidden-xs back-home">
                    <a href="{{ route('home') }}">&larr; Go back Home</a>
                </div>
                <!-- /.col-md-6 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.page-header -->

        <div class="row">

            <div class="col-md-8">
                @foreach($projects as $project)
                    <div class="event-list">
                        <div class="event-thumb">
                            @if( empty($project->featured_thumbnail) )
                                <img src="http://placehold.it/240x192" alt="">
                            @else
                                <img src="{{ $project->featured_thumbnail }}" alt="">
                            @endif
                        </div>
                        <!-- /.event-thumb -->
                        <div class="event-content">
                            <h4 class="event-title">
                                <a href="{{ route('viewProject', $project->slug) }}">{{ $project->title }}</a>
                            </h4>
                            <p>
                                Supported By: {{ $project->sponsor }}
                            </p>
                            <p>{{ $project->intro_text }}</p>
                            <div class="clearfix"></div>
                            <a href="{{ route('viewProject', $project->slug) }}">Read More &rarr;</a>
                        </div>
                        <!-- /.event-content -->
                    </div>
                    <!-- /.event-list -->
                @endforeach
                <div class="row">
                    <div class="col-md-12">
                       {!! $projects->render() !!}
                    </div>
                    <!-- /.col-md-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.col-md-8 -->

            <div class="col-md-4">
                <div class="box-content">
                    <h4 class="widget-title"><span>Our Gallery</span></h4>

                    @include('front.partials.sidebar-gallery', [
                        'photoObjects' => $photoObjects
                    ])
                </div>
                <!-- /.box-content -->

                @include('front.partials.sidebar-news', ['objects' => $objects])

                @include('front.partials.advertisement-sidebar')
            </div>
            <!-- /.col-md-4 -->

        </div>
        <!-- /.row -->

    </div> <!-- /.container -->
@stop