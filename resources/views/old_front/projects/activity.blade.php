@extends('layouts.front')

@section('content')

    <div class="container">

        <div class="page-header">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <h2 class="page-title">{{ $activity->name }}</h2>
                </div>
                <!-- /.col-md-6 -->
                <div class="col-md-6 col-sm-6 hidden-xs back-home">
                    <a href="{{ route('viewProject', $project->slug) }}">&larr; Go back Project</a>
                </div>
                <!-- /.col-md-6 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.page-header -->

        <div class="row">

            <div class="col-md-8">
                <div class="blog-single">
                    <div class="post-image">
                        <img src="{{ asset($activity->featured_image) }}" alt="">
                    </div>
                    <!-- /.blog-thumb -->
                    <div class="post-content">
                        <h3 class="post-title">{{ $activity->title }}</h3>

                        {!! $activity->description !!}
                    </div>
                    <!-- /.blog-content -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.col-md-8 -->
            @include('front.projects.partials.sidebar-gallery', ['name' => 'Activity', 'type' => $activity, 'object' => $activity->photos])

            @include('front.projects.partials.sidebar-news')

            @include('front.partials.advertisement-sidebar')

        </div>
        <!-- /.row -->

    </div> <!-- /.container -->

@stop