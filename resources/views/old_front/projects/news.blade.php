@extends('layouts.front')

@section('content')

    <div class="container">

        <div class="page-header">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <h2 class="page-title">{{ $news->name }}</h2>
                </div>
                <!-- /.col-md-6 -->
                <div class="col-md-6 col-sm-6 hidden-xs back-home">
                    <a href="{{ route('viewProject', $project->slug) }}">&larr; Go back Project</a>
                </div>
                <!-- /.col-md-6 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.page-header -->

        <div class="row">

            <div class="col-md-8">
                <div class="blog-single">
                    <div class="post-image">
                        <img src="{{ asset($news->featured_image) }}" alt="">
                    </div>
                    <!-- /.blog-thumb -->
                    <div class="post-content">
                        <h3 class="post-title">{{ $news->title }}</h3>

                        {!! $news->description !!}
                    </div>
                    <!-- /.blog-content -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.col-md-8 -->

            <div class="col-md-4">
                @include('front.projects.partials.sidebar-gallery', ['name' => 'News', 'type' => $news, 'object' => $news->photos])

                @include('front.projects.partials.sidebar-activity')

                @include('front.partials.advertisement-sidebar')

            </div>
            <!-- /.col-md-4 -->

        </div>
        <!-- /.row -->

    </div> <!-- /.container -->

@stop