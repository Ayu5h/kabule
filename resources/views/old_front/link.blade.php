@extends('layouts.front')


@section('content')

    <div class="page-header">
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <h2 class="page-title">
                    BCN and its partners around the World
                </h2>
            </div>
            <!-- /.col-md-6 -->
            <div class="col-md-6 col-sm-6 hidden-xs back-home">
                <a href="index.html">&larr; Go back Home</a>
            </div>
            <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
    </div> <!-- /.page-header -->

    <div class="row">
        <div class="col-md-8">

            @if($links->count() == 0)

                <div class="page-header">
                    <h2 class="page-title">Sorry, We don't have any link yet!</h2>
                </div>
            @else
                @foreach($links as $link)

                    <div class="blog-list">

                        <div class="post-thumb" style="height:98px; width: 200px;">
                            @if($link->image)
                                <img src="{{ asset($link->thumbnail) }}" alt=""/>
                            @else
                                <img src="http://placehold.it/200x80" alt="">
                            @endif
                        </div>

                        <div class="post-content">
                            <h4 class="post-title"><a href="http://{{ $link->link }}" target="_blank">{{ $link->title }}</a></h4>
                        </div>

                    </div>

                @endforeach
            @endif
        </div>

        <div class="col-md-4">

            <!-- /.box-content -->
            <div class="box-content">
                <h4 class="widget-title"><span>Our Gallery</span></h4>

                <div class="gallery-wrapper">
                    <div class="gallery-thumb">
                        <a href="images/gallery-placeholder.png" class="fancybox" data-fancybox-group="group1">
                            <img src="http://placehold.it/80x80" alt="">
                        </a>
                    </div>
                    <!-- /.gallery-thumb -->
                    <div class="gallery-thumb">
                        <a href="images/gallery-placeholder.png" class="fancybox" data-fancybox-group="group1">
                            <img src="http://placehold.it/80x80" alt="">
                        </a>
                    </div>
                    <!-- /.gallery-thumb -->
                    <div class="gallery-thumb">
                        <a href="images/gallery-placeholder.png" class="fancybox" data-fancybox-group="group1">
                            <img src="http://placehold.it/80x80" alt="">
                        </a>
                    </div>
                    <!-- /.gallery-thumb -->
                    <div class="gallery-thumb">
                        <a href="images/gallery-placeholder.png" class="fancybox" data-fancybox-group="group1">
                            <img src="http://placehold.it/80x80" alt="">
                        </a>
                    </div>
                    <!-- /.gallery-thumb -->
                    <div class="gallery-thumb">
                        <a href="images/gallery-placeholder.png" class="fancybox" data-fancybox-group="group1">
                            <img src="http://placehold.it/80x80" alt="">
                        </a>
                    </div>
                    <!-- /.gallery-thumb -->
                    <div class="gallery-thumb">
                        <a href="images/gallery-placeholder.png" class="fancybox" data-fancybox-group="group1">
                            <img src="http://placehold.it/80x80" alt="">
                        </a>
                    </div>
                    <!-- /.gallery-thumb -->
                </div>
                <!-- /.gallery-wrapper -->
            </div>
            <!-- /.box-content -->
            <div class="box-content">
                <h4 class="widget-title"><span>Upcoming events</span></h4>

                <div class="events-sidebar">
                    <ul>
                        <li class="event-item">
                            <div class="event-thumb">
                                <img src="http://placehold.it/80x80" alt="">
                            </div>
                            <div class="event-content">
                                <h5 class="event-title"><a href="event-detail.html">Memorial Day Sale: 30% All Day Monday May 26</a></h5>

                                <p class="event-meta">02 January 2015</p>
                            </div>
                        </li>
                        <li class="event-item">
                            <div class="event-thumb">
                                <img src="http://placehold.it/80x80" alt="">
                            </div>
                            <div class="event-content">
                                <h5 class="event-title"><a href="event-detail.html">Fashion Figures Online Sale: Sizes 10 - 3X</a></h5>

                                <p class="event-meta">02 January 2015</p>
                            </div>
                        </li>
                        <li class="event-item">
                            <div class="event-thumb">
                                <img src="http://placehold.it/80x80" alt="">
                            </div>
                            <div class="event-content">
                                <h5 class="event-title"><a href="event-detail.html">Speak Up! National Summit for Women Living with HIV</a></h5>

                                <p class="event-meta">02 January 2015</p>
                            </div>
                        </li>
                    </ul>
                    <a href="events-list.html" class="read-more">More Events &rarr;</a>
                </div>
                <!-- /.events-sidebar -->
            </div>
            <!-- /.box-content -->
        </div>
        <!-- /.col-md-4 -->

    </div> <!-- /.row -->


@stop
