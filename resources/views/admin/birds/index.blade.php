@extends('layouts.admin')

@section('content')

    <div class="box">
        <div class="box-body">
            <p>
                <a class="btn btn-success" href="{{ route('admin.birds.create') }}">
                    <i class="fa fa-aw fa-plus"></i>
                    Add New Bird </a>
            </p>

            <div class="divider"></div>
            <div class="table-responsive">

                <table class="table table-bordered table-striped datatable">
                    <thead>
                    <tr>
                        <th class="col-md-1">SN</th>
                        <th class="col-md-2">Image</th>
                        <th class="col-md-2">Name</th>
                        <th class="col-md-1">Kingdom</th>
                        <th class="col-md-2">Local Name</th>
                        <th class="col-md-3">Short Description</th>
                        <th class="col-md-1">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($birds as $k => $bird)
                        <tr>
                            <td>{{ ++$k }}</td>
                            <td>
                                <img src="{{ asset($bird->featured_thumbnail) }}" alt="" class="admin-thumbnail"/>
                            </td>
                            <td>{{ $bird->name }}</td>
                            <td>{{ $bird->kingdom }}</td>
                            <td>{{ $bird->local_name }}</td>
                            <td>{!! $bird->short_description !!}...</td>
                            <td>
                                <div class="dropdown action-menu">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-aw fa-cog fa-2x"></i></a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="{{ route('admin.birds.edit', array($bird->id)) }}" >Edit</a></li>
                                        <li><a href="#" class="application-delete-link">Delete</a></li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

        </div>
        <!-- /.box-body -->


    </div>

@stop



@section('scripts.footer')

    <script>
        $(function(){
            $('.datatable').dataTable({
                "pageLength": 2
            });
        });
    </script>

@stop