@extends('layouts.admin')

@section('content')

    {!! Form::open(array( 'url' => route("admin.birds.store"), 'class' => '' ) ) !!}

    <div class='box'>
        <div class='box-body'>

            <div class="row">

                @include('admin.birds.form')

            </div>
        </div>
        <div class="box-footer">
            <button class="btn btn-success" type="submit">
                <i class="fa fa-aw fa-save"></i> Save Bird
            </button>
        </div>

    </div>

    {!! Form::close()!!}
@stop



