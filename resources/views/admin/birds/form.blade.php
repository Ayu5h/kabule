<div class="col-md-6">
    <div class="form-group">
        <label for="title">Name <i class="fa fa-aw fa-asterisk"></i></label>
        {!! Form::input('text', 'name', null, ['class' => 'form-control', 'placeholder'=>'Enter Name']) !!}
        <span id="helpBlock2" class="help-block error">{{ $errors->first('name') }}</span>
    </div>

    <div class="form-group">
        <label for="title">Phylum <i class="fa fa-aw fa-asterisk"></i></label>
        {!! Form::input('text', 'phylum', null, ['class' => 'form-control', 'placeholder'=>'Enter Phylum']) !!}
        <span id="helpBlock2" class="help-block error">{{ $errors->first('phylum') }}</span>
    </div>

    <div class="form-group">
        <label for="title">Class <i class="fa fa-aw fa-asterisk"></i></label>
        {!! Form::input('text', 'class', null, ['class' => 'form-control', 'placeholder'=>'Enter Class']) !!}
        <span id="helpBlock2" class="help-block error">{{ $errors->first('class') }}</span>
    </div>

    <div class="form-group">
        <label for="title">Order <i class="fa fa-aw fa-asterisk"></i></label>
        {!! Form::input('text', 'order', null, ['class' => 'form-control', 'placeholder'=>'Enter Order']) !!}
        <span id="helpBlock2" class="help-block error">{{ $errors->first('order') }}</span>
    </div>

    <div class="form-group">
        <label for="title">Genus <i class="fa fa-aw fa-asterisk"></i></label>
        {!! Form::input('text', 'genus', null, ['class' => 'form-control', 'placeholder'=>'Enter Genus']) !!}
        <span id="helpBlock2" class="help-block error">{{ $errors->first('genus') }}</span>
    </div>

    <div class="form-group">
        <label for="title">Species <i class="fa fa-aw fa-asterisk"></i></label>
        {!! Form::input('text', 'species', null, ['class' => 'form-control', 'placeholder'=>'Enter Species']) !!}
        <span id="helpBlock2" class="help-block error">{{ $errors->first('species') }}</span>
    </div>

    <div class="form-group">
        <label for="title">Sub Species <i class="fa fa-aw fa-asterisk"></i></label>
        {!! Form::input('text', 'sub_species', null, ['class' => 'form-control', 'placeholder'=>'Enter Sub Species'])
        !!}
        <span id="helpBlock2" class="help-block error">{{ $errors->first('sub_species') }}</span>
    </div>

</div>

<div class="col-md-6">

    <div class="form-group">
        <label for="title">Kingdom <i class="fa fa-aw fa-asterisk"></i></label>
        {!! Form::input('text', 'kingdom', null, ['class' => 'form-control', 'placeholder'=>'Enter Kingdom']) !!}
        <span id="helpBlock2" class="help-block error">{{ $errors->first('kingdom') }}</span>
    </div>

    <div class="form-group">
        <label for="title">Family <i class="fa fa-aw fa-asterisk"></i></label>
        {!! Form::input('text', 'family', null, ['class' => 'form-control', 'placeholder'=>'Enter Family']) !!}
        <span id="helpBlock2" class="help-block error">{{ $errors->first('family') }}</span>
    </div>

    <div class="form-group">
        <label for="title">Synonym <i class="fa fa-aw fa-asterisk"></i></label>
        {!! Form::input('text', 'synonym', null, ['class' => 'form-control', 'placeholder'=>'Enter Synonym']) !!}
        <span id="helpBlock2" class="help-block error">{{ $errors->first('synonym') }}</span>
    </div>

    <div class="form-group">
        <label for="title">Local Name <i class="fa fa-aw fa-asterisk"></i></label>
        {!! Form::input('text', 'local_name', null, ['class' => 'form-control', 'placeholder'=>'Enter Local Name']) !!}
        <span id="helpBlock2" class="help-block error">{{ $errors->first('local_name') }}</span>
    </div>

    <div class="form-group">
        <label for="title">Distribution <i class="fa fa-aw fa-asterisk"></i></label>
        {!! Form::input('text', 'distribution', null, ['class' => 'form-control', 'placeholder'=>'Enter Distribution'])
        !!}
        <span id="helpBlock2" class="help-block error">{{ $errors->first('distribution') }}</span>
    </div>

    <div class="form-group">
        <label for="description">Short Description <i class="fa fa-aw fa-asterisk"></i></label>
        {!! Form::textarea('short_description', null, ['class' => 'form-control', 'placeholder'=>'Enter Description'])
        !!}
        <span id="helpBlock2" class="help-block error">{{ $errors->first('short_description') }}</span>
    </div>

</div>

<div class="col-md-12">
    <div class="form-group">
        <label for="title">Meta Title </label>
        {!! Form::input('text', 'meta_title', null, ['class' => 'form-control', 'placeholder'=>'Enter meta title for SEO'])
        !!}
    </div>

    <div class="form-group">
        <label for="title">Meta Description </label>
        {!! Form::input('text', 'meta_description', null, ['class' => 'form-control', 'placeholder'=>'Enter meta description for
        SEO']) !!}
    </div>
</div>

<div class="col-md-12">
    <div class="form-group">
        <label for="description">Description <i class="fa fa-aw fa-asterisk"></i></label>
        {!! Form::textarea('description', null, ['class' => 'form-control', 'placeholder'=>'Enter Description', 'class'
        =>
        'ckeditor']) !!}
        <span id="helpBlock2" class="help-block error">{{ $errors->first('description') }}</span>
    </div>
</div>

