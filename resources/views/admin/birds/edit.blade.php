@extends('layouts.admin')

@section('content')

    {!! Form::model( $bird, [ 'method' => 'PATCH', 'url' => route('admin.birds.update', $bird->id ) ] ) !!}

    <div class='box'>
        <div class='box-body'>

            <div class="row">
                <div class="col-md-6">

                    @include('admin.partials.image-upload')

                </div>

                <div class="col-md-6 uploaded-images">

                    @include('admin.partials.image-selection' , ['object' => $bird])

                </div>

                <div class="clearfix"></div>

                @include('admin.birds.form')

            </div>
        </div>
        <div class="box-footer">
            <button class="btn btn-success" type="submit">
                <i class="fa fa-aw fa-save"></i> Edit Bird
            </button>
        </div>

    </div>

    {!! Form::close()!!}
@stop

@section('scripts.footer')

    @include('admin.partials.dropzone-script', [
        'url' => route('admin.birds.storePhoto', [$bird->id]),
        'uploadType' => 'multiple'
    ])

@stop



