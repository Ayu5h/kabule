@extends('layouts.admin')

@section('content')


    <div class='box'>
        <div class='box-body'>

            <div class="row">


                <div class="col-md-12">
                     <div class="form-group">
                         <label for="name">Name: {{ $contactMessage->name }}</label>
                      </div>

                    <div class="form-group">
                        <label for="name">Email: {{ $contactMessage->email }}</label>
                    </div>

                    <div class="form-group">
                        <label for="name">Subject: {{ $contactMessage->subject }}</label>
                    </div>

                    <div class="form-group">
                        <label for="name">description: </label>
                        <p>{{ nl2br($contactMessage->description) }}</p>
                    </div>

                </div>


            </div>
        </div>
        <div class="box-footer">

        </div>

    </div>

    {!! Form::close()!!}




@stop



