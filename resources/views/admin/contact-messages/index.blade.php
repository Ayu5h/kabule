@extends('layouts.admin')

@section('content')

    <div class="box">
        <div class="box-body">


            <div class="divider"></div>
            <div class="table-responsive">

                <table class="table table-bordered table-striped datatable">
                    <thead>
                    <tr>
                        <th class="col-md-1">SN</th>
                        <th class="col-md-2">Name</th>
                        <th class="col-md-2">Email</th>
                        <th class="col-md-2">Subject</th>
                        <th class="col-md-4">Description</th>
                        <th class="col-md-1">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($contactMessages as $k => $contactMessage)
                        <tr>
                            <td>{{ ++$k }}</td>
                            <td>{{ $contactMessage->name }}</td>
                            <td>{{ $contactMessage->email }}</td>
                            <td>{{ $contactMessage->subject }}</td>
                            <td>{!! substr($contactMessage->description,0,200) !!}........</td>
                            <td>
                                <div class="dropdown action-menu">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-aw fa-cog fa-2x"></i></a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="{{ route('contactMessagesShow', array($contactMessage->id)) }}">View Details</a></li>
                                        <li class="divider"></li>
                                        <li>{!! delete_form(route('contactMessagesDelete', [$contactMessage->id]), 'Delete') !!}</li>
                                    </ul>
                                </div>

                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

        </div>
        <!-- /.box-body -->


    </div>

@stop



@section('scripts.footer')

    <script>
        $(function () {
            $('.datatable').dataTable({
                "pageLength": 2
            });
        });
    </script>

@stop