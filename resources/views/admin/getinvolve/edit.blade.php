@extends('layouts.admin')

@section('content')

{!! Form::model( $getinvolve, [ 'method' => 'PATCH', 'action' => [ 'GetinvolveController@update', $getinvolve->id ] ] ) !!}

<div class='box'>
    <div class='box-body'>

        <div class="row">


           {{--<div class="col-md-6">--}}
                {{--@include('admin.partials.image-upload')--}}
            {{--</div>--}}

           {{--<div class="col-md-6 uploaded-images">--}}
                {{--@include('admin.partials.image-selection', ['object' => $getinvolve])--}}
            {{--</div>--}}

           @include('admin.getinvolve.form')


        </div>
    </div>
    <div class="box-footer">



        <button class="btn btn-success" type="submit" >
           <i class="fa fa-aw fa-save"></i> Save Get Involve
        </button>

    </div>



</div>

{!! Form::close()!!}




@stop


@section('scripts.footer')


    @include('admin.partials.dropzone-script', [
        'url' => route('store_getinvolve_photo', [$getinvolve->id]),
        'uploadType' => 'multiple'
    ])


@stop
