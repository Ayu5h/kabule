@extends('layouts.admin')

@section('content')

{!! Form::open(array( 'url' => route("admin.getinvolve.store"), 'class' => '' ) ) !!}

<div class='box'>
    <div class='box-body'>

        <div class="row">


                @include('admin.getinvolve.form')


        </div>
    </div>
    <div class="box-footer">



        <button class="btn btn-success" type="submit" >
            <i class="fa fa-aw fa-save"></i> Save Get Involve </button>
    </div>

</div>

{!! Form::close()!!}




@stop



