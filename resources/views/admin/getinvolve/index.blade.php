@extends('layouts.admin')

@section('content')

<div class="box">
    <div class="box-body">
        <p>
            <a class="btn btn-success" href="{{ route('admin.getinvolve.create') }}">
                <i class="fa fa-aw fa-plus"></i>
                Add New Get Involve </a>
        </p>

        <div class="divider"></div>
        <div class="table-responsive">

            <table class="table table-bordered table-striped datatable">
                <thead>
                    <tr>
                        <th class="col-md-1">SN</th>
                        <th class="col-md-2">Title</th>
                        {{--<th class="col-md-2">Featured Image</th>--}}
                        <th class="col-md-2">Intro Text</th>
                        <th class="col-md-4">Description</th>
                        <th class="col-md-1">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($getinvolved as $k => $getinvolve)
                    <tr>
                        <td>{{ ++$k }}</td>
                        <td>{{ $getinvolve->title }}</td>
                        {{--<td><img src="{{ display_image($getinvolve->featured_thumbnail, 192, 145) }}" class="admin-thumbnail" alt=""/></td>--}}

                        <td>{{ $getinvolve->intro_text }}</td>

                        <td>{!! substr($getinvolve->description,0,200) !!}........</td>
                        <td>
                            <div class="dropdown action-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-aw fa-cog fa-2x"></i></a>
                                <ul class="dropdown-menu pull-right">
                                    {{--<li><a href="{{ route('admin.getinvolve.{id}.news.index', array($getinvolve->id)) }}" >News</a></li>--}}
                                    {{--<li><a href="{{ route('admin.getinvolve.{id}.topic.index', array($getinvolve->id)) }}" >Topic</a></li>--}}
                                    {{--<li><a href="{{ route('admin.getinvolve.{id}.activity.index', array($getinvolve->id)) }}" >Activity</a></li>--}}
                                    <li class="divider"></li>
                                    <li><a href="{{ route('admin.getinvolve.edit', array($getinvolve->id)) }}" >Edit</a></li>
                                    <li>{!! delete_form(route('admin.getinvolve.destroy', [$getinvolve->id]), 'Delete') !!}</li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

    </div>
    <!-- /.box-body -->


</div>

@stop



@section('scripts.footer')

    <script>
        $(function(){
            $('.datatable').dataTable({
                "pageLength": 2
            });
        });
    </script>

@stop