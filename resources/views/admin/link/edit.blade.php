@extends('layouts.admin')

@section('content')

    {!! Form::model( $link, array( 'url' => route("admin.link.update", [$link->id]), 'method' => 'PATCH' ) ) !!}

    <div class='box'>
        <div class='box-body'>

            {{--<div class="row">--}}
                {{--<div class="col-md-6">--}}
                    {{--@include('admin.partials.image-upload')--}}
                {{--</div>--}}

                {{--<div class="col-md-6">--}}
                    {{--@if($link->image)--}}
                        {{--<img src="{{ asset($link->thumbnail) }}" alt=""/>--}}
                    {{--@endif--}}
                {{--</div>--}}
            {{--</div>--}}

            <div class="row">
                @include('admin.link.form')
            </div>

        </div>

        <div class="box-footer">

            <button class="btn btn-success" type="submit">
                <i class="fa fa-aw fa-save"></i> Save Link
            </button>

        </div>

    </div>

    {!! Form::close()!!}




@stop


@section('scripts.footer')

    @include('admin.partials.dropzone-script', [
        'url' => route('admin.link.addPhoto', [$link->id]),
        'uploadType' => 'single'
    ]);

@stop




