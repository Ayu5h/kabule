@extends('layouts.admin')

@section('content')

    <div class="box">
        <div class="box-body">
            <p>
                <a class="btn btn-success" href="{{ route("admin.link.create") }}">
                    <i class="fa fa-aw fa-plus"></i>
                    Add Link </a>
            </p>

            <div class="divider"></div>
            <div class="table-responsive">

                <table class="table table-bordered table-striped datatable">
                    <thead>
                    <tr>
                        <th class="col-md-1">SN</th>
                        <th class="col-md-1">Title</th>
                        {{--<th class="col-md-2">Image</th>--}}
                        <th class="col-md-2">Link</th>
                        <th class="col-md-1">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($links as $k => $link)
                        <tr>
                            <td>{{ ++$k }}</td>
                            <td>{{ $link->title }}</td>
                            {{--<td>--}}
                                {{--@if(!empty($link->image))--}}
                                    {{--<img src="{{ asset($link->thumbnail) }}" class="admin-thumbnail"/>--}}
                                {{--@else--}}
                                    {{--<img src="http://fakeimg.pl/100x100/?text=No Image">--}}
                                {{--@endif--}}
                            {{--</td>--}}
                            <td>{{ $link->link }}</td>
                            <td>
                                <div class="dropdown action-menu">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-aw fa-cog fa-2x"></i></a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="{{ route('admin.link.edit', [$link->id]) }}" >Edit</a></li>
                                        <li>{!! delete_form(route('admin.link.destroy', [$link->id]), 'Delete') !!}</li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

        </div>
        <!-- /.box-body -->


    </div>

@stop



@section('scripts.footer')

    <script>
        $(function(){
            $('.datatable').dataTable({
                "pageLength": 2
            });
        });
    </script>

@stop