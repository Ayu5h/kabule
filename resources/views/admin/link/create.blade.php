@extends('layouts.admin')

@section('content')

    {!! Form::open( array( 'url' => route("admin.link.store") ) ) !!}

    <div class='box'>
        <div class='box-body'>

            @include('admin.link.form')

        </div>

        <div class="box-footer">

            <button class="btn btn-success" type="submit">
                <i class="fa fa-aw fa-save"></i> Save Link
            </button>

        </div>

    </div>

    {!! Form::close()!!}




@stop




