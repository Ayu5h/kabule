<div class="col-md-6">

    <div class="form-group">
        <label for="title">Title</label>
        {!! Form::input('text', 'title', null, ['class' => 'form-control', 'placeholder'=>'Enter Title']) !!}
        <span id="helpBlock2" class="help-block error">{{ $errors->first('title') }} </span>
    </div>

    <div class="form-group">
        <label for="link">Link</label>
        {!! Form::input('text', 'link', null, ['class' => 'form-control', 'placeholder'=>'Enter Link']) !!}
        <span id="helpBlock2" class="help-block error">{{ $errors->first('link') }} </span>
    </div>

</div>