@extends('layouts.admin')

@section('content')

{!! Form::open(array( 'url' => route("admin.whatWeDo.store"), 'class' => '' ) ) !!}

<div class='box'>
    <div class='box-body'>

        <div class="row">


                @include('admin.whatWeDo.form')


        </div>
    </div>
    <div class="box-footer">



        <button class="btn btn-success" type="submit" >
            <i class="fa fa-aw fa-save"></i> Save What We Do </button>
    </div>

</div>

{!! Form::close()!!}




@stop



