@extends('layouts.admin')

@section('content')

{!! Form::model( $whatWeDo, [ 'method' => 'PATCH', 'action' => [ 'WhatWeDoController@update', $whatWeDo->id ] ] ) !!}

<div class='box'>
    <div class='box-body'>

        <div class="row">


           <div class="col-md-6">
                @include('admin.partials.image-upload')
            </div>

           <div class="col-md-6 uploaded-images">
                @include('admin.partials.image-selection', ['object' => $whatWeDo])
            </div>

           @include('admin.whatWeDo.form')


        </div>
    </div>
    <div class="box-footer">



        <button class="btn btn-success" type="submit" >
           <i class="fa fa-aw fa-save"></i> Save What We Do
        </button>

    </div>



</div>

{!! Form::close()!!}




@stop


@section('scripts.footer')


    @include('admin.partials.dropzone-script', [
        'url' => route('store_whatWeDo_photo', [$whatWeDo->id]),
        'uploadType' => 'multiple'
    ])


@stop
