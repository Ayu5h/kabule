@extends('layouts.admin')

@section('content')

{!! Form::open(array( 'url' => route("admin.project.store"), 'class' => '' ) ) !!}

<div class='box'>
    <div class='box-body'>

        <div class="row">


                @include('admin.program.form')


        </div>
    </div>
    <div class="box-footer">



        <button class="btn btn-success" type="submit" >
            <i class="fa fa-aw fa-save"></i> Save Project </button>
    </div>

</div>

{!! Form::close()!!}




@stop



