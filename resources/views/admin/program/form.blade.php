<div class="col-md-6">
    <div class="form-group">
        <label for="title">Title <i class="fa fa-aw fa-asterisk"></i></label>
        {!! Form::input('text', 'title', null, ['class' => 'form-control', 'placeholder'=>'Enter Title']) !!}
        <span id="helpBlock2" class="help-block error">{{ $errors->first('title') }}</span>
    </div>


    <div class="form-group">
        <label for="show_on_home_page">Show on Home Page <i class="fa fa-aw fa-asterisk"></i></label>
        {!! Form::select('show_on_home_page',['1'=>'Yes','0'=>'No'], null, ['class' => 'form-control',
        'placeholder'=>'Enter Show on Home Page']) !!}
        <span id="helpBlock2" class="help-block error">{{ $errors->first('show_on_home_page') }}</span>
    </div>

    <div class="form-group">
        <label for="sponsor">Sponsor <i class="fa fa-aw fa-asterisk"></i></label>
        {!! Form::input('text', 'sponsor', null, ['class' => 'form-control', 'placeholder'=>'Enter Sponsor Name']) !!}
        <span id="helpBlock2" class="help-block error">{{ $errors->first('sponsor') }}</span>
    </div>

</div>

<div class="col-md-6">
    <div class="form-group">
        <label for="intro_text">Intro Text <i class="fa fa-aw fa-asterisk"></i></label>
        {!! Form::textarea('intro_text', null, ['class' => 'form-control', 'placeholder'=>'Enter Intro Text', 'rows'=>
        5]) !!}
        <span id="helpBlock2" class="help-block error">{{ $errors->first('intro_text') }}</span>
    </div>
</div>

<div class="clearfix"></div>

<div class="col-md-12">

    <div class="form-group">
        <label for="description">Description <i class="fa fa-aw fa-asterisk"></i></label>
        {!! Form::textarea('description', null, ['class' => 'form-control', 'placeholder'=>'Enter Description',
        'class' => 'ckeditor']) !!}
        <span id="helpBlock2" class="help-block error">{{ $errors->first('description') }}</span>
    </div>

</div>


<div class="col-md-12">
    <div class="form-group">
        <label for="title">Meta Title </label>
        {!! Form::input('text', 'meta_title', null, ['class' => 'form-control', 'placeholder'=>'Enter meta title for SEO'])
        !!}
    </div>

    <div class="form-group">
        <label for="title">Meta Description </label>
        {!! Form::input('text', 'meta_description', null, ['class' => 'form-control', 'placeholder'=>'Enter meta description for SEO']) !!}
    </div>
</div>

