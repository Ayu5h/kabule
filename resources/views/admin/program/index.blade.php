@extends('layouts.admin')

@section('content')

<div class="box">
    <div class="box-body">
        <p>
            <a class="btn btn-success" href="{{ route('admin.project.create') }}">
                <i class="fa fa-aw fa-plus"></i>
                Add New Project </a>
        </p>

        <div class="divider"></div>
        <div class="table-responsive">

            <table class="table table-bordered table-striped datatable">
                <thead>
                    <tr>
                        <th class="col-md-1">SN</th>
                        <th class="col-md-2">Title</th>
                        <th class="col-md-2">Featured Image</th>
                        <th class="col-md-2">Intro Text</th>
                        <th class="col-md-4">Description</th>
                        <th class="col-md-1">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($programs as $k => $program)
                    <tr>
                        <td>{{ ++$k }}</td>
                        <td>{{ $program->title }}</td>
                        <td><img src="{{ display_image($program->featured_thumbnail, 192, 145) }}" class="admin-thumbnail" alt=""/></td>

                        <td>{{ $program->intro_text }}</td>

                        <td>{!! substr($program->description,0,200) !!}........</td>
                        <td>
                            <div class="dropdown action-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-aw fa-cog fa-2x"></i></a>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="{{ route('admin.project.{id}.news.index', array($program->id)) }}" >News</a></li>
                                    <li><a href="{{ route('admin.project.{id}.topic.index', array($program->id)) }}" >Topic</a></li>
                                    <li><a href="{{ route('admin.project.{id}.activity.index', array($program->id)) }}" >Activity</a></li>
                                    <li class="divider"></li>
                                    <li><a href="{{ route('admin.project.edit', array($program->id)) }}" >Edit</a></li>
                                    <li>{!! delete_form(route('admin.project.destroy', [$program->id]), 'Delete') !!}</li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

    </div>
    <!-- /.box-body -->


</div>

@stop



@section('scripts.footer')

    <script>
        $(function(){
            $('.datatable').dataTable({
                "pageLength": 2
            });
        });
    </script>

@stop