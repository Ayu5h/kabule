@extends('layouts.admin')

@section('content')

    {!! Form::model( $event, array( 'method' => 'PATCH', 'url' => route("admin.event.update", [$event->id]) ) ) !!}

    <div class='box'>
        <div class='box-body'>

            <div class="row">

                <div class="col-md-6">
                    @include('admin.partials.image-upload')
                </div>

                <div class="col-md-6 uploaded-images">
                    @include('admin.partials.image-selection', ['object' => $event])
                </div>

            </div>

            @include('admin.event.form')

        </div>

        <div class="box-footer">

            <button class="btn btn-success" type="submit">
                <i class="fa fa-aw fa-save"></i> Save Event
            </button>

        </div>

    </div>

    {!! Form::close()!!}




@stop

@section('scripts.footer')
    <script src="http://maps.google.com/maps/api/js?sensor=false&libraries=places"></script>

    @include('admin.partials.dropzone-script', [
        'url' => route('admin.event.addPhoto', [$event->id]),
        'uploadType' => 'multiple'
    ])
    <script>
        $(function() {
            searchMapInitialize('create_map_canvas', 'search-address', 12, '{{ $event->lat }}', '{{ $event->lng }}');
        });
    </script>


@stop


