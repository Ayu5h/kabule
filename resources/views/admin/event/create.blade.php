@extends('layouts.admin')

@section('content')

    {!! Form::open( array( 'url' => route("admin.event.store") ) ) !!}

    <div class='box'>
        <div class='box-body'>

            @include('admin.event.form')

        </div>

        <div class="box-footer">

            <button class="btn btn-success" type="submit">
                <i class="fa fa-aw fa-save"></i> Save Event
            </button>

        </div>

    </div>

    {!! Form::close()!!}




@stop

@section('scripts.footer')

    <script src="http://maps.google.com/maps/api/js?sensor=false&libraries=places"></script>

    <script>
        $(function() {
            searchMapInitialize('create_map_canvas', 'search-address');
        });
    </script>

@stop




