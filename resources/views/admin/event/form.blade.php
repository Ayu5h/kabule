<div class="row">

    <div class="col-md-12">

        <div class="col-md-6">
            <div class="form-group">
                <label for="title">Title</label>
                {!! Form::input('text', 'title', null, ['class' => 'form-control', 'placeholder'=>'Enter Title']) !!}
                <span id="helpBlock2" class="help-block error">{{ $errors->first('title') }} </span>
            </div>

            <div class="form-group">
                <label for="contact_phone">Contact Phone</label>
                {!! Form::input('text', 'contact_phone', null, ['class' => 'form-control', 'placeholder'=>'Enter Contact
                Phone']) !!}
                <span id="helpBlock2" class="help-block error">{{ $errors->first('contact_phone') }} </span>
            </div>

            <div class="form-group">
                <label for="contact_email">Contact Email</label>
                {!! Form::input('text', 'contact_email', null, ['class' => 'form-control', 'placeholder'=>'Enter Contact
                Email']) !!}
                <span id="helpBlock2" class="help-block error">{{ $errors->first('contact_email') }} </span>
            </div>

            <div class="form-group">
                <label for="contact_name">Contact Name</label>
                {!! Form::input('text', 'contact_name', null, ['class' => 'form-control', 'placeholder'=>'Enter Contact
                Name']) !!}
                <span id="helpBlock2" class="help-block error">{{ $errors->first('contact_name') }} </span>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <label for="start_date">Start Date</label>
                {!! Form::input('date', 'start_date', null, ['class' => 'form-control datePicker', 'placeholder'=>'Enter
                Start Date']) !!}
                <span id="helpBlock2" class="help-block error">{{ $errors->first('start_date') }} </span>
            </div>

            <div class="form-group">
                <label for="end_date">End Date</label>
                {!! Form::input('date', 'end_date', null, ['class' => 'form-control datePicker', 'placeholder'=>'Enter
                End Date']) !!}
                <span id="helpBlock2" class="help-block error">{{ $errors->first('end_date') }} </span>
            </div>

            <div class="form-group">
                <label for="start_time">Start Time</label>
                {!! Form::input('text', 'start_time', null, ['class' => 'form-control', 'placeholder'=>'Enter Start
                Time']) !!}
                <span id="helpBlock2" class="help-block error">{{ $errors->first('start_time') }} </span>
            </div>

            <div class="form-group">
                <label for="end_time">End Time</label>
                {!! Form::input('text', 'end_time', null, ['class' => 'form-control', 'placeholder'=>'Enter End Time'])
                !!}
                <span id="helpBlock2" class="help-block error">{{ $errors->first('end_time') }} </span>
            </div>
        </div>

        <div class="clearfix"></div>

        <div class="col-md-12">
            <div class="form-group view-map-canvas">

                {!! Form::input('text', 'location', null, ['class' => 'controls search-address',
                'placeholder'=>'Enter Location', 'id' => 'search-address'])
                !!}

                <div id="create_map_canvas" class="col-sm-6" style="height: 200px"></div>
                {!! Form::input('hidden', 'lat', '27.6915196', ['id' => 'lat']) !!}
                {!! Form::input('hidden', 'lng', '85.3420486', ['id' => 'lng']) !!}
            </div>

            <div class="clearfix"></div>
            <span id="helpBlock2" class="help-block error">{{ $errors->first('location') }} </span>
            {{--<div class="form-group">--}}
            {{--<label for="short_intro">Short Intro</label>--}}
            {{--{!! Form::textarea('short_intro', null, ['class' => 'form-control', 'placeholder'=>'Enter Short Intro',--}}
            {{--'rows' => 5]) !!}--}}
            {{--<span id="helpBlock2" class="help-block error">{{ $errors->first('short_intro') }} </span>--}}
            {{--</div>--}}

            <div class="form-group">
                <label for="description">Description</label>
                {!! Form::textarea('description', null, ['class' => 'form-control ckeditor', 'placeholder'=>'Enter
                Description']) !!}
                <span id="helpBlock2" class="help-block error">{{ $errors->first('description') }} </span>
            </div>
        </div>
    </div>

</div>