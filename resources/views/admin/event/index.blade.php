@extends('layouts.admin')

@section('content')

    <div class="box">
        <div class="box-body">
            <p>
                <a class="btn btn-success" href="{{ route("admin.event.create") }}">
                    <i class="fa fa-aw fa-plus"></i>
                    Add Event </a>
            </p>

            <div class="divider"></div>
            <div class="table-responsive">

                <table class="table table-bordered table-striped datatable">
                    <thead>
                    <tr>
                        <th class="col-md-1">SN</th>
                        <th class="col-md-1">Title</th>
                        <th class="col-md-2">Featured Image</th>
                        <th class="col-md-1">Location</th>
                        <th class="col-md-2">Date</th>
                        <th class="col-md-1">Time</th>

                        <th class="col-md-1">Contacts</th>
                        <th class="col-md-1">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($events as $k => $event)
                        <tr>
                            <td>{{ ++$k }}</td>
                            <td>{{ $event->title }}</td>
                            <td>
                                @if(!empty($event->featured_image))
                                    <img src="{{ asset($event->featured_thumbnail) }}" class="admin-thumbnail"/>
                                @else
                                    <img src="http://fakeimg.pl/100x100/?text=No Image">
                                @endif
                            </td>
                            <td>{{ $event->location }}</td>
                            <td>{{ $event->start_date . ' - ' . $event->end_date }}</td>
                            <td>{{ $event->start_time . ' - ' . $event->end_time }}</td>

                            <td>{{ $event->contact }}</td>
                            <td>
                                <div class="dropdown action-menu">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-aw fa-cog fa-2x"></i></a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="{{ route('admin.event.edit', [$event->id]) }}" >Edit</a></li>
                                        <li>{!! delete_form(route('admin.event.destroy', [$event->id]), 'Delete') !!}</li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

        </div>
        <!-- /.box-body -->


    </div>

@stop



@section('scripts.footer')

    <script>
        $(function(){
            $('.datatable').dataTable({
                "pageLength": 2
            });
        });
    </script>

@stop