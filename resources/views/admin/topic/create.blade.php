@extends('layouts.admin')

@section('content')

{!! Form::open(array( 'url' => route("admin.project.{id}.topic.store", [$programId]), 'class' => '' ) ) !!}

<div class='box'>
    <div class='box-body'>

        <div class="row">

            @include('admin.topic.form')

        </div>
    </div>
    <div class="box-footer">



        <button class="btn btn-success" type="submit" >
            <i class="fa fa-aw fa-save"></i> Save Topic </button>
    </div>

</div>

{!! Form::close()!!}




@stop



