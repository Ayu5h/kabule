<div class="col-md-12">
    <div class="form-group">
        <label for="name">Name <i class="fa fa-aw fa-asterisk"></i></label>
        {!! Form::input('text', 'name', null, ['class' => 'form-control', 'placeholder'=>'Enter Name']) !!}
        <span id="helpBlock2" class="help-block error">{{ $errors->first('name') }}</span>
    </div>

    <div class="form-group">
        <label for="description">Description <i class="fa fa-aw fa-asterisk"></i></label>
        {!! Form::textarea('description', null, ['class' => 'form-control', 'placeholder'=>'Enter Description', 'class'
        => 'ckeditor']) !!}
        <span id="helpBlock2" class="help-block error">{{ $errors->first('description') }}</span>
    </div>

</div>
