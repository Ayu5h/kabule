@extends('layouts.admin')

@section('content')

{!! Form::model( $topic, [ 'method' => 'PATCH', 'action' => [ 'TopicController@update', $programId, $topic->id ] ] ) !!}

<div class='box'>
    <div class='box-body'>

        <div class="row">

            <div class="col-md-6">

                @include('admin.partials.image-upload')

            </div>

            <div class="col-md-6 uploaded-images">

                @include('admin.partials.image-selection', ['object' => $topic])

            </div>

            @include('admin.topic.form')

        </div>
    </div>
    <div class="box-footer">



        <button class="btn btn-success" type="submit" >
           <i class="fa fa-aw fa-save"></i> Save Program
        </button>

    </div>



</div>

{!! Form::close()!!}




@stop


@section('scripts.footer')

      @include('admin.partials.dropzone-script', [
          'url' => route('topic.addPhoto', [$topic->id]),
          'uploadType' => 'multiple'
      ])


@stop
