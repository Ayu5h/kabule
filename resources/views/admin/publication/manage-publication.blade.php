@extends('layouts.admin')

@section('content')
    <div class="box">
        <div class="box-body">
            <p>
                <a class="btn btn-success" href="{{ route('admin.publication.create') }}">
                    <i class="fa fa-aw fa-plus"></i>
                    Add Resource</a>
            </p>
            <div class="divider"></div>
            <div class="row">
                <div class="col-sm-12 table-responsive">
                    <table class="table table-bordered table-striped datatable">
                        <thead>
                        <tr>
                            <th class="col-md-2">SN</th>
                            <th class="col-md-8">Category</th>
                            <th class="col-md-2">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($publications as $k => $publication)
                            <tr>
                                <td>{{ ++$k }}</td>
                                <td>{{ $publication->category }}</td>
                                <td>
                                    <div class="dropdown action-menu">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                            <i class="fa fa-aw fa-cog fa-2x"></i></a>
                                        <ul class="dropdown-menu pull-right">
                                            <li>
                                                <a href="{{ route('admin.publication.show', [$publication->slug])  }}">Manage {{ $publication->category }}</a>
                                            </li>
                                            <li class="divider"></li>
                                            <li>
                                                <a href="{{ route('admin.publication.edit', [$publication->slug])  }}">Edit</a>
                                            </li>
                                            <li>
                                                {!! delete_form(route('admin.publication.destroy', [$publication->id]), 'Delete') !!}
                                            </li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div>

@stop

@section('scripts.footer')

    <script>
        $(function(){
            $('.datatable').dataTable({
                "pageLength": 2
            });
        });
    </script>

@stop