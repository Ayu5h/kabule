@extends('layouts.admin')

@section('content')

    {!! Form::model( $publication, [ 'method' => 'PATCH', 'action' => [ 'PublicationController@update', $publication->id ] ] ) !!}

    <div class='box'>
        <div class='box-body'>

            @include('admin.publication.form-publication')

        </div>

        <div class="box-footer">

            <button class="btn btn-success" type="submit">
                <i class="fa fa-aw fa-save"></i> Save Resource
            </button>

        </div>

    </div>

    {!! Form::close()!!}
@stop
