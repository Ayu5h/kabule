@extends('layouts.admin')

@section('content')

    {!! Form::open( [ 'method' => 'POST', 'url' => action('SubPublicationController@store', $publication->slug ) ] ) !!}

    <div class='box'>
        <div class='box-body'>

            @include('admin.publication.sub-publication.form')

        </div>

        <div class="box-footer">

            <button class="btn btn-success" type="submit">
                <i class="fa fa-aw fa-save"></i> Save {{ $publication->category }}
            </button>

        </div>

    </div>

    {!! Form::close()!!}
@stop
