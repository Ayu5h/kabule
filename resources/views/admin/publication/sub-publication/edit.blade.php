@extends('layouts.admin')

@section('content')

    {!! Form::model( $subPublication, [ 'method' => 'PATCH', 'url' => action('SubPublicationController@update', [$publication->slug, $subPublication->slug] ) ] ) !!}

    <div class='box'>
        <div class='box-body'>

            <div class="row">

                <div class="col-md-6">

                    @include('admin.partials.image-upload',['name' => 'Attachments'])

                </div>

                <div class="col-md-6">

                    @if($subPublication->attachments()->count() > 0)

                        <div class="form-group">
                            <label for="">Attachments:</label>
                            @foreach($subPublication->attachments as $attachment)
                                <div class="btn-group" style="display: table; margin-bottom: 5px;">
                                    <button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fa fa-file-pdf-o"></i> {{$attachment->name}} <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li><a href="{{ asset($attachment->path) }}" target="_blank">View</a></li>
                                        <li><a href="{{ action('MediaController@download', $attachment->id) }}">Download</a></li>
                                        <li role="separator" class="divider"></li>
                                        {{--Don't remove the empty form tag below. It's Important to solve weird issue caused by unknown source.
                                        Issue: The first form tag is being automatically removed.--}}
                                        {{--<li>--}}
                                            {{--<form action=""></form>--}}
                                        {{--</li>--}}
                                        {{--<li>{!! delete_form(route('media.destroy', [$attachment->id]), 'Delete') !!}</li>--}}
                                        <li><a href="javascript:;" class="file-delete-button" data-media-id="{{ $attachment->id }}">Delete</a></li>
                                    </ul>
                                </div>
                            @endforeach


                        </div>

                    @endif

                </div>

            </div>


            @include('admin.publication.sub-publication.form')


        </div>

        <div class="box-footer">

            <button class="btn btn-success" type="submit">
                <i class="fa fa-aw fa-save"></i> Save {{ $publication->category }}
            </button>

        </div>

    </div>

    {!! Form::close()!!}
@stop

@section('scripts.footer')

    @include('admin.partials.dropzone-script', [
        'url' => route('admin.subPublication.addPhoto', $subPublication->id),
        'uploadType' => 'multiple',
        'stopReload' => 'true',
        'paramName' => 'file',
        'acceptedFiles' => '.pdf'
    ])

@stop