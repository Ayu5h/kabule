<div class="row">

    <div class="col-md-6">

        <div class="form-group">
            <label for="title">Title <i class="fa fa-aw fa-asterisk"></i></label>
            {!! Form::text('title',null,['class' => 'form-control'])!!}
            <span id="title" class="help-block error">{{ $errors->first('title') }} </span>
        </div>

    </div>

    <div class="col-md-6">

        <div class="form-group">
            <label for="date">Date</label>
            {!! Form::input('date', 'date', null, ['class' => 'form-control', 'placeholder'=>'Enter Date']) !!}
            <span id="date" class="help-block error">{{ $errors->first('date') }} </span>
        </div>

    </div>

</div>

<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <label for="">Description <i class="fa fa-aw fa-asterisk"></i></label>
            {!! Form::textarea('description',null,['class' => 'form-control ckeditor'])!!}
        </div>
    </div>
</div>


