@extends('layouts.admin')

@section('content')
    <div class="box">
        <div class="box-body">
            <p>
                <a class="btn btn-success" href="{{ action('SubPublicationController@create', $publication->slug) }}">
                    <i class="fa fa-aw fa-plus"></i>
                    Add {{ $publication->category }}</a>
            </p>
            <div class="divider"></div>
            <div class="row">
                <div class="col-sm-12 table-responsive">
                    <table class="table table-bordered table-striped datatable">
                        <thead>
                        <tr>
                            <th class="col-md-1">SN</th>
                            <th class="col-md-3">Title</th>
                            <th class="col-md-2">No. of Attachments</th>
                            <th class="col-md-2">Date</th>
                            <th class="col-md-3">Description</th>
                            <th class="col-md-1">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($subPublications as $k => $subPublication)
                            <tr>
                                <td>{{ ++$k }}</td>
                                <td>{{ $subPublication->title }}</td>
                                <td>{{ $subPublication->attachments()->count() }}</td>
                                <td>{{ $subPublication->date }}</td>
                                <td>{!! str_limit($subPublication->description, 200, '...') !!} </td>
                                <td>
                                    <div class="dropdown action-menu">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                            <i class="fa fa-aw fa-cog fa-2x"></i></a>
                                        <ul class="dropdown-menu pull-right">
                                            <li>
                                                <a href="{{ action('SubPublicationController@edit', [$publication->slug, $subPublication->slug])  }}">Edit</a>
                                            </li>
                                            <li>
                                                {!! delete_form(action('SubPublicationController@destroy', [$publication->slug, $subPublication->slug]), 'Delete') !!}
                                            </li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div>

@stop

@section('scripts.footer')

    <script>
        $(function(){
            $('.datatable').dataTable({
                "pageLength": 2
            });
        });
    </script>

@stop