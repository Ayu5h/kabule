<div class="row">
    <div class="col-md-6">

        <div class="form-group">
            <label for="category">Category</label>
            {!! Form::input('text', 'category', null, ['class' => 'form-control', 'placeholder'=>'Enter Category']) !!}
            <span id="category" class="help-block error">{{ $errors->first('category') }} </span>
        </div>

    </div>
</div>