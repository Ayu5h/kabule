@extends('layouts.admin')

@section('content')

    {!! Form::open( array( 'url' => route("admin.publication.store") ) ) !!}

    <div class='box'>
        <div class='box-body'>

            @include('admin.publication.form-publication')

        </div>

        <div class="box-footer">
            <button class="btn btn-success" type="submit" >
                <i class="fa fa-aw fa-save"></i> Save Resource Category </button>
        </div>

    </div>

    {!! Form::close()!!}




@stop



