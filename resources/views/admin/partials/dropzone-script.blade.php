<script>

      var token = '{{ Session::token() }}';

      Dropzone.autoDiscover = false;

      var myDropzone = new Dropzone("div#myId", {
                                                    url: "{{ $url }}",
                                                    paramName: "{{ isset($paramName) ? $paramName : 'photo' }}",
                                                    maxFilesize: 2,
                                                    acceptedFiles: "{{ isset($acceptedFiles) ? $acceptedFiles : '.jpg, .jpeg, .png, .bmp' }}",


                                                  //TEMPORARY
                                                  @if($uploadType == 'multiple')
                                                    uploadMultiple: false,
                                                    parallelUploads: 25,
                                                  @elseif($uploadType == 'single')
                                                    maxFiles: 1,
                                                  @endif

                                                    addRemoveLinks: true,
                                                    autoProcessQueue: false,
                                                    sending: function(file, xhr, formData) {
                                                        formData.append("_token", token);
                                                    },
                                                    accept: function(file, done) {
                                                      done();
                                                    },
                                                    init: function() {

                                                      var myDropzone = this;

                                                      $("#upload-program-image").click(function (e) {
                                                          e.preventDefault();
                                                          e.stopPropagation();
                                                          myDropzone.processQueue();
                                                      });

                                                        @if( $uploadType == 'single' )
                                                          this.on("addedfile", function() {
                                                            if (this.files[1]!=null){
                                                              this.removeFile(this.files[0]);
                                                            }
                                                          });
                                                        @endif

                                                    },
                                                    queuecomplete: function() {

                                                        @if(!isset($stopReload))
                                                           location.reload();
                                                        @endif
                                                          $('#upload-program-image').closest('form').trigger('submit');
                                                    }
                                                });


</script>