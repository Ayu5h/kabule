                <div class="form-group">
                    <label for="file">Add {{ isset($name) ? $name : 'Images' }} Here:</label>
                    {!! Form::token() !!}
                    <div id="myId" class="dropzone">
                        <div class="fallback">
                            <input name="file" type="file"/>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <button type="button" id="upload-program-image" class="btn btn-sm btn-success">
                                    <i class="fa fa-aw fa-upload" id="image"></i> Upload {{ isset($name) ? $name : 'Images' }}
                    </button>
                </div>