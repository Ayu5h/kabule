@if($object->photos->count() > 0)

    <div class="form-group">
        <label for="">Select featured Image:</label>

        <div class="row">
            <div class="col-md-12">
                <ul style="margin: 0px; padding: 0px">
                    @foreach($object->photos as $photo)
                        <li class="program_gallery_image">
                            <label>
                                <input type="radio" name="featured_thumbnail"
                                       value="{{ $photo->thumbnail_path  }}" {{ $object->featured_thumbnail == $photo->thumbnail_path ? 'checked' : '' }} />
                                <input type="radio" name="featured_image"
                                       value="{{ $photo->path  }}" {{ $object->featured_image == $photo->path ? 'checked' : '' }} />
                                <img src="{{ asset($photo->thumbnail_path) }}" alt="" class="uploaded-image"/>

                                <i class="fa fa-trash-o fa-2x image-delete-button" title="Click to delete image"
                                   data-media-id="{{ $photo->id }}">
                                </i>
                            </label>
                        </li>
                    @endforeach
                </ul>
            </div>

        </div>
    </div>

@endif