@extends('layouts.admin')

@section('content')

    {!! Form::open(array( 'url' => route("admin.news.store"), 'class' => '' ) ) !!}

    <div class='box'>
        <div class='box-body'>

            @include('admin.site-news.form')

        </div>
        <div class="box-footer">



            <button class="btn btn-success" type="submit" >
                <i class="fa fa-aw fa-save"></i> Save News </button>
        </div>

    </div>

    {!! Form::close()!!}




@stop


@section('scripts.footer')

    <script>

        toggleLink();

        $('#select-type').on('change', toggleLink);

        function toggleLink() {

            if($('#select-type').val() == 0)
                $('#toggle-link').hide();
            else
                $('#toggle-link').show();

        }

    </script>


@stop


