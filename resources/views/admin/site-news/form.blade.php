<div class="row">
    <div class="col-md-6">
        <hr/>
        <div class="form-group">
            <label for="name">Name <i class="fa fa-aw fa-asterisk"></i></label>
            {!! Form::input('text', 'name', null, ['class' => 'form-control', 'placeholder'=>'Enter Name']) !!}
            <span id="helpBlock2" class="help-block error">{{ $errors->first('name') }} </span>
        </div>


        <div class="form-group">
            <label for="type">Type</label>
            {!! Form::select('type', [0 => 'Local', '1' => 'International'], null, ['class' => 'form-control', 'id' => 'select-type', 'placeholder'=>'Enter Type']) !!}
            <span id="helpBlock2" class="help-block error">{{ $errors->first('type') }} </span>
        </div>
    </div>

    <div class="col-md-6">
        <hr/>
        <div class="form-group">
            <label for="date">Date</label>
            {!! Form::input('date', 'date', null, ['class' => 'form-control', 'placeholder'=>'Enter Date']) !!}
            <span id="helpBlock2" class="help-block error">{{ $errors->first('date') }} </span>
        </div>


        <div class="form-group" id="toggle-link">
            <label for="link">Link</label>
            {!! Form::input('text', 'link', null, ['class' => 'form-control', 'placeholder'=>'Enter Link']) !!}
            <span id="helpBlock2" class="help-block error">{{ $errors->first('link') }} </span>
        </div>

    </div>
</div>

<div class="row">
    <div class="col-md-12">

        <div class="form-group">
            <label for="meta_title">Meta Title</label>
            {!! Form::input('text', 'meta_title', null, ['class' => 'form-control', 'placeholder'=>'Enter Meta Title']) !!}
            <span id="helpBlock2" class="help-block error">{{ $errors->first('meta_title') }} </span>
        </div>

        <div class="form-group">
            <label for="meta_description">Meta Description</label>
            {!! Form::textarea('meta_description', null, ['class' => 'form-control', 'placeholder'=>'Enter Meta Description', 'rows' => 5]) !!}
            <span id="helpBlock2" class="help-block error">{{ $errors->first('meta_description') }} </span>
        </div>

        <div class="form-group">
            <label for="description">Description <i class="fa fa-aw fa-asterisk"></i></label>
            {!! Form::textarea('description', null, ['class' => 'form-control', 'placeholder'=>'Enter Description', 'class' => 'ckeditor']) !!}
            <span id="helpBlock2" class="help-block error">{{ $errors->first('description') }} </span>
        </div>
    </div>
</div>
