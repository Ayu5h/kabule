@extends('layouts.admin')

@section('content')

    {!! Form::model($news, array( 'url' => route("admin.news.update", [$news->id]), 'method' => 'patch' ) ) !!}

    <div class='box'>

        <div class='box-body'>



                <div class="row">
                    <div class="col-md-6">
                        @include('admin.partials.image-upload')
                    </div>

                    <div class="col-md-6 uploaded-images">
                        @include('admin.partials.image-selection', ['object' => $news])
                    </div>
                </div>

                @include('admin.site-news.form')


        </div>

        <div class="box-footer">


            <button class="btn btn-success" type="submit">
                <i class="fa fa-aw fa-save"></i> Save News
            </button>
        </div>

    </div>

    {!! Form::close()!!}




@stop


@section('scripts.footer')

    <script>

        toggleLink();

        $('#select-type').on('change', toggleLink);

        function toggleLink() {

            if ($('#select-type').val() == 0)
                $('#toggle-link').hide();
            else
                $('#toggle-link').show();

        }

    </script>

    @include('admin.partials.dropzone-script', [
        'url' => route('admin.news.addPhoto', [$news->id]),
        'uploadType' => 'multiple'
    ]);


@stop


