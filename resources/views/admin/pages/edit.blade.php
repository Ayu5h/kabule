@extends('layouts.admin')

@section('content')

    {!! Form::open(['method' => 'DELETE', 'url' => route('admin.pages.destroy', $page->id)]) !!}
        <button class="btn btn-danger btn-sm" type="submit" 
        onclick="return confirm('Are you sure you want to delete this page?')"> <i class="fa fa-aw fa-trash-o"></i> Delete This Page?</button>
    {!! Form::close() !!}

    {!! Form::model( $page, [ 'method' => 'PATCH', 'url' => route('admin.pages.update', $page->id ) ] ) !!}

    <div class='box'>
        <div class='box-body'>

            <div class="row">
                <div class="col-md-6">

                    @include('admin.partials.image-upload')

                </div>

                <div class="col-md-6 uploaded-images">

                    @include('admin.partials.image-selection' , ['object' => $page])

                </div>

                <div class="clearfix"></div>

                @include('admin.pages.form')

            </div>
        </div>
        <div class="box-footer">
            <button class="btn btn-success" type="submit">
                <i class="fa fa-aw fa-save"></i> Upgrade Page
            </button>
        </div>

        @include('errors.list')

    </div>

    {!! Form::close()!!}
@stop

@section('scripts.footer')

    @include('admin.partials.dropzone-script', [
        'url' => route('admin.pages.storePhoto', [$page->id]),
        'uploadType' => 'multiple'
    ])

@stop



