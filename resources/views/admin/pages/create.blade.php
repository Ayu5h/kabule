@extends('layouts.admin')

@section('content')

    {!! Form::open(array( 'url' => route("admin.pages.store"), 'class' => '' ) ) !!}

    <div class='box'>
        <div class='box-body'>

            <div class="row">

                @include('admin.pages.form')

            </div>
        </div>
        <div class="box-footer">
            <button class="btn btn-success" type="submit">
                <i class="fa fa-aw fa-save"></i> Save Page
            </button>
        </div>

    </div>

    {!! Form::close()!!}
@stop



