<div class="col-md-6">

    <input type="hidden" name="type" value="0"/>

    <div class="form-group">
        <label for="title">Name <i class="fa fa-aw fa-asterisk"></i></label>
        {!! Form::input('text', 'name', null, ['class' => 'form-control', 'placeholder'=>'Enter Name']) !!}
        <span id="helpBlock2" class="help-block error">{{ $errors->first('name') }}</span>
    </div>

    <div class="form-group">
        <label for="title">Title <i class="fa fa-aw fa-asterisk"></i></label>
        {!! Form::input('text', 'title', null, ['class' => 'form-control', 'placeholder'=>'Enter Title']) !!}
        <span id="helpBlock2" class="help-block error">{{ $errors->first('title') }}</span>
    </div>

</div>

<div class="col-md-6">
    <div class="form-group">
        <label for="title">Meta Title </label>
        {!! Form::input('text', 'meta_title', null, ['class' => 'form-control', 'placeholder'=>'Enter meta title for SEO']) !!}
    </div>

    <div class="form-group">
        <label for="title">Meta Description </label>
        {!! Form::input('text', 'meta_description', null, ['class' => 'form-control', 'placeholder'=>'Enter meta description for SEO']) !!}
    </div>
</div>

<div class="col-md-12">
    <div class="form-group">
        <label for="description">Description <i class="fa fa-aw fa-asterisk"></i></label>
        {!! Form::textarea('description', null, ['class' => 'form-control', 'placeholder'=>'Enter Description', 'class'
        =>
        'ckeditor']) !!}
        <span id="helpBlock2" class="help-block error">{{ $errors->first('description') }}</span>
    </div>
</div>

