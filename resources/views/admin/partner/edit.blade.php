@extends('layouts.admin')

@section('content')

{!! Form::model( $partner, [ 'method' => 'PATCH', 'action' => [ 'PartnerController@update', $partner->id ] ] ) !!}

<div class='box'>
    <div class='box-body'>


           <div class="col-md-6">
                @include('admin.partials.image-upload')
            </div>

           <div class="col-md-6 uploaded-images">
                @include('admin.partials.image-selection', ['object' => $partner])
            </div>

           @include('admin.partner.form')

    </div>
    <div class="box-footer">



        <button class="btn btn-success" type="submit" >
           <i class="fa fa-aw fa-save"></i> Save Partner
        </button>

    </div>



</div>

{!! Form::close()!!}




@stop


@section('scripts.footer')


    @include('admin.partials.dropzone-script', [
        'url' => route('store_partner_photo', [$partner->id]),
        'uploadType' => 'multiple'
    ])


@stop
