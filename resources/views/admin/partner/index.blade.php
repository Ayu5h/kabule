@extends('layouts.admin')

@section('content')

<div class="box">
    <div class="box-body">
        <p>
            <a class="btn btn-success" href="{{ route('admin.partner.create') }}">
                <i class="fa fa-aw fa-plus"></i>
                Add New Partner </a>
        </p>

        <div class="divider"></div>
        <div class="table-responsive">

            <table class="table table-bordered table-striped datatable">
                <thead>
                    <tr>
                        <th class="col-md-1">SN</th>
                        <th class="col-md-2">Title</th>
                        <th class="col-md-2">Featured Image</th>
                        <th class="col-md-4">Link</th>
                        <th class="col-md-1">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($partners as $k => $partner)
                    <tr>
                        <td>{{ ++$k }}</td>
                        <td>{{ $partner->title }}</td>
                        <td><img src="{{ display_image($partner->featured_thumbnail, 192, 145) }}" class="admin-thumbnail" alt=""/></td>
                        <td>{{ $partner->link }}</td>
                        <td>
                            <div class="dropdown action-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-aw fa-cog fa-2x"></i></a>
                                <ul class="dropdown-menu pull-right">
                                    {{--<li><a href="{{ route('admin.partner.{id}.news.index', array($partner->id)) }}" >News</a></li>--}}
                                    {{--<li><a href="{{ route('admin.partner.{id}.topic.index', array($partner->id)) }}" >Topic</a></li>--}}
                                    {{--<li><a href="{{ route('admin.partner.{id}.activity.index', array($partner->id)) }}" >Activity</a></li>--}}
                                    <li class="divider"></li>
                                    <li><a href="{{ route('admin.partner.edit', array($partner->id)) }}" >Edit</a></li>
                                    <li>{!! delete_form(route('admin.partner.destroy', [$partner->id]), 'Delete') !!}</li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

    </div>
    <!-- /.box-body -->


</div>

@stop



@section('scripts.footer')

    <script>
        $(function(){
            $('.datatable').dataTable({
                "pageLength": 2
            });
        });
    </script>

@stop