@extends('layouts.admin')

@section('content')

{!! Form::open(array( 'url' => route("admin.partner.store"), 'class' => '' ) ) !!}

<div class='box'>
    <div class='box-body'>


                @include('admin.partner.form')


    </div>
    <div class="box-footer">



        <button class="btn btn-success" type="submit" >
            <i class="fa fa-aw fa-save"></i> Save Partner </button>
    </div>

</div>

{!! Form::close()!!}




@stop



