@extends('layouts.admin')

@section('content')

{!! Form::open(array( 'url' => route("admin.disaster.store"), 'class' => '' ) ) !!}

<div class='box'>
    <div class='box-body'>

        <div class="row">


                @include('admin.disaster.form')


        </div>
    </div>
    <div class="box-footer">



        <button class="btn btn-success" type="submit" >
            <i class="fa fa-aw fa-save"></i> Save Disaster Response </button>
    </div>

</div>

{!! Form::close()!!}




@stop



