@extends('layouts.admin')

@section('content')

<div class="box">
    <div class="box-body">
        <p>
            <a class="btn btn-success" href="{{ route('admin.disaster.create') }}">
                <i class="fa fa-aw fa-plus"></i>
                Add New Disaster Response </a>
        </p>

        <div class="divider"></div>
        <div class="table-responsive">

            <table class="table table-bordered table-striped datatable">
                <thead>
                    <tr>
                        <th class="col-md-1">SN</th>
                        <th class="col-md-2">Title</th>
                        <th class="col-md-2">Featured Image</th>
                        <th class="col-md-2">Intro Text</th>
                        <th class="col-md-4">Description</th>
                        <th class="col-md-1">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($disasters as $k => $disaster)
                    <tr>
                        <td>{{ ++$k }}</td>
                        <td>{{ $disaster->title }}</td>
                        <td><img src="{{ display_image($disaster->featured_thumbnail, 192, 145) }}" class="admin-thumbnail" alt=""/></td>

                        <td>{{ $disaster->intro_text }}</td>

                        <td>{!! substr($disaster->description,0,200) !!}........</td>
                        <td>
                            <div class="dropdown action-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-aw fa-cog fa-2x"></i></a>
                                <ul class="dropdown-menu pull-right">
                                    {{--<li><a href="{{ route('admin.disaster.{id}.news.index', array($disaster->id)) }}" >News</a></li>--}}
                                    {{--<li><a href="{{ route('admin.disaster.{id}.topic.index', array($disaster->id)) }}" >Topic</a></li>--}}
                                    {{--<li><a href="{{ route('admin.disaster.{id}.activity.index', array($disaster->id)) }}" >Activity</a></li>--}}
                                    <li class="divider"></li>
                                    <li><a href="{{ route('admin.disaster.edit', array($disaster->id)) }}" >Edit</a></li>
                                    <li>{!! delete_form(route('admin.disaster.destroy', [$disaster->id]), 'Delete') !!}</li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

    </div>
    <!-- /.box-body -->


</div>

@stop



@section('scripts.footer')

    <script>
        $(function(){
            $('.datatable').dataTable({
                "pageLength": 2
            });
        });
    </script>

@stop