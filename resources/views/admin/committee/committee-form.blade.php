<div class="col-md-6">
    <div class="form-group">
        <label for="">Name <i class="fa fa-aw fa-asterisk"></i></label>
        {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Enter name of the committee'] )!!}
    </div>
</div>

<div class="col-md-6">
    <div class="form-group">
        <label for="show_on_home_page">Has Designation <i class="fa fa-aw fa-asterisk"></i></label>
        {!! Form::select('has_designations',['1'=>'Yes','0'=>'No'], null, ['class' => 'form-control', 'placeholder'=>'Has Designation?']) !!}
    </div>
</div>

<div class="col-md-12">
    <div class="form-group">
        <label for="title">Meta Title </label>
        {!! Form::input('text', 'meta_title', null, ['class' => 'form-control', 'placeholder'=>'Enter meta title for SEO'])
        !!}
    </div>

    <div class="form-group">
        <label for="title">Meta Description </label>
        {!! Form::input('text', 'meta_description', null, ['class' => 'form-control', 'placeholder'=>'Enter meta description for SEO']) !!}
    </div>
</div>