@extends('layouts.admin')

@section('content')

    {!! Form::open(array( 'action' => ['CommitteeController@store'], 'class' => '' ) ) !!}
    <div class='box'>
        <div class='box-body'>

            <div class="row">
                @include('admin.committee.committee-form')
            </div>
        </div>
        <div class="box-footer">

            <button class="btn btn-success" type="submit">
                <i class="fa fa-aw fa-save"></i> Save Committee
            </button>

        </div>

        @include('errors.list')

    </div>

    {!! Form::close()!!}
@stop