@extends('layouts.admin')
@section('content')
    <div class="box">
        <div class="box-body">
            <p>
                <a class="btn btn-success" href="{{ route('admin.committee.create') }}">
                    <i class="fa fa-aw fa-plus"></i>
                    Add Committee </a>
            </p>
            <div class="divider"></div>
            <div class="row">
                <div class="col-sm-12 table-responsive">
                    <table class="table table-bordered table-striped datatable">
                        <thead>
                        <tr>
                            <th>SN</th>
                            <th>Name</th>
                            <th>Created Date</th>
                            <th>Has Designations</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($committees as $k => $committee)
                            <tr>
                                <td>{{ ++$k }}</td>
                                <td>{{ $committee->name }}</td>
                                <td>{{ date('jS F Y', strtotime($committee->created_at)) }}</td>
                                <td>{{ $committee->hasDesignation() }}</td>
                                <td>
                                    <div class="dropdown action-menu">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i
                                                    class="fa fa-aw fa-cog fa-2x"></i></a>
                                        <ul class="dropdown-menu">
                                            <li>
                                                <a href="{{ route('admin.committee.edit', [$committee->id])  }}">Edit</a>
                                            </li>
                                            <li>
                                                {!! delete_form(route('admin.committee.destroy', [$committee->id]), 'Delete') !!}
                                            </li>
                                            <li>
                                                <a href="{{ route('admin.committee.{committeeId}.member.index', [$committee->id]) }}">Members</a>
                                            </li>

                                        </ul>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div>

@stop

@section('scripts.footer')

    <script>
        $(function(){
            $('.datatable').dataTable({
                "pageLength": 2
            });
        });
    </script>

@stop