@extends('layouts.admin')
@section('content')
    <div class="box">
        <div class="box-body">
            <p>
                <a class="btn btn-success" href="{{ route('admin.committee.{committeeId}.member.create',[$committeeId]) }}">
                    <i class="fa fa-aw fa-plus"></i>
                    Add Member </a>
            </p>

            <div class="divider"></div>
            <div class="row">
                <div class="col-sm-12 table-responsive">
                    <table class="table table-bordered table-striped datatable">
                        <thead>
                        <tr>
                            <th>SN</th>
                            <th>Image</th>
                            <th>Name</th>
                            @if( $committee->has_designations )
                                <th>Designation</th>
                            @endif
                            <th>Email</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($committeeMembers as $k => $committeeMember)
                            <tr>
                                <td>
                                    {{ ++$k }}
                                </td>
                                <td>
                                    @if(!empty($committeeMember->image))
                                        <img src="{{ asset($committeeMember->image_thumbnail) }}" class="admin-thumbnail"/>
                                    @else
                                        <img src="http://fakeimg.pl/350x200/?text=No Image" class="admin-thumbnail">
                                    @endif
                                </td>

                                <td>
                                    {{ $committeeMember->name }}
                                </td>
                                @if( $committee->has_designations )
                                    <td>
                                        {{ $committeeMember->designation }}
                                    </td>
                                @endif
                                <td>
                                    {{ $committeeMember->email }}
                                </td>
                                <td>
                                    <div class="dropdown action-menu">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i
                                                    class="fa fa-aw fa-cog fa-2x"></i></a>
                                        <ul class="dropdown-menu pull-right">
                                            <li>
                                                <a href="{{ route('admin.committee.{committeeId}.member.edit', [$committeeId, $committeeMember->id])  }}">Edit</a>
                                            </li>
                                            <li>
                                                {!! delete_form(route('admin.committee.{committeeId}.member.destroy', [$committeeId, $committeeMember->id]), 'Delete') !!}
                                            </li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div>

@stop

@section('scripts.footer')

    <script>
        $(function () {
            $('.datatable').dataTable({
                "pageLength": 2
            });
        });
    </script>

@stop