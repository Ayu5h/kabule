@extends('layouts.admin')

@section('content')

    {!! Form::open(array( 'url' => route('admin.committee.{committeeId}.member.store', [$committeeId]), 'class' => '' ) ) !!}
    <div class='box'>
        <div class='box-body'>

            <div class="row">

                <div class="col-md-6">

                    @include('admin.committee.members.committee-member-form')

                </div>

            </div>
        </div>
        <div class="box-footer">

            <button class="btn btn-success" type="submit" >
                <i class="fa fa-aw fa-save"></i> Save Member
            </button>

        </div>


    </div>

    {!! Form::close()!!}
@stop