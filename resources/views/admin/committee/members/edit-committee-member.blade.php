@extends('layouts.admin')

@section('content')

    {!! Form::model( $committeeMember, [ 'method' => 'PATCH', 'action' => [ 'MemberController@update', $committeeId, $committeeMember->id ] ] ) !!}

    <div class='box'>
        <div class='box-body'>

            <div class="row">

                <div class="col-md-6">

                    @include('admin.committee.members.committee-member-form')

                </div>

                <div class="col-md-6">

                    <div class="form-group">
                        <label for="file">Add Member Image</label>
                        {!! Form::token() !!}
                        <div id="memberId" class="dropzone">
                            <div class="fallback">
                                <input name="photo" type="file"/>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <button type="button" id="upload-member-image" class="btn btn-sm btn-success">
                            <i class="fa fa-aw fa-upload" id="image"></i> Upload Image
                        </button>
                    </div>

                    @if($committeeMember->image)

                        <div class="form-group">
                            <img src="/{{$committeeMember->image_thumbnail}}" alt=""/>
                        </div>

                    @endif


                </div>

            </div>
        </div>
        <div class="box-footer">

            <button class="btn btn-success" type="submit" >
                <i class="fa fa-aw fa-save"></i> Save Member
            </button>

        </div>


    </div>

    {!! Form::close()!!}
@stop

@section('scripts.footer')


    <script>


        var token = '{{ Session::token() }}';
        Dropzone.autoDiscover = false;
        var myDropzone = new Dropzone("div#memberId", {
            url: "{{ route('addMemberPhoto', [$committeeMember->id]) }}",
            paramName: "photo",
            maxFilesize: 2,
            acceptedFiles: '.jpg, .jpeg, .png, .bmp',
//                                                uploadMultiple: false,
//                                                parallelUploads: 25,
            maxFiles: 1,
            addRemoveLinks: true,
            autoProcessQueue: false,
            sending: function(file, xhr, formData) {
                formData.append("_token", token);
            },
            accept: function(file, done) {
                console.log("uploaded");
                done();
            },
            init: function() {

                var myDropzone = this;

                $("#upload-member-image").click(function (e) {
                    e.preventDefault();
                    e.stopPropagation();
                    myDropzone.processQueue();
                });

                this.on("addedfile", function() {
                    if (this.files[1]!=null){
                        this.removeFile(this.files[0]);
                    }
                });


            },
            queuecomplete: function() {
                console.log('all finished');
                //location.reload();
            }
        });



    </script>




@stop