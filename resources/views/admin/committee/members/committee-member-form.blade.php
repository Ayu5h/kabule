{{--{!! Form::hidden('committee_id', !empty($committeeId) ? $committeeId : '' ) !!}--}}
{{--{!! Form::hidden('committee_member_id', !empty($committeeMember->id) ? $committeeMember->id : '' )!!}--}}

<div class="form-group">
    <label for="">Name <i class="fa fa-aw fa-asterisk"></i></label>
    {!! Form::text('name',null,['class' => 'form-control'])!!}
    <span id="helpBlock2" class="help-block error">{{ $errors->first('name') }} </span>
</div>

@if( $committee->has_designations )
    <div class="form-group">
        <label for="">Designation <i class="fa fa-aw fa-asterisk"></i></label>
        {!! Form::text('designation',null,['class' => 'form-control'])!!}
        <span id="helpBlock2" class="help-block error">{{ $errors->first('designation') }} </span>
    </div>
@endif

<div class="form-group">
    <label for="">Email <i class="fa fa-aw fa-asterisk"></i></label>
    {!! Form::text('email',null,['class' => 'form-control'])!!}
    <span id="helpBlock2" class="help-block error">{{ $errors->first('email') }} </span>
</div>

<div class="form-group">
    <label for="">Description</label>
    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
    <span id="helpBlock2" class="help-block error">{{ $errors->first('description') }} </span>
</div>


