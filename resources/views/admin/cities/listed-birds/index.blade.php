@extends('layouts.admin')

@section('content')

    <div class="box">
        <div class="box-body">
            <p>
                <a class="btn btn-success" href="{{ route('admin.cities-categories.{id}.listed-birds.create', $category->id) }}">
                    <i class="fa fa-aw fa-plus"></i>
                    Add Listed Birds </a>

                <a class="btn btn-warning" href="{{ route('admin.cities-categories.{id}.listed-birds.multi-edit', $category->id) }}">
                    <i class="fa fa-aw fa-pencil"></i>
                    Multi Edit Listed Birds </a>
            </p>

            <div class="divider"></div>
            <div class="table-responsive">

                <table class="table table-bordered table-striped datatable">
                    <thead>
                    <tr>
                        <th class="col-md-1">SN</th>
                        <th class="col-md-4">Name</th>
                        <th class="col-md-4">Appendix</th>
                        <th class="col-md-1">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($listedBirds as $k => $listedBird)
                        <tr>
                            <td>{{ ++$k }}</td>
                            <td>{{ $listedBird->name }}</td>
                            <td>{{ $listedBird->appendix }}</td>
                            <td>
                                <div class="dropdown action-menu">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-aw fa-cog fa-2x"></i></a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="{{ route('admin.cities-categories.{id}.listed-birds.edit', array($listedBird->cities_category_id, $listedBird->id)) }}" >Edit</a></li>
                                        <li>
                                            {!! delete_form(route('admin.cities-categories.{id}.listed-birds.destroy', [$listedBird->cities_category_id, $listedBird->id])) !!}
                                        </li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

        </div>
        <!-- /.box-body -->


    </div>

@stop



@section('scripts.footer')

    <script>
        $(function(){
            $('.datatable').dataTable({
                "pageLength": 2
            });
        });
    </script>

@stop