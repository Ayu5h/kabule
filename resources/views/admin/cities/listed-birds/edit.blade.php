@extends('layouts.admin')

@section('content')

    {!! Form::model( $listedBird, [ 'method' => 'PATCH', 'url' => route('admin.cities-categories.{id}.listed-birds.update', [$category->id, $listedBird->id] ) ] ) !!}

    <div class='box'>
        <div class='box-body'>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="title">Name <i class="fa fa-aw fa-asterisk"></i></label>
                        {!! Form::input('text', 'name', null, ['class' => 'form-control', 'placeholder'=>'Enter Name']) !!}
                        <span class="help-block error">{{ $errors->first('name') }}</span>
                    </div>

                    <div class="form-group">
                        <label for="description">Appendix <i class="fa fa-aw fa-asterisk"></i></label>
                        {!! Form::input('text', 'appendix', null, ['class' => 'form-control', 'placeholder'=>'Enter Appendix']) !!}
                        <span class="help-block error">{{ $errors->first('appendix') }}</span>
                    </div>
                </div>

            </div>
        </div>
        <div class="box-footer">
            <button class="btn btn-default" type="submit">
                <i class="fa fa-aw fa-save"></i> Edit Listed Bird
            </button>
        </div>

    </div>

    {!! Form::close()!!}
@stop



