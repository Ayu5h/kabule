@extends('layouts.admin')

@section('content')

    {!! Form::open(array( 'url' => route("admin.cities-categories.{id}.listed-birds.multi-update", $category->id), 'class' => '',
    'method' => 'PATCH' ) ) !!}

    <div class='box'>
        <div class='box-body'>

            <div class="row">
                <div class="col-md-8">
                    <div class="table-responsive">

                        <table class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Appendix</th>
                                <th>Action</th>
                            </tr>
                            </thead>

                            <tbody>
                            @foreach( $listedBirds as $listedBird )
                                <tr>
                                    <td>
                                        {!! Form::input('text', 'name[]', $listedBird->name, ['class' => 'form-control',
                                        'placeholder'=>'Enter Name']) !!}
                                    </td>
                                    <td>
                                        {!! Form::input('text', 'appendix[]', $listedBird->appendix, ['class' => 'form-control',
                                        'placeholder'=>'Enter Appendix']) !!}
                                    </td>
                                    <td>
                                        {!! deleteButton(route('admin.cities-categories.{id}.listed-birds.multi-edit.delete', [$listedBird->cities_category_id, $listedBird->id, TRUE])) !!}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>

                            <tfoot>
                            <tr>
                                <td colspan="3" style="text-align: right">
                                    <button class="btn btn-sm btn-default simple-add-row"><i
                                                class="fa fa-aw fa-plus-circle"></i> Add New Row
                                    </button>
                                </td>
                            </tr>
                            </tfoot>

                        </table>
                    </div>
                </div>

            </div>
        </div>
        <div class="box-footer">
            <button class="btn btn-default" type="submit">
                <i class="fa fa-aw fa-save"></i> Save Listed Birds
            </button>
        </div>

    </div>

    {!! Form::close()!!}
@stop



