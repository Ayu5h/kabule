@extends('layouts.admin')

@section('content')

    {!! Form::open(array( 'url' => route("admin.cities-categories.{id}.listed-birds.store", $category->id), 'class' => '') ) !!}

    <div class='box'>
        <div class='box-body'>

            <div class="row">
                <div class="col-md-8">
                    @include('admin.cities.listed-birds.form')
                </div>

            </div>
        </div>
        <div class="box-footer">
            <button class="btn btn-default" type="submit">
                <i class="fa fa-aw fa-save"></i> Save Listed Birds
            </button>
        </div>

    </div>

    {!! Form::close()!!}
@stop



