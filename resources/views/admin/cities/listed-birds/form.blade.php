<div class="table-responsive">

    <table class="table table-bordered table-striped">
        <thead>
        <tr>
            <th>Name</th>
            <th>Appendix</th>
            <th>Action</th>
        </tr>
        </thead>

        <tbody>
        @for( $i = 0; $i < 4; $i++ )
            <tr>
                <td>
                    {!! Form::input('text', 'name[]', null, ['class' => 'form-control', 'placeholder'=>'Enter Name']) !!}
                </td>
                <td>
                    {!! Form::input('text', 'appendix[]', null, ['class' => 'form-control', 'placeholder'=>'Enter Appendix']) !!}
                </td>
                <td>
                    <button class="btn btn-danger btn-sm simple-delete-row">
                        <i class="fa fa-aw fa-trash-o"></i>
                        Delete
                    </button>
                </td>
            </tr>
        @endfor
        </tbody>

        <tfoot>
            <tr>
                <td colspan="3" style="text-align: right">
                    <button class="btn btn-sm btn-default simple-add-row"> <i class="fa fa-aw fa-plus-circle"></i> Add New Row</button>
                </td>
            </tr>
        </tfoot>

    </table>
</div>