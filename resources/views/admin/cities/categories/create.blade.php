@extends('layouts.admin')

@section('content')

    {!! Form::open(array( 'url' => route("admin.cities-categories.store"), 'class' => '' ) ) !!}

    <div class='box'>
        <div class='box-body'>

            <div class="row">
                <div class="col-md-6">
                    @include('admin.cities.categories.form')
                </div>

            </div>
        </div>
        <div class="box-footer">
            <button class="btn btn-success" type="submit">
                <i class="fa fa-aw fa-save"></i> Save Category
            </button>
        </div>

    </div>

    {!! Form::close()!!}
@stop



