@extends('layouts.admin')

@section('content')

    {!! Form::model( $category, [ 'method' => 'PATCH', 'url' => route('admin.cities-categories.update', $category->id ) ] ) !!}

    <div class='box'>
        <div class='box-body'>

            <div class="row">
                <div class="col-md-6">
                    @include('admin.cities.categories.form')
                </div>

            </div>
        </div>
        <div class="box-footer">
            <button class="btn btn-success" type="submit">
                <i class="fa fa-aw fa-save"></i> Edit Category
            </button>
        </div>

    </div>

    {!! Form::close()!!}
@stop



