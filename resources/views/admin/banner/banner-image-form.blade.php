<div class="col-md-6">
    <div class="form-group">

        <label for="" class="control-label">Title <i class="fa fa-aw fa-asterisk"></i></label>
        {!! Form::text('title',null,['class' => 'form-control'])!!}
        <span id="helpBlock2" class="help-block error">{{ $errors->first('title') }}</span>
    </div>
</div>

<div class="col-md-6">

    <div class="form-group">
        <label for="">Link To Page <i class="fa fa-aw fa-asterisk"></i></label>
        {!! Form::select('links_to_page',['1'=>'Yes','0'=>'No'], null, ['class' => 'form-control link_to_page']) !!}
    </div>

    {{--<div class="form-group link">--}}
        {{--<label for="" class="control-label">Link</label>--}}
        {{--{!! Form::text('link',null,['class' => 'form-control'])!!}--}}
    {{--</div>--}}

</div>

<div class="col-md-12">

    <div class="form-group">

        <label for="" class="control-label">Description</label>
        {!! Form::textarea('description',null,['class' => 'form-control'])!!}
        <span id="helpBlock2" class="help-block error">{{ $errors->first('description') }}</span>

    </div>

    {{--<div class="form-group">--}}

        {{--<label for="" class="control-label">Meta Title</label>--}}
        {{--{!! Form::text('meta_title',null,['class' => 'form-control'])!!}--}}

    {{--</div>--}}

    {{--<div class="form-group">--}}

        {{--<label for="" class="control-label">Meta Description</label>--}}
        {{--{!! Form::text('meta_description',null,['class' => 'form-control'])!!}--}}

    {{--</div>--}}

</div>