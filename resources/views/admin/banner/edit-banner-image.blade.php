@extends('layouts.admin')

@section('breadcrumbs', Breadcrumbs::render('edit-banner'))

@section('content')

    {!! Form::model( $bannerImage, [ 'method' => 'PATCH', 'action' => [ 'BannerController@update', $bannerImage->id ] ] ) !!}

    <div class='box'>
        <div class='box-body'>

            <div class="row">

                <div class="col-md-6">

                    <div class="form-group">
                        <label for="file">Add Banner Image of Size: 620 X 310</label>
                        {!! Form::token() !!}
                        <div id="bannerImageId" class="dropzone">
                            <div class="fallback">
                                <input name="photo" type="file"/>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <button type="button" id="upload-banner-image" class="btn btn-sm btn-success">
                            <i class="fa fa-aw fa-upload" id="image"></i> Upload Image
                        </button>
                    </div>
                </div>

                <div class="col-md-6">
                    @if($bannerImage->image)

                        <div class="form-group">
                            <img src="/{{$bannerImage->image_thumbnail}}" alt=""/>
                        </div>

                    @endif


                </div>

                <div class="clearfix"></div>

                @include('admin.banner.banner-image-form')

            </div>
        </div>
        <div class="box-footer">

            <button class="btn btn-success" type="submit">
                <i class="fa fa-aw fa-save"></i> Save Banner
            </button>

        </div>

    </div>

    {!! Form::close()!!}
@stop
@section('scripts.footer')


    <script>

        $(function () {
            var link_to_page = $('.link_to_page').val()
            if (link_to_page == 1) {
                $('div.link').show();
            } else {
                $('div.link').hide();
            }
            $('.link_to_page').on('change', function (e) {
                e.preventDefault();
                var $this = $(this);
                if ($this.val() == 1) {
                    $('div.link').show();
                } else {
                    $('div.link').hide();
                }
            });
        });


        var token = '{{ Session::token() }}';
        Dropzone.autoDiscover = false;
        var myDropzone = new Dropzone("div#bannerImageId", {
            url: "{{ route('addBannerImage', [$bannerImage->id]) }}",
            paramName: "photo",
            maxFilesize: 5,
            acceptedFiles: '.jpg, .jpeg, .png, .bmp',
//                                                uploadMultiple: false,
//                                                parallelUploads: 25,
            maxFiles: 1,
            addRemoveLinks: true,
            autoProcessQueue: false,
            sending: function (file, xhr, formData) {
                formData.append("_token", token);
            },
            accept: function (file, done) {
                console.log("uploaded");
                done();
            },
            init: function () {

                var myDropzone = this;

                $("#upload-banner-image").click(function (e) {
                    e.preventDefault();
                    e.stopPropagation();
                    myDropzone.processQueue();
                });

                this.on("addedfile", function () {
                    if (this.files[1] != null) {
                        this.removeFile(this.files[0]);
                    }
                });


            },
            queuecomplete: function () {
                console.log('all finished');
                //location.reload();
            }
        });


    </script>




@stop