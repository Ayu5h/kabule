@extends('layouts.admin')

@section('breadcrumbs', Breadcrumbs::render('banner'))

@section('content')
    <div class="box">
        <div class="box-body">
            <p>
                <a class="btn btn-success" href="{{ route('banner.create') }}">
                    <i class="fa fa-aw fa-plus"></i>
                    Add Banner </a>
            </p>

            <div class="divider"></div>
            <div class="row">
                <div class="col-sm-12 table-responsive">

                    <table class="table table-bordered table-striped datatable">
                        <thead>
                        <tr>
                            <th class="col-sm-1">SN</th>
                            <th class="col-sm-3">Banner Image</th>
                            <th class="col-sm-4">Title</th>
                            {{--<th class="col-sm-3">Link</th>--}}
                            <th class="col-sm-1">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($bannerImages as $k => $bannerImage)
                            <tr>
                                <td>{{ ++$k }}</td>
                                <td>
                                    @if( !empty($bannerImage->image))
                                    <img src="{{ $bannerImage->image_thumbnail }}" width="100" height="100"/>
                                    @else
                                        <img src="http://fakeimg.pl/100x100/?text=No Image">
                                    @endif
                                </td>

                                <td>
                                    {{ $bannerImage->title }}
                                </td>
                                {{--<td>{{ $bannerImage->link }}</td>--}}
                                <td>
                                    <div class="dropdown action-menu">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i
                                                    class="fa fa-aw fa-cog fa-2x"></i></a>
                                        <ul class="dropdown-menu pull-right">
                                            <li>
                                                <a href="{{ route('banner.edit',[$bannerImage->id]) }}">Edit</a>
                                            </li>
                                            <li>
                                                {!! delete_form(route('banner.destroy', [$bannerImage->id]), 'Delete') !!}
                                            </li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div>

@stop

@section('scripts.footer')

    <script>
        $(function(){
            $('.datatable').dataTable({
                "pageLength": 2
            });
        });
    </script>

@stop