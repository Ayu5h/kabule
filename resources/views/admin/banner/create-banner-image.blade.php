@extends('layouts.admin')

@section('breadcrumbs', Breadcrumbs::render('create-banner'))

@section('content')

    {!! Form::open(array( 'url' => route("banner.store"), 'class' => '' ) ) !!}

    <div class='box'>
        <div class='box-body'>

            <div class="row">

                @include('admin.banner.banner-image-form')

            </div>
        </div>
        <div class="box-footer">


            <button class="btn btn-success" type="submit">
                <i class="fa fa-aw fa-save"></i> Save Banner
            </button>
        </div>

    </div>

    {!! Form::close()!!}

@stop

@section('scripts.footer')
    <script>
        $(function () {
            var link_to_page = $('.link_to_page').val()
            if (link_to_page == 1) {
                $('div.link').show();
            } else {
                $('div.link').hide();
            }
            $('.link_to_page').on('change', function (e) {
                e.preventDefault();
                var $this = $(this);
                if ($this.val() == 1) {
                    $('div.link').show();
                } else {
                    $('div.link').hide();
                }
            });
        });
    </script>
@stop



