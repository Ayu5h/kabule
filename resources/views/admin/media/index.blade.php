@extends('layouts.admin')

@section('content')

    {!! Form::model( $album, array( 'method' => 'PATCH', 'url' => route("admin.albums.update", [$album->id]) ) ) !!}

    <div class='box'>
        <div class='box-body'>

            <div class="row">

                <div class="col-md-6">
                    @include('admin.partials.image-upload')
                </div>

                <div class="col-md-6 uploaded-images">
                    @include('admin.partials.image-selection', ['object' => $album])
                </div>

            </div>

        </div>

        <div class="box-footer">

            <button class="btn btn-success" type="submit">
                <i class="fa fa-aw fa-save"></i> Save Album
            </button>

        </div>

    </div>

    {!! Form::close()!!}




@stop

@section('scripts.footer')

    @include('admin.partials.dropzone-script', [
        'url' => route('admin.albums.{id}.upload', [$album->id]),
        'uploadType' => 'multiple'
    ])


@stop