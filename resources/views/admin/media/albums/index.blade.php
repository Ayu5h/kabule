@extends('layouts.admin')

@section('content')
    <div class="box">
        <div class="box-body">
            <p>
                <a class="btn btn-success" href="{{ route('admin.albums.create') }}">
                    <i class="fa fa-aw fa-plus"></i>
                    Add Album</a>
            </p>
            <div class="divider"></div>
            <div class="row">
                <div class="col-sm-12 table-responsive">
                    <table class="table table-bordered table-striped datatable">
                        <thead>
                        <tr>
                            <th class="col-md-2">SN</th>
                            <th class="col-md-3">Image</th>
                            <th class="col-md-3">Name</th>
                            <th class="col-md-2">Created Date</th>
                            <th class="col-md-2">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($albums as $k => $album)
                            <tr>
                                <td>{{ ++$k }}</td>
                                <td>
                                    <img src="{{ asset($album->featured_thumbnail) }}" class="admin-thumbnail"/>
                                </td>
                                <td>{{ $album->name }}</td>
                                <td>{{ $album->createdDate() }}</td>
                                <td>
                                    <div class="dropdown action-menu">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-aw fa-cog fa-2x"></i></a>
                                        <ul class="dropdown-menu pull-right">
                                            <li><a href="{{ route('admin.albums.{id}.galleries.index', array($album->id)) }}" >Gallery</a></li>
                                            <li><a href="{{ route('admin.albums.edit', array($album->id)) }}" >Edit</a></li>
                                            <li>
                                                {!! delete_form(route('admin.albums.destroy', [$album->id]), 'Delete') !!}
                                            </li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div>

@stop

@section('scripts.footer')

    <script>
        $(function(){
            $('.datatable').dataTable({
                "pageLength": 2
            });
        });
    </script>

@stop