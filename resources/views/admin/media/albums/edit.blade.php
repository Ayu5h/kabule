@extends('layouts.admin')

@section('content')

    {!! Form::model( $album, [ 'method' => 'PATCH', 'url' => route('admin.albums.update', $album->id ) ] ) !!}

    <div class='box'>
        <div class='box-body'>

            @include('admin.media.albums.form')

        </div>

        <div class="box-footer">

            <button class="btn btn-success" type="submit">
                <i class="fa fa-aw fa-save"></i> Save Album
            </button>

        </div>

    </div>

    {!! Form::close() !!}
@stop
