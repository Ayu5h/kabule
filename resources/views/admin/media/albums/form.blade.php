<div class="row">
    <div class="col-md-6">

        <div class="form-group">
            <label for="category">Name</label>
            {!! Form::input('text', 'name', null, ['class' => 'form-control', 'placeholder'=>'Enter Name Of Album']) !!}
            <span id="category" class="help-block error">{{ $errors->first('name') }} </span>
        </div>


        <div class="form-group">
            <label for="category">Created Date</label>
            {!! Form::input('date', 'created_date', null, ['class' => 'form-control', 'placeholder'=>'Enter Created Date']) !!}
            <span id="category" class="help-block error">{{ $errors->first('created_date') }} </span>
        </div>

    </div>
</div>