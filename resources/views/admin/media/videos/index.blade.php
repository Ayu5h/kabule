@extends('layouts.admin')

@section('content')

    <div class="box">
        <div class="box-body">
            <p>
                <a class="btn btn-success" href="{{ route('admin.video-categories.{id}.videos.create', $category->id) }}">
                    <i class="fa fa-aw fa-plus"></i>
                    Add New Video </a>
            </p>

            <div class="divider"></div>
            <div class="table-responsive">

                <table class="table table-bordered table-striped datatable">
                    <thead>
                    <tr>
                        <th class="col-md-1">SN</th>
                        <th class="col-md-2">Image</th>
                        <th class="col-md-2">Name</th>
                        <th class="col-md-1">Created At</th>
                        <th class="col-md-1">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($videos as $k => $video)
                        <tr>
                            <td>{{ ++$k }}</td>
                            <td>
                                <img src="{{ asset($video->photos()->first()->path) }}" alt="" class="admin-thumbnail"/>
                            </td>
                            <td>{{ $video->name }}</td>
                            <td>{{ $video->created_at }}</td>
                            <td>
                                <div class="dropdown action-menu">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-aw fa-cog fa-2x"></i></a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="{{ route('admin.video-categories.{id}.videos.edit', array($video->category_id, $video->id)) }}" >Edit</a></li>
                                        <li>
                                            {!! delete_form(route('admin.video-categories.{id}.videos.destroy', [$video->category_id, $video->id])) !!}
                                        </li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

        </div>
        <!-- /.box-body -->


    </div>

@stop



@section('scripts.footer')

    <script>
        $(function(){
            $('.datatable').dataTable({
                "pageLength": 2
            });
        });
    </script>

@stop