@extends('layouts.admin')

@section('content')

    {!! Form::open(array( 'url' => route("admin.video-categories.{id}.videos.store", $category->id), 'class' => '' ) ) !!}

    <div class='box'>
        <div class='box-body'>

            <div class="row">

                @include('admin.media.videos.form')

            </div>
        </div>
        <div class="box-footer">
            <button class="btn btn-success" type="submit">
                <i class="fa fa-aw fa-save"></i> Save Video
            </button>
        </div>

    </div>

    {!! Form::close()!!}
@stop

@section('scripts.footer')

    <script>
        $(function(){

            $(document).on('click', '.preview-link', function(e){

                e.preventDefault();
                console.log('i am testing');
                var video = $('#video-link').val();
                if(video == ''){
                    $('#video-link').closest('.form-group').addClass('error');
                    return false;
                }else if(video.indexOf('=') != -1){
                    var videoArr = video.split('=');
                    var videoCode = videoArr[1];

                    var html = '<iframe width="500" height="280" src="//www.youtube.com/embed/'+videoCode+'" frameborder="0" allowfullscreen></iframe>';
                    $('.video-preview').html(html);
                }else{
                    $('#video-link').closest('.form-group').addClass('error');
                    return false;
                }
            });
        })
    </script>

@stop



