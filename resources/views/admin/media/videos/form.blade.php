<div class="col-md-6">
    <div class="form-group">
        <label class="control-label">Name of Video</label>
        {!! Form::input('text', 'name', null, ['class' => 'form-control', 'placeholder'=>'Enter Title']) !!}
        <span id="helpBlock2" class="help-block error">{{ $errors->first('name') }}</span>
    </div>
    <div class="form-group">
        <label for="description">Description </label>
        {!! Form::textarea('description', null, ['class' => 'form-control', 'placeholder'=>'Enter Description', 'class'
        =>
        'ckeditor']) !!}
    </div>
</div>

<div class="col-md-6">
    <div class="form-group">
        <label for="">Video Link</label>
        {!! Form::input('text', 'link', null, ['class' => 'form-control', 'id' => 'video-link', 'placeholder'=>'Enter Link']) !!}
        <span id="helpBlock2" class="help-block error">{{ $errors->first('link') }}</span>
    </div>
    <div class="form-group">
        <button class="btn btn-warning preview-link" type="button">Preview</button>
    </div>
    <div class="form-group video-preview">

        @if( !empty($video) )
            <iframe width="500" height="280" src="//www.youtube.com/embed/{{ $video->vlink }}" frameborder="0"
                    allowfullscreen></iframe>
        @endif
    </div>
</div>