<div class="row">
    <div class="col-md-6">

        <div class="form-group">
            <label for="category">Name</label>
            {!! Form::input('text', 'name', null, ['class' => 'form-control', 'placeholder'=>'Enter Name Of Category']) !!}
            <span id="category" class="help-block error">{{ $errors->first('name') }} </span>
        </div>

    </div>
</div>