@extends('layouts.admin')

@section('content')

    {!! Form::open( array( 'url' => route("admin.video-categories.store") ) ) !!}

    <div class='box'>
        <div class='box-body'>

            @include('admin.media.video-categories.form')

        </div>

        <div class="box-footer">
            <button class="btn btn-success" type="submit" >
                <i class="fa fa-aw fa-save"></i> Save Category </button>
        </div>

    </div>

    {!! Form::close()!!}




@stop



