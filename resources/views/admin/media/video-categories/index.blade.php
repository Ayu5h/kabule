@extends('layouts.admin')

@section('content')
    <div class="box">
        <div class="box-body">
            <p>
                <a class="btn btn-success" href="{{ route('admin.video-categories.create') }}">
                    <i class="fa fa-aw fa-plus"></i>
                    Add Category</a>
            </p>
            <div class="divider"></div>
            <div class="row">
                <div class="col-sm-12 table-responsive">
                    <table class="table table-bordered table-striped datatable">
                        <thead>
                        <tr>
                            <th class="col-md-2">SN</th>
                            <th class="col-md-8">Category</th>
                            <th class="col-md-2">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($categories as $k => $category)
                            <tr>
                                <td>{{ ++$k }}</td>
                                <td>{{ $category->name }}</td>
                                <td>
                                    <div class="dropdown action-menu">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-aw fa-cog fa-2x"></i></a>
                                        <ul class="dropdown-menu pull-right">
                                            <li><a href="{{ route('admin.video-categories.{id}.videos.index', array($category->id)) }}" >Videos</a></li>
                                            <li><a href="{{ route('admin.video-categories.edit', array($category->id)) }}" >Edit</a></li>
                                            <li>
                                                {!! delete_form(route('admin.video-categories.destroy', [$category->id]), 'Delete') !!}
                                            </li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div>

@stop

@section('scripts.footer')

    <script>
        $(function(){
            $('.datatable').dataTable({
                "pageLength": 2
            });
        });
    </script>

@stop