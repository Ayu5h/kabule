@extends('layouts.admin')

@section('content')

{!! Form::open(array( 'url' => route("admin.impact.store"), 'class' => '' ) ) !!}

<div class='box'>
    <div class='box-body'>

        <div class="row">


                @include('admin.impact.form')


        </div>
    </div>
    <div class="box-footer">



        <button class="btn btn-success" type="submit" >
            <i class="fa fa-aw fa-save"></i> Save Our Impacts </button>
    </div>

</div>

{!! Form::close()!!}




@stop



