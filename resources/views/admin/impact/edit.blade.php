@extends('layouts.admin')

@section('content')

{!! Form::model( $impact, [ 'method' => 'PATCH', 'action' => [ 'ImpactController@update', $impact->id ] ] ) !!}

<div class='box'>
    <div class='box-body'>

        <div class="row">


           <div class="col-md-6">
                @include('admin.partials.image-upload')
            </div>

           <div class="col-md-6 uploaded-images">
                @include('admin.partials.image-selection', ['object' => $impact])
            </div>

           @include('admin.impact.form')


        </div>
    </div>
    <div class="box-footer">



        <button class="btn btn-success" type="submit" >
           <i class="fa fa-aw fa-save"></i> Save Our Impacts
        </button>

    </div>



</div>

{!! Form::close()!!}




@stop


@section('scripts.footer')


    @include('admin.partials.dropzone-script', [
        'url' => route('store_impact_photo', [$impact->id]),
        'uploadType' => 'multiple'
    ])


@stop
