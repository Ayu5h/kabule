@extends('layouts.admin')

@section('content')

    <div class="box">
        <div class="box-body">
            <p>
                <a class="btn btn-success" href="{{ route("admin.notice.create") }}">
                    <i class="fa fa-aw fa-plus"></i>
                    Add Notice </a>
            </p>

            <div class="divider"></div>
            <div class="table-responsive">

                <table class="table table-bordered table-striped datatable">
                    <thead>
                    <tr>
                        <th class="col-md-1">SN</th>
                        <th class="col-md-1">Title</th>
                        <th class="col-md-2">Attachment</th>
                        <th class="col-md-2">Description</th>
                        <th class="col-md-1">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($notices as $k => $notice)
                        <tr>
                            <td>{{ ++$k }}</td>
                            <td>{{ $notice->title }}</td>
                            <td>
                                @foreach($notice->attachments as $attachment)

                                    <a href="{{ route('media.download', [$attachment->id]) }}">{{ $attachment->name }} </a>

                                @endforeach
                            </td>
                            <td>{!! $notice->description !!}</td>
                            <td>
                                <div class="dropdown action-menu">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-aw fa-cog fa-2x"></i></a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="{{ route('admin.notice.edit', [$notice->id]) }}" >Edit</a></li>
                                        <li>{!! delete_form(route('admin.notice.destroy', [$notice->id]), 'Delete') !!}</li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

        </div>
        <!-- /.box-body -->


    </div>

@stop



@section('scripts.footer')

    <script>
        $(function(){
            $('.datatable').dataTable({
                "pageLength": 2
            });
        });
    </script>

@stop