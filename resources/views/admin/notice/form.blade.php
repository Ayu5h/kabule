<div class="col-md-6">
    <div class="form-group">
        <label for="title">Title</label>
        <input type="text" name="title" value="{{ old('title') ? old('title') : $notice->title }}" class="form-control" id="title"/>
        <span id="title" class="help-block error">{{ $errors->first('title') }} </span>
    </div>

    <div class="form-group">
        <label for="date">Date</label>
        <input type="date" name="date" value="{{ old('date') ? old('date') : $notice->date }}" class="form-control" id="date"/>
        <span id="date" class="help-block error">{{ $errors->first('date') }} </span>
    </div>

    <div class="form-group">
        <label for="file">Attachment</label>
        <input type="file" name="file" id="file" class="form-control"/>
        <span id="file" class="help-block error">{{ $errors->first('file') }} </span>

        @if( isset($notice) AND $notice->file_name )
            <span id="helpBlock2" class="help-block">
                    <i class="fa fa-file-pdf-o"></i> {{ $notice->file_name }}
                </span>
        @endif
    </div>
</div>

<div class="col-md-12">
    <div class="form-group">
        <label for="description">Description</label>
        <textarea name="description" id="description" cols="30" rows="5"
                  class="form-control ckeditor">{{ old('description') ? old('description') : $notice->description }}</textarea>
        <span id="description" class="help-block error">{{ $errors->first('description') }} </span>
    </div>
</div>