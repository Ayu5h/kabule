@extends('layouts.admin')


@section('content')

    <form action="{{ route('admin.notice.update', [$notice->id]) }}" method="POST" enctype="multipart/form-data">

        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
        <input type="hidden" name="_method" value="PATCH"/>

        <div class="box">
            <div class="box-body">

                <div class="row">


                        @include('admin.notice.form')


                </div>

            </div>

            <div class="box-footer">
                <button type="submit" class="btn btn-success">
                    <i class="fa fa-aw fa-save"></i> Save Notice
                </button>
            </div>
        </div>

    </form>

@stop