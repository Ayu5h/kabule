@extends('layouts.admin')


@section('content')

    <form action="{{ route('admin.notice.store') }}" method="POST" enctype="multipart/form-data">

        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>

        <div class="box">
            <div class="box-body">

                <div class="row">

                        @include('admin.notice.form')

                </div>

            </div>

            <div class="box-footer">
                <button type="submit" class="btn btn-success">
                    <i class="fa fa-aw fa-save"></i> Save Notice
                </button>
            </div>
        </div>

    </form>

@stop
