@extends('layouts.admin')

@section('content')

{!! Form::model( $activity, [ 'method' => 'PATCH', 'action' => [ 'ActivityController@update', $programId, $activity->id ] ] ) !!}

<div class='box'>
    <div class='box-body'>

        <div class="row">

           <div class="col-md-6">
                @include('admin.partials.image-upload')
            </div>

           <div class="col-md-6 uploaded-images">
                @include('admin.partials.image-selection', ['object' => $activity])
            </div>

           <div class="clearfix"></div>

           @include('admin.activity.form')

        </div>
    </div>
    <div class="box-footer">



        <button class="btn btn-success" type="submit" >
           <i class="fa fa-aw fa-save"></i> Save Activity
        </button>

    </div>


</div>

{!! Form::close()!!}




@stop



@section('scripts.footer')


    @include('admin.partials.dropzone-script', [
        'url' => route('activity.addPhoto', [$activity->id]),
        'uploadType' => 'multiple'
    ])

@stop

