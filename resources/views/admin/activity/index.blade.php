@extends('layouts.admin')

@section('content')

    <div class="box">
        <div class="box-body">
            <p>
                <a class="btn btn-success" href="{{ route("admin.project.{id}.activity.create",[$programId]) }}">
                    <i class="fa fa-aw fa-plus"></i>
                    Add Activity </a>
            </p>

            <div class="divider"></div>
            <div class="table-responsive">

                <table class="table table-bordered table-striped datatable">
                    <thead>
                    <tr>
                        <th class="col-md-1">SN</th>
                        <th class="col-md-2">Name</th>
                        <th class="col-md-2">Featured Image</th>
                        <th class="col-md-1">Date</th>
                        <th class="col-md-2">Short Intro</th>
                        <th class="col-md-3">Description</th>
                        <th class="col-md-1">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($activities as $k => $activity)
                        <tr>
                            <td>{{ ++$k }}</td>
                            <td>{{ $activity->name }}</td>
                            <td>
                                @if(!empty($activity->featured_image))
                                    <img src="/{{ $activity->featured_thumbnail }}" class="admin-thumbnail"/>
                                @else
                                    <img src="http://fakeimg.pl/100x100/?text=No Image">
                                @endif
                            </td>
                            <td> {{ $activity->date }}  </td>
                            <td> {{ $activity->short_intro }} </td>
                            <td>{!! substr($activity->description,0,200) !!}........</td>
                            <td>
                                <div class="dropdown action-menu">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i
                                                class="fa fa-aw fa-cog fa-2x"></i></a>
                                    <ul class="dropdown-menu pull-right">
                                        <li>
                                            <a href="{{ route('admin.project.{id}.activity.edit', [$programId, $activity->id]) }}">Edit</a>
                                        </li>
                                        <li>{!! delete_form(route('admin.project.{id}.activity.destroy', [$programId,
                                            $activity->id]), 'Delete') !!}
                                        </li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

        </div>
        <!-- /.box-body -->


    </div>

@stop



@section('scripts.footer')

    <script>
        $(function () {
            $('.datatable').dataTable({
                "pageLength": 2
            });
        });
    </script>

@stop