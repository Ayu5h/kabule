<div class="col-md-6">
    <div class="form-group">
        <label for="name">Name <i class="fa fa-aw fa-asterisk"></i></label>
        {!! Form::input('text', 'name', null, ['class' => 'form-control', 'placeholder'=>'Enter Name']) !!}
        <span id="helpBlock2" class="help-block error">{{ $errors->first('name') }} </span>
    </div>

    <div class="form-group">
        <label for="date">Date <i class="fa fa-aw fa-asterisk"></i></label>
        {!! Form::input('date', 'date', null, ['class' => 'form-control', 'placeholder'=>'Enter Date']) !!}
        <span id="helpBlock2" class="help-block error">{{ $errors->first('date') }} </span>
    </div>
</div>

<div class="col-md-6">
    <div class="form-group">
        <label for="short_intro">Short Intro <i class="fa fa-aw fa-asterisk"></i></label>
        {!! Form::textarea('short_intro', null, ['class' => 'form-control', 'placeholder'=>'Enter Short Intro',
        'rows'=>5]) !!}
        <span id="helpBlock2" class="help-block error">{{ $errors->first('short_intro') }} </span>
    </div>
</div>

<div class="col-md-12">
    <div class="form-group">
        <label for="title">Meta Title </label>
        {!! Form::input('text', 'meta_title', null, ['class' => 'form-control', 'placeholder'=>'Enter meta title for SEO'])
        !!}
    </div>

    <div class="form-group">
        <label for="title">Meta Description </label>
        {!! Form::input('text', 'meta_description', null, ['class' => 'form-control', 'placeholder'=>'Enter meta description forSEO']) !!}
    </div>
</div>

<div class="clearfix">
    <div class="col-md-12">
        <div class="form-group">
            <label for="description">Description <i class="fa fa-aw fa-asterisk"></i></label>
            {!! Form::textarea('description', null, ['class' => 'form-control', 'placeholder'=>'Enter Description',
            'class' => 'ckeditor']) !!}
            <span id="helpBlock2" class="help-block error">{{ $errors->first('description') }} </span>
        </div>
    </div>
</div>


