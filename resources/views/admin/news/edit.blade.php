@extends('layouts.admin')

@section('content')

{!! Form::model( $news, [ 'method' => 'PATCH', 'action' => [ 'NewsController@update', $programId, $news->id ] ] ) !!}

<div class='box'>

    <div class='box-body'>

        <div class="row">

            <div class="col-md-6">

                @include('admin.partials.image-upload')

            </div>

            <div class="col-md-6 uploaded-images">

                @include('admin.partials.image-selection', ['object' => $news])

            </div>

            @include('admin.news.form')

        </div>
    </div>

    <div class="box-footer">

        <button class="btn btn-success" type="submit" >
           <i class="fa fa-aw fa-save"></i> Save News
        </button>

    </div>

</div>

{!! Form::close()!!}




@stop


@section('scripts.footer')

    @include('admin.partials.dropzone-script', [
        'url' => route('news.addPhoto', [$news->id]),
        'uploadType' => 'multiple'
    ])


@stop
