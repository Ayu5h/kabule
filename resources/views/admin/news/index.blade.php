@extends('layouts.admin')

@section('content')

<div class="box">
    <div class="box-body">
        <p>
            <a class="btn btn-success" href="{{ route("admin.project.{id}.news.create",[$programId]) }}">
                <i class="fa fa-aw fa-plus"></i>
                Add Project News </a>
        </p>

        <div class="divider"></div>
        <div class="table-responsive">

            <table class="table table-bordered table-striped datatable">
                <thead>
                    <tr>
                        <th class="col-md-1">SN</th>
                        <th class="col-md-4">Name</th>
                        <th class="col-md-2">Featured Image</th>
                        <th class="col-md-4">Description</th>
                        <th class="col-md-1">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($allNews as $k => $news)
                    <tr>
                        <td>{{ ++$k }}</td>
                        <td>{{ $news->name }}</td>
                        <td>
                                @if(!empty($news->featured_image))
                                    <img src="{{ asset($news->featured_thumbnail) }}" class="admin-thumbnail"/>
                                @else
                                <img src="http://fakeimg.pl/100x100/?text=No Image">
                                @endif
                        </td>

                        <td>{!! substr($news->description,0,200) !!}........</td>
                        <td>
                            <div class="dropdown action-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-aw fa-cog fa-2x"></i></a>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="{{ route('admin.project.{id}.news.edit', [$programId, $news->id]) }}" >Edit</a></li>
                                    <li>{!! delete_form(route('admin.project.{id}.news.destroy', [$programId, $news->id]), 'Delete') !!}</li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

    </div>
    <!-- /.box-body -->


</div>

@stop



@section('scripts.footer')

    <script>
        $(function(){
            $('.datatable').dataTable({
                "pageLength": 2
            });
        });
    </script>

@stop