@extends('layouts.admin')

@section('content')

    {!! Form::model( $contactSettings, [ 'method' => 'PATCH', 'action' => [ 'ContactController@update' ] ] ) !!}

    <div class='box'>
        <div class='box-body'>

            <div class="row">

                <div class="col-md-6">

                    <div class="form-group">
                        <label for="address">Address <i class="fa fa-aw fa-asterisk"></i></label>
                        {!! Form::input('text', 'address', null, ['class' => 'form-control', 'placeholder'=>'Enter
                        Address']) !!}
                        <span id="helpBlock2" class="help-block error">{{ $errors->first('address') }}</span>
                    </div>

                    <div class="form-group">
                        <label for="phone">Phone<i class="fa fa-aw fa-asterisk"></i></label>
                        {!! Form::input('text', 'phone', null, ['class' => 'form-control', 'placeholder'=>'Enter
                        Phone']) !!}
                        <span id="helpBlock2" class="help-block error">{{ $errors->first('phone') }}</span>
                    </div>

                    {{--<div class="form-group">--}}
                        {{--<label for="hotline">Hotline<i class="fa fa-aw fa-asterisk"></i></label>--}}
                        {{--{!! Form::input('text', 'hotline', null, ['class' => 'form-control', 'placeholder'=>'Enter--}}
                        {{--Hotline']) !!}--}}
                        {{--<span id="helpBlock2" class="help-block error">{{ $errors->first('hotline') }}</span>--}}
                    {{--</div>--}}

                    <div class="form-group">
                        <label for="email">Email<i class="fa fa-aw fa-asterisk"></i></label>
                        {!! Form::input('text', 'email', null, ['class' => 'form-control', 'placeholder'=>'Enter
                        Email']) !!}
                        <span id="helpBlock2" class="help-block error">{{ $errors->first('email') }}</span>
                    </div>

                    {{--<div class="form-group">--}}
                        {{--<label for="fax_number">Fax Number<i class="fa fa-aw fa-asterisk"></i></label>--}}
                        {{--{!! Form::input('text', 'fax_number', null, ['class' => 'form-control', 'placeholder'=>'Enter--}}
                        {{--Fax Number']) !!}--}}
                        {{--<span id="helpBlock2" class="help-block error">{{ $errors->first('fax_number') }}</span>--}}
                    {{--</div>--}}

                    {{--<div class="form-group">--}}
                        {{--<label for="skype_handle">Skype Handle<i class="fa fa-aw fa-asterisk"></i></label>--}}
                        {{--{!! Form::input('text', 'skype_handle', null, ['class' => 'form-control', 'placeholder'=>'Enter--}}
                        {{--Skype Handle']) !!}--}}
                        {{--<span id="helpBlock2" class="help-block error">{{ $errors->first('skype_handle') }}</span>--}}
                    {{--</div>--}}

                    <div class="form-group">
                        <label for="description">Description<i class="fa fa-aw fa-asterisk"></i></label>
                        {!! Form::textarea('description', null, ['class' => 'form-control', 'placeholder'=>'Enter
                        Description']) !!}
                        <span id="helpBlock2" class="help-block error">{{ $errors->first('description') }}</span>
                    </div>

                </div>

                {{--<div class="col-md-6">--}}
                    {{--<div class="form-group view-map-canvas">--}}

                        {{--{!! Form::input('text', 'location', null, ['class' => 'controls search-address',--}}
                        {{--'placeholder'=>'Enter Location', 'id' => 'search-address'])--}}
                        {{--!!}--}}

                        {{--<div id="create_map_canvas" style="height: 400px"></div>--}}
                        {{--{!! Form::input('hidden', 'lat', '27.6915196', ['id' => 'lat']) !!}--}}
                        {{--{!! Form::input('hidden', 'lng', '85.3420486', ['id' => 'lng']) !!}--}}


                    {{--</div>--}}
                    {{--<span id="helpBlock2" class="help-block error">{{ $errors->first('location') }} </span>--}}


                    {{--<div class="col-md-7">--}}
                        {{--@include('admin.partials.image-upload', ['name' => 'Office Image'])--}}
                    {{--</div>--}}

                    {{--@if($contactSettings->photos()->count())--}}
                        {{--<div class="col-md-5">--}}
                            {{--<img src="{{ display_image($contactSettings->photos()->first()->thumbnail_path, 250, 200) }}"--}}
                                 {{--alt=""/>--}}
                        {{--</div>--}}
                    {{--@endif--}}

                {{--</div>--}}

            </div>
        </div>
        <div class="box-footer">


            <button class="btn btn-success" type="submit">
                <i class="fa fa-aw fa-save"></i> Save Contact Settings
            </button>

        </div>


    </div>

    {!! Form::close()!!}




@stop


@section('scripts.footer')
    <script src="http://maps.google.com/maps/api/js?sensor=false&libraries=places"></script>

    <script>
        $(function () {
            searchMapInitialize('create_map_canvas', 'search-address', 12, '{{ $contactSettings->lat }}', '{{ $contactSettings->lng }}');
        });


    </script>

    @include('admin.partials.dropzone-script', [
                'url' => route('admin.contact.addPhoto', [$contactSettings->id]),
                'uploadType' => 'single'
            ])
@stop

