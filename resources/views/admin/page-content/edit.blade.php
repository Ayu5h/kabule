@extends('layouts.admin')

@section('content')

    {!! Form::model( $pageContent, [ 'method' => 'PATCH', 'url' => route('admin.page-contents.update', $pageContent->id ) ] ) !!}

    <div class='box'>
        <div class='box-body'>

            @if($pageContent->id == 4)
                <div class="row">
                    <div class="col-md-6">
                        @include('admin.partials.image-upload')
                    </div>
                    <div class="col-md-6">
                        @if($pageContent->photos()->count() > 0)
                            <img src="{{ asset($pageContent->photos()->first()->thumbnail_path) }}" alt=""/>
                        @endif
                    </div>
                </div>
            @endif
            <div class="row">

                @include('admin.page-content.form')

            </div>
        </div>
        <div class="box-footer">
            <button class="btn btn-success" type="submit">
                <i class="fa fa-aw fa-save"></i> Edit Page
            </button>
        </div>


    </div>

    {!! Form::close()!!}
@stop


@section('scripts.footer')

    @include('admin.partials.dropzone-script', [
        'url' => route('pageContent.addPhoto', [$pageContent->id]),
        'uploadType' => 'single',
        'stopReload' => 'true'
    ])

@stop
