<div class="col-md-6">

    <div class="form-group">
        <label for="title">Title <i class="fa fa-aw fa-asterisk"></i></label>
        {!! Form::input('text', 'title', null, ['class' => 'form-control', 'placeholder'=>'Enter Title']) !!}
        <span id="title" class="help-block error">{{ $errors->first('title') }}</span>
    </div>

    <div class="form-group">
        <label for="description">Description <i class="fa fa-aw fa-asterisk"></i></label>
        {!! Form::textarea('description', null, ['class' => 'form-control', 'placeholder'=>'Enter Description']) !!}
        <span id="description" class="help-block error">{{ $errors->first('description') }}</span>
    </div>
</div>

