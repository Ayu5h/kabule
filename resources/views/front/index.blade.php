@extends('layouts.frontend.index')

@section('content')
    <!-- Slider -->
    <div class="slider-container">
        <div class="slider">
            <div class="flexslider">
                <ul class="slides">
                    <li>
                        <img src="{{ asset('frontend') }}/img/banner1.jpg">
                        <!-- <div class="flex-caption">
                            Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit
                            lobortis nisl ut aliquip ex ea commodo consequat.
                        </div> -->
                    </li>
                    <li>
                        <img src="{{ asset('frontend') }}/img/banner2.jpg">
                        <!-- <div class="flex-caption">
                            Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit
                            lobortis nisl ut aliquip ex ea commodo consequat.
                        </div> -->
                    </li>
                    <li>
                        <img src="{{ asset('frontend') }}/img/banner1.jpg">
                        <!-- <div class="flex-caption">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et.
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et.
                            Lorem ipsum dolor sit amet, consectetur.
                        </div> -->
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <!-- training courses -->

    <section class="topics">
        <div class="container">
            <div class="row training-topics">
                <h1 class="topicss">Training Topics</h1>

                <div class="col-md-4">
                    <ul>
                        <a href="#">
                            <li>Leadership</li>
                        </a>
                        <a href="#">
                            <li>Managenent</li>
                        </a>
                        <a href="#">
                            <li>Mindfulness</li>
                        </a>
                        <a href="#">
                            <li>Teambuilding</li>
                        </a>
                    </ul>
                </div>

                <div class="col-md-4">
                    <ul>
                        <a href="#">
                            <li>Stress Management</li>
                        </a>
                        <a href="#">
                            <li>Motivation</li>
                        </a>
                        <a href="#">
                            <li>Sales</li>
                        </a>
                        <a href="#">
                            <li>Organization Development</li>
                        </a>
                    </ul>
                </div>

                <div class="col-md-4">
                    <ul>
                        <a href="#">
                            <li>Retreat Facilitation</li>
                        </a>
                        <a href="#">
                            <li>Coaching & Counseling</li>
                        </a>
                        <a href="#">
                            <li>Motivation</li>
                        </a>
                        <a href="#">
                            <li>Sales</li>
                        </a>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <div class="gap"></div>

    <!-- Presentation -->
    <div class="presentation-container">
        <div class="container">
            <div class="row">
                <div class="col-md-8" style="background:#fff;">
                    <h2 class='home-titles' style="text-align: center;">Introduction</h2>

                    <div class="col-md-6">
                        <img src="{{ asset('frontend') }}/img/a.jpg" class="img-responsive introimg">
                    </div>
                    <div class="col-md-6 introtext">
                        <p>
                            I have had an opportunity to attend Kabule's training on Leadership and Teambuilding. It was
                            interesting and very unconventional training than those I have attended so far. It is so
                            profound and yet so simple. The warm-hearted nature and facilitation style of the trainers
                            added much value to the training. It led me to seek Kabule to facilitate retreat in
                            'Joyshop' of 'Women's Cooperative Society Ltd. (WCS)' I have had an opportunity to attend
                            Kabule's.
                        </p>
                        <a class="big-link-1" href="services.html">Read More</a>
                    </div>
                </div>
                <div class="col-md-4">
                    <section style="background: #fff">
                        <h2 class="home-titles" style="text-align: center;">News and Events</h2>
                        <ul class="notice">
                            <table class="table table-condensed">
                                <thead>
                                <tr>
                                    <th>Title</th>
                                    <th>Tag</th>
                                    <th>Date</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>Management</td>
                                    <td><a href="#">News</a></td>
                                    <td>2014/01/8</td>
                                </tr>
                                <tr>
                                    <td>NLP Programs</td>
                                    <td><a href="#">Notice</a></td>
                                    <td>2014/01/8</td>
                                </tr>
                                <tr>
                                    <td>Management</td>
                                    <td><a href="#">News</a></td>
                                    <td>2014/01/8</td>
                                </tr>
                                <tr>
                                    <td>NLP Programs</td>
                                    <td><a href="#">Notice</a></td>
                                    <td>2014/01/8</td>
                                </tr>
                                <tr>
                                    <td>Management</td>
                                    <td><a href="#">News</a></td>
                                    <td>2014/01/8</td>
                                </tr>
                                <tr>
                                    <td>NLP Programs</td>
                                    <td><a href="#">Notice</a></td>
                                    <td>2014/01/8</td>
                                </tr>

                                </tbody>
                            </table>
                            <a class="big-link-1" href="services.html">Read More</a>

                        </ul>
                    </section>
                </div>
            </div>
        </div>

    </div>

    </div>


    <div class="gap"></div>

    <!-- Services -->
    <section class="trainingcourse">
        <div class="services-container">
            <div class="container">
                <h2 class='home-titles'>Trainers and Courses</h2>

                <div class="row">

                    <div class="col-sm-4">
                        <div class="service wow fadeInUp">
                            <div class="service-icon"><img src="{{ asset('frontend') }}/img/a.jpg"></div>
                            <h3>Leadership, Facilitation, Organizational Value</h3>
                            <em>- Kiran Gulrajani </em>

                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                                incididunt ut labore et...</p>
                            <a class="big-link-1" href="services.html">Read more</a>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="service wow fadeInUp">
                            <div class="service-icon"><img src="{{ asset('frontend') }}/img/a.jpg"></div>
                            <h3>Mindfulness<br>lorem</h3>
                            <em>- Nithya Shanti </em>

                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                                incididunt ut labore et...</p>
                            <a class="big-link-1" href="services.html">Read more</a>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="service wow fadeInUp">
                            <div class="service-icon"><img src="{{ asset('frontend') }}/img/a.jpg"></div>
                            <h3>Management, Sales<br>lorem</h3>
                            <em>- Deepak Sawhney </em>

                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                                incididunt ut labore et...</p>
                            <a class="big-link-1" href="services.html">Read more</a>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="service wow fadeInUp">
                            <div class="service-icon"><img src="{{ asset('frontend') }}/img/a.jpg"></div>
                            <h3>Leadership</h3>
                            <em>- Amit Shrestha </em>

                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                                incididunt ut labore et...</p>
                            <a class="big-link-1" href="services.html">Read more</a>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="service wow fadeInUp">
                            <div class="service-icon"><img src="{{ asset('frontend') }}/img/a.jpg"></div>
                            <h3>Teambuilding, Motivation, Fire Walking</h3>
                            <em>- Vikram Badhwar</em>

                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                                incididunt ut labore et...</p>
                            <a class="big-link-1" href="services.html">Read more</a>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="service wow fadeInUp">
                            <div class="service-icon"><img src="{{ asset('frontend') }}/img/a.jpg"></div>
                            <h3>Motivation </h3>
                            <em>- Balasuriya </em>

                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                                incididunt ut labore et...</p>
                            <a class="big-link-1" href="services.html">Read more</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        </div>
        </div>
        </div>
    </section>
@stop