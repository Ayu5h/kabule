<!-- Login form -->
<div class="login-popup popUpBlock">
    <div class="popup"><a class="close" href="#">×</a>

        <div class="content">
            <div class="title">Authorization</div>
            <div class="form">
                <form name="login_form" method="post">
                    <div class="col1">
                        <label for="log">Login</label>

                        <div class="field">
                            <input type="text" id="log" name="log">
                        </div>
                    </div>
                    <div class="col2">
                        <label for="pwd">Password</label>

                        <div class="field">
                            <input type="password" id="pwd" name="pwd">
                        </div>
                    </div>
                    <div class="extra-col">
                        <ul>
                            <li><a class="register-redirect" href="#">Registration</a></li>
                        </ul>
                    </div>
                    <div class="column button">
                        <input type="hidden" value="" name="redirect_to">
                        <a class="enter" href="#"><span>Login</span></a>

                        <div class="remember">
                            <input type="checkbox" value="forever" id="rememberme" name="rememberme">
                            <label for="rememberme">Remember me</label>
                        </div>
                    </div>
                    <div class="soc-login">
                        <div class="section-title">Enter with social networking</div>
                        <div class="section-subtitle">Unde omnis iste natus error sit voluptatem.</div>
                        <ul class="soc-login-links">
                            <li class="tw"><a href="#"><em></em><span>With Twitter</span></a></li>
                            <li class="fb"><a href="#"><em></em><span>Connect</span></a></li>
                            <li class="gp"><a href="#"><em></em><span>With Google +</span></a></li>
                        </ul>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /Login form -->