<!-- Registration form -->
<div class="registration-popup popUpBlock">
    <div class="popup"><a class="close" href="#">×</a>

        <div class="content">
            <div class="title">Registration</div>
            <div class="form">
                <form name="registration_form" method="post">
                    <div class="col1">
                        <div class="field">
                            <div class="label-wrap">
                                <label class="required" for="registration_form_username">Name</label>
                            </div>
                            <div class="input-wrap">
                                <input type="text" id="registration_form_username" name="registration_form_username">
                            </div>
                        </div>
                    </div>
                    <div class="col2">
                        <div class="field">
                            <div class="label-wrap">
                                <label class="required" for="registration_form_email">Email</label>
                            </div>
                            <div class="input-wrap">
                                <input type="text" id="registration_form_email" name="registration_form_email">
                            </div>
                        </div>
                    </div>
                    <div class="col1">
                        <div class="field">
                            <div class="label-wrap">
                                <label class="required" for="registration_form_pwd1">Password</label>
                            </div>
                            <div class="input-wrap">
                                <input type="password" id="registration_form_pwd1" name="registration_form_pwd1">
                            </div>
                        </div>
                    </div>
                    <div class="col2">
                        <div class="field">
                            <div class="label-wrap">
                                <label class="required" for="registration_form_pwd2">Confirm Password</label>
                            </div>
                            <div class="input-wrap">
                                <input type="password" id="registration_form_pwd2" name="registration_form_pwd2">
                            </div>
                        </div>
                    </div>
                    <div class="extra-col">
                        <ul>
                            <li><a class="autorization-redirect" href="#">Autorization</a></li>
                        </ul>
                    </div>
                    <div class="column button"><a class="enter" href="#"><span>Register</span></a>

                        <div class="notice">* All fields required</div>
                    </div>
                    <div class="result sc_infobox"></div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /Registration form -->