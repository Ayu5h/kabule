<footer role="contentinfo" class="site-footer" id="footer">
    <section class="ft_section_1">
        <div class="footer-wrapper">
            <div class="col1">
                <div class="block_footer_widgets" style="margin-right: 20px">
                    <div class="column">
                        <div class="widget_popular_footer">
                            <div class="widget_header">
                                <h3>Useful Links</h3>
                            </div>
                            <div class="widget_body">
                                <ul class="useful-links">
                                    <li>
                                        <a href="">
                                            प्रधानमन्त्री तथा मन्त्रिपरिषद्को कार्यालय
                                        </a>
                                    </li>
                                    <li>
                                        <a href="">
                                            नेपाल सरकारको आधिकारिक पोर्टल
                                        </a>
                                    </li>
                                    <li>
                                        <a href="">
                                            कानून, न्याय तथा संसदीय मामिला मन्त्रालय
                                        </a>
                                    </li>
                                    <li>
                                        <a href="">
                                            प्रधानमन्त्री तथा मन्त्रिपरिषद्को कार्यालय
                                        </a>
                                    </li>
                                    <li>
                                        <a href="">
                                            नेपाल सरकारको आधिकारिक पोर्टल
                                        </a>
                                    </li>
                                    <li>
                                        <a href="">
                                            कानून, न्याय तथा संसदीय मामिला मन्त्रालय
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col2">
                <div class="block_footer_widgets">
                    <div class="column" style="width: 460px;">
                        <div class="widget_header">
                            <h3>DOID Nepal</h3>
                        </div>
                        <div id="map-location" style="height: 160px; width: 430px"></div>
                    </div>
                    <div class="column last">
                        <h3>Contact Information</h3>

                        <div class="block_flickr_footer">
                            Address : {{ $contact->address }} <br/>
                            Phone : {{ $contact->phone }} <br/>
                            Fax : {{ $contact->fax_number }} <br/>
                            Email : {{ $contact->email }} <br/>
                            {!! $contact->description !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="ft_section_2">
        <div class="footer-wrapper">
            <ul id="footer_menu">
                <li><a href="{{ route('home') }}">Home</a></li>
                <li><a href="{{ route('aboutUs') }}">About Us</a></li>
                <li><a href="{{ url('contact-us') }}">Contact Us</a></li>
            </ul>
            <div class="copyright">
                <div class="footer_text">&copy; 2013. All Rights Reserved. Developed by <a href="http://kalpacreatives.com" style="color: #fff9c0;">Kalpacreatives</a></div>
            </div>
        </div>
    </section>
</footer>

@section('scripts.footer')
    <script>
        jQuery(function () {
            googlemap_init(jQuery("#map-location").get(0), "{{ $contact->lat }}", "{{ $contact->lng }}", "{{ $contact->location }}");
        });
    </script>
@stop