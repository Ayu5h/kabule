<!-- header begin -->
<header role="banner" class="site-header" id="header">
    <div>
        <section class="top">
            <div class="inner clearboth">
                <div class="top-left">
                    <div id="jclock1" class="simpleclock"></div>
                </div>
            </div>
        </section>
        <section class="section3">
            <div class="section-wrap clearboth">
                <div class="name-and-slogan">
                    <h1 class="site-title"><a rel="home" title="DOID" href="/">
                            <img alt="logo" src="{{ asset('front/images/logo.png') }}"> </a>
                    </h1>

                    <div style="float: left; margin-top: 10px">
                        <h2>Government of Nepal</h2>

                        <h3>Ministry of Agriculture Development</h3>

                        <h3>Directorate of Industrial Entomology Development (DOIED)</h3>
                    </div>
                </div>
                <div class="banner-block">
                    <img src="{{ asset('front/images/nepal.gif') }}" alt="" height="100px"/>
                </div>
            </div>
        </section>
        <div class="headStyleMenu">
            <section class="section-nav">
                <nav role="navigation" class="navigation-main">
                    <ul class="clearboth mainHeaderMenu">
                        <li class="home"><a href="{{ url('/') }}"></a></li>

                        <li class="red"><a href="{{ url('/') }}">Home</a></li>

                        <li class="blue">
                            <a href="javascript:;">About Us</a>
                            <ul>
                                @foreach( $pages as $page )
                                    <li><a href="{{ route('aboutUs', $page->slug) }}">{{ $page->name }}</a></li>
                                @endforeach

                                {{--@foreach( $committees as $committee )--}}
                                    {{--<li><a href="{{ route('committeeDetail', $committee->slug) }}">{{ $committee->name }}</a></li>--}}
                                {{--@endforeach--}}
                            </ul>
                        </li>

                        <li class="yellow"><a href="{{ url('notices') }}">Notices</a></li>

                        <li class="sky-blue"><a href="{{ url('programs') }}">Programs</a></li>

                        <li class="purple">
                            <a href="javascript:;">Publications</a>
                            <ul>
                                @foreach($publications as $publication)
                                    <li>
                                        <a href="{{ url("publications/".$publication->slug) }}">{{ $publication->category }}</a>
                                    </li>
                                @endforeach
                            </ul>
                        </li>

                        <li class="green">
                            <a href="javascript:;">News & Events</a>
                            <ul>
                                <li><a href="{{ url('news') }}">News</a></li>
                                <li><a href="{{ url('events') }}">Events</a></li>
                            </ul>
                        </li>

                        <li class="orange">
                            <a href="javascript:;">Gallery</a>
                            <ul>
                                <li><a href="{{ route('videos') }}">All Videos</a></li>
                                <li><a href="{{ route('albums') }}">All Albums</a></li>
                            </ul>
                        </li>

                        <li class="gray"><a href="{{ url('contact') }}">Contact Us</a></li>

                        <li class="yellow"><a href="{{ url('links') }}">Links</a></li>

                    </ul>
                </nav>
            </section>
            <!-- /#site-navigation -->
            <!-- mobile menu -->
            <section class="section-navMobile">
                <div class="mobileMenuSelect"><span class="icon"></span>Home</div>
                <ul class="clearboth mobileHeaderMenuDrop">

                    <li class="red"><a href="{{ url('/') }}">Home</a></li>

                    <li class="blue">
                        <a href="javascript:;">About Us</a>
                        <ul>
                            @foreach( $pages as $page )
                                <li><a href="{{ route('aboutUs', $page->slug) }}">{{ $page->name }}</a></li>
                            @endforeach

                            @foreach( $committees as $committee )
                                <li><a href="{{ route('committeeDetail', $committee->slug) }}">{{ $committee->name }}</a></li>
                            @endforeach
                        </ul>
                    </li>

                    <li class="yellow"><a href="{{ url('notices') }}">Notices</a></li>

                    <li class="sky-blue"><a href="{{ url('programs') }}">Programs</a></li>

                    <li class="purple">
                        <a href="javascript:;">Publications</a>
                        <ul>
                            @foreach($publications as $publication)
                                <li>
                                    <a href="{{ url("publications/".$publication->slug) }}">{{ $publication->category }}</a>
                                </li>
                            @endforeach
                        </ul>
                    </li>

                    <li class="green">
                        <a href="javascript:;">News & Events</a>
                        <ul>
                            <li><a href="{{ url('news') }}">News</a></li>
                            <li><a href="{{ url('events') }}">Events</a></li>
                        </ul>
                    </li>

                    <li class="orange">
                        <a href="javascript:;">Gallery</a>
                        <ul>
                            <li><a href="{{ route('videos') }}">All Videos</a></li>
                            <li><a href="{{ route('albums') }}">All Albums</a></li>
                        </ul>
                    </li>

                    <li class="gray"><a href="{{ url('contact') }}">Contact Us</a></li>

                    <li class="yellow"><a href="{{ url('links') }}">Links</a></li>
                </ul>
            </section>
            <!-- /mobile menu -->
        </div>
        <section class="news-ticker">
            <!-- Recent News slider -->
            <div id="flexslider-news" class="header_news_ticker">
                <ul class="news slides">
                    @foreach($notices as $notice)
                        <li><a href="{{ url("notices/$notice->id") }}">{{ $notice->title }}</a></li>
                    @endforeach
                </ul>
            </div>
            <!-- /Recent News slider -->
        </section>
    </div>
</header>
<!-- / header  -->