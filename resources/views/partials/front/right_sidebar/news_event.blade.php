<aside class="widget widget_news_combine" id="news-combine-widget-2">
    <div class="widget_header">
        {{--<div class="widget_subtitle"><a class="lnk_all_news" href="#">All Publications</a></div>--}}
        <h3 class="widget_title"><i class="fa fa-newspaper-o"></i> News & Events</h3>
    </div>
    <div class="widget_body">
        <div class="block_news_tabs" id="tabs">
            <div class="tabs">
                <ul>

                    <li><a href="#tabs1"><span>News</span></a></li>

                    <li><a href="#tabs2"><span>Events</span></a></li>

                </ul>
            </div>

            <!-- News tab -->
            <div class="tab_content" id="tabs1">

                @if($allNews->count() > 0)
                    @foreach($allNews->take(6) as $news)
                        <div class="block_home_news_post">

                            <div class="info">
                                <div class="date">{{ \Carbon\Carbon::parse($news->date)->format('M j') }}</div>
                            </div>

                            <p class="title">
                                <a href="{{ url('news/'.$news->slug) }}">{{ $news->name }}</a>
                            </p>
                        </div>
                    @endforeach
                @else
                    <div class="block_home_news_post">
                        <p class="title">News not available at the moment!</p>
                    </div>
                @endif

                @if($allNews->count() > 6)
                    <div class="block_home_news_post">
                        <p class="title"><a href="{{ url('news') }}">View All News</a></p>
                    </div>
                @endif

            </div>

            <!-- Event tab -->
            <div class="tab_content" id="tabs2">

                @if($events->count() > 0)
                    @foreach($events as $event)
                        <div class="block_home_news_post">
                            <div class="info">
                                <div class="date">{{ \Carbon\Carbon::parse($event->start_date)->format('M j') }}</div>
                            </div>
                            <p class="title">
                                <a href="{{ url('events/'.$event->slug) }}">{{ $event->title }}</a>
                            </p>
                        </div>
                    @endforeach
                @else
                    <div class="block_home_news_post">
                        <p class="title">Event not available at the moment!</p>
                    </div>
                @endif

                @if($allNews->count() > 6)
                    <div class="block_home_news_post">
                        <p class="title"><a href="{{ url('events') }}">View All Events</a></p>
                    </div>
                @endif

            </div>

        </div>
    </div>
</aside>