<aside class="widget widget_news_combine" id="news-combine-widget-2">
    <div class="widget_header">
        {{--<div class="widget_subtitle"><a class="lnk_all_news" href="#">All Publications</a></div>--}}
        <h3 class="widget_title"><i class="fa fa-book"></i> Recent Publications</h3>
    </div>
    <div class="widget_body">
        <div class="block_news_tabs" id="tabs">
            <div class="tabs">
                <ul>
                    @foreach($publications->take(3) as $k => $publication)
                        <li><a href="#tabs{{ $k+1 }}"><span>{{ $publication->category }}</span></a></li>
                    @endforeach
                </ul>
            </div>

            @foreach($publications->take(3) as $k => $publication)

                <!-- tab content goes here -->
                <div class="tab_content" id="tabs{{ $k+1 }}">

                    @foreach($publication->subPublications as $subPublication)
                        <div class="block_home_news_post">
                            <div class="info">
                                <div class="date">{{ \Carbon\Carbon::parse($subPublication->date)->format('M j') }}</div>
                            </div>
                            <p class="title"><a href="{{ url('publications/'.$publication->slug.'/'.$subPublication->slug) }}">{{ $subPublication->title }}</a></p>
                        </div>
                    @endforeach

                </div>

            @endforeach


        </div>
    </div>
</aside>