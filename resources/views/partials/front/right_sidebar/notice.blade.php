<aside class="widget widget_recent_blogposts">
    <div class="widget_header">
        <div class="widget_subtitle"><a class="lnk_all_posts" href="{{ url('notices') }}">all notice</a></div>
        <h3 class="widget_title"><i class="fa fa-list"></i> Notice</h3>
    </div>
    <div class="widget_body">
        <ul class="slides">
            @if($notices->count() > 0)
                @foreach($notices->chunk(5) as $sets)
                    <li>
                        @foreach($sets as $notice)
                            <div class="article">

                                <div class="text">
                                    <span class="doa-info">{{ \Carbon\Carbon::parse($notice->date)->format('M j') }}</span>

                                    <p class="title" style="float: left">
                                        <a href="{{ url('notices/'.$notice->id) }}">
                                            {{--<span class="doa-new">New!</span>--}}

                                            {{ $notice->title }}
                                        </a>
                                    </p>

                                </div>
                            </div>
                        @endforeach
                    </li>
                @endforeach
            @else
                <div class="article">
                    <div class="text">
                        <p class="title">
                            Notice not available at the moment!
                        </p>
                    </div>
                </div>
            @endif
        </ul>
        @if( !empty($sets) )
            <div class="pages_info"><span class="cur_page">1</span> of <span
                        class="all_pages">{{ !empty($sets) ? count($sets) : '' }}</span></div>
        @endif
    </div>
</aside>
<!-- Recent posts -->