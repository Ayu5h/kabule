<!--Latest reviews-->
<aside class="widget widget_recent_reviews">
    <div class="widget_header">
        <div class="widget_subtitle"><a class="lnk_all_posts" href="reviews-all.html">All reviews</a></div>
        <h3 class="widget_title">Latest reviews</h3>
    </div>
    <div class="widget_body">
        <div class="recent_reviews">
            <ul class="slides">
                <li>
                    <div class="img_wrap"><a class="w_hover img_wrap" href="reviews-item-page.html"><img alt="" src="http://placehold.it/88x64"></a></div>
                    <div class="extra_wrap">
                        <div class="review-title"><a href="reviews-item-page.html">Tempor dolor nec lectus facilisis et consequat.</a></div>
                        <div class="post-info">
                            <div class="post_rating"><span class="points4"></span></div>
                            <a class="comments_count" href="reviews-item-page.html">1</a></div>
                    </div>
                </li>
                <li>
                    <div class="img_wrap"><a class="w_hover img_wrap" href="reviews-item-page.html"><img alt="" src="http://placehold.it/88x64"></a></div>
                    <div class="extra_wrap">
                        <div class="review-title"><a href="reviews-item-page.html">Lectus facilisis et consequat lectus malesuada.</a></div>
                        <div class="post-info">
                            <div class="post_rating"><span class="points2"></span></div>
                            <a class="comments_count" href="reviews-item-page.html">1</a></div>
                    </div>
                </li>
                <li>
                    <div class="img_wrap"><a class="w_hover img_wrap" href="reviews-item-page.html"><img alt="" src="http://placehold.it/88x64"></a></div>
                    <div class="extra_wrap">
                        <div class="review-title"><a href="reviews-item-page.html">Etiam sapien urna, mollis volutpat malesuada.</a></div>
                        <div class="post-info">
                            <div class="post_rating"><span class="points5"></span></div>
                            <a class="comments_count" href="reviews-item-page.html">1</a></div>
                    </div>
                </li>
                <li>
                    <div class="img_wrap"><a class="w_hover img_wrap" href="reviews-item-page.html"><img alt="" src="http://placehold.it/88x64"></a></div>
                    <div class="extra_wrap">
                        <div class="review-title"><a href="reviews-item-page.html">Nunc ut metus eu leo ornare sagittis non...</a></div>
                        <div class="post-info">
                            <div class="post_rating"><span class="points5"></span></div>
                            <a class="comments_count" href="reviews-item-page.html">1</a></div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</aside>