<!--Video Widget-->
@if( !empty($videos) && $videos->count() > 0 )
    <aside class="widget widget_recent_video">
        <div class="widget_header">
            <div class="widget_subtitle"><a class="lnk_all_posts" href="{{ route('videos') }}">All Videos</a></div>
            <h3 class="widget_title">Featured Videos</h3>
        </div>
        <div class="widget_body" style="min-height: auto !important;">
            <div id="video_carousel" class="thumb_carousel jcarousel-container jcarousel-container-vertical">
                <div class="jcarousel-container">
                    <div class="jcarousel-clip jcarousel-clip-vertical">
                        <ul class="jcarousel-list">
                            @foreach( $videos as $k => $video )
                                <?php if($k == 0) $firstVideo = $video; ?>
                                <li>
                                    <a data-content="#"
                                       data-href="{{ asset($video->photos()->first()->path) }}"
                                       title="{{ $video->name }}"
                                       href="http://www.youtube.com/watch?v={{ $video->vlink }}">
                                        <img alt="" src="{{ asset($video->photos()->first()->path) }}">
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="jcarousel-prev jcarousel-prev-vertical jcarousel-prev-disabled jcarousel-prev-disabled-vertical">
                    <span></span></div>
                <div class="jcarousel-next jcarousel-next-vertical"><span></span></div>
            </div>
            <div id="carousel_target" class="video-thumb">
                <a class="w_hover img-link img-wrap prettyPhoto"
                   href="http://www.youtube.com/watch?v={{ $firstVideo->vlink }}"><img
                            alt="" src="{{ asset($firstVideo->photos()->first()->path) }}"><span class="v_link"></span></a>

                <div class="post_title"><a class="post_name" href="post-youtube.html">{{ $firstVideo->name }}</a></div>
            </div>
        </div>
    </aside>
    <!--Video Widget-->
@endif