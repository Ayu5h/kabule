<aside class="widget widget_recent_photos">
    <div class="widget_header">
        <div class="widget_subtitle"><a class="lnk_all_posts" href="{{ route('committeeDetail', ['staff']) }}">All Staffs</a></div>
        <h3 class="widget_title"><i class="fa fa-user"></i> Staffs</h3>
    </div>
    <div class="widget_body" style="height: auto !important;">
        <div id="recent_photos_thumbs">
            <div class="flex-viewport">
                <ul class="slides">
                    @foreach( $members as $member )
                        <li><img alt="" src="{{ display_image($member->image, 80, 80) }}" style="width: 80px; height: 45px"></li>
                    @endforeach
                </ul>
            </div>
            <ul class="flex-direction-nav">
                <li><a href="#" class="flex-prev flex-disabled"><span></span></a></li>
                <li><a href="#" class="flex-next"><span></span></a></li>
            </ul>
        </div>
        <!--Recent photos-->
        <div id="recent_photos_slider">
            <div class="flex-viewport">
                <ul class="slides">
                    @foreach( $members as $member )
                        <li><a class="gal_link w_hover img-link prettyPhoto" style="height: 171px; width: 276px" href="{{ asset($member->image) }}"><img
                                        alt="" src="{{ asset($member->image_thumbnail) }}">
                                <span class="link-icon"></span> </a>

                            <div class="title">
                                <a href="{{ route('home') }}">{{ $member->name }}</a>
                                <br/>
                                {{ $member->designation }}
                            </div>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
</aside>