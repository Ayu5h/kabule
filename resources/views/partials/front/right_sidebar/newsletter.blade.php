<!-- newsletter -->
<aside class="widget feedburner_subscribe">
    <div class="block_newsletter">
        <div class="widget_header">
            <h3 class="widget_title">FeedBurner Widget</h3>
        </div>
        <div class="widget_body">
            <div class="label">Subscribe to our email newsletter.</div>
            <form target="_blank" method="post" action="http://feedburner.google.com/fb/a/mailverify?">
                <div class="field">
                    <input type="text" placeholder="Enter Your E-mail address" class="w_def_text" title="Enter Your Email Address" name="email">
                </div>
                <input type="submit" value="Subscribe" class="button">
            </form>
        </div>
    </div>
</aside>