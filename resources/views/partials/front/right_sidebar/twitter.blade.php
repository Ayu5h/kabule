<aside class="widget twitter">
    <div class="block_twitter_widget">
        <div class="widget_header blue">
            <div class="lnk_follow widget_subtitle"><a target="_blank" href="https://twitter.com/envato">Follow on Twitter</a></div>
            <h3 class="widget_title">Latest tweets</h3>
        </div>
        <div class="tweet widget_body">
            <div class="tweetBody">
                <ul>
                    <li><a href="#">@wpspacethemes</a> How can our bank can get the corrected Name of the Receiver for our account, since it has not been
                        specified correctly?
                    </li>
                    <li><a href="#">@wpspacethemes</a> Check out this great #themeforest item 'PrimeTime - Clean, Responsive WP Magazine</li>
                </ul>
            </div>
        </div>
    </div>
</aside>
