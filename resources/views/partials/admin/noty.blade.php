@if (Session::has('flash_notification.message'))
    <script>

        $(function () {

            generate("{{ Session::get('flash_notification.level') }}", "{{ Session::get('flash_notification.message') }}", "{{ session('flash_notification.important') }}");

        });

        function generate(title, text, important) {

            titleText = title;

            if (title == 'validation-error' || title == 'validation_error') {
                title = 'warning';
                titleText = 'Validation Error';
            }

            if (title == 'Invalid Name') {
                title = 'error';
                titleText = 'Invalid Name';
            }

            if (title == 'success') {
                faIcon = '<i class="fa fa-aw fa-check-circle-o"></i> ';
            } else if (title == 'error') {
                faIcon = '<i class="fa fa-aw fa-times-circle-o"></i> ';
            } else if (title == 'warning') {
                faIcon = '<i class="fa fa-aw fa-exclamation-circle"></i> ';
            } else {
                faIcon = '<i class="fa fa-aw fa-info-circle"></i> ';
            }

            var n = noty({
                text: faIcon + '<strong>' + titleText + ' !! </strong> <br />' + text,
                type: title,
                dismissQueue: true,
                layout: 'topRight',
                theme: 'defaultTheme',
                timeout: important ? 0 : 3000
            });

            return n;
        }

    </script>
@endif