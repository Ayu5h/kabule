<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <ul class="sidebar-menu">

            <li class="header">MAIN NAVIGATION</li>

            <li>
                <a href="{{ action('BannerController@index') }}">
                    <i class="fa fa-aw fa-picture-o"></i> <span>Banner</span>
                </a>
            </li>

            {{--<li class="treeview">--}}
                {{--<a href="#">--}}
                    {{--<i class="fa fa-university"></i> <span>Home</span>--}}
                    {{--<i class="fa fa-angle-left pull-right"></i>--}}
                {{--</a>--}}
                {{--<ul class="treeview-menu">--}}
                    {{--<li>--}}
                        {{--<a href="{{ route('admin.page-contents.edit', [4]) }}"><i class="fa fa-circle-o"></i> Welcome To--}}
                            {{--TLM</a>--}}
                    {{--</li>--}}
                {{--</ul>--}}
            {{--</li>--}}

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-university"></i> <span>About Us</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    @foreach( $aboutPages as $abPage )
                        <li>
                            <a href="{{ route('admin.pages.edit', [$abPage->slug]) }}"><i
                                        class="fa fa-circle-o"></i> {{ $abPage->name }} Page</a>
                        </li>
                    @endforeach
                    {{--<li>--}}
                        {{--<a href="{{ route('admin.committee.index')  }}"><i class="fa fa-circle-o"></i> Committees</a>--}}
                    {{--</li>--}}
                    <li>
                        <a href="{{ route('admin.pages.create')  }}"><i class="fa fa-circle-o"></i> Add Page</a>
                    </li>
                </ul>
            </li>

            {{--<li>--}}
                {{--<a href="{{ action('ProgramController@index') }}" id="project">--}}
                    {{--<i class="fa fa-suitcase"></i> <span>Project</span>--}}
                {{--</a>--}}
            {{--</li>--}}

            <li>
                <a href="{{ action('WhatWeDoController@index') }}" id="whatWeDo">
                    <i class="fa fa-suitcase"></i> <span>What We Do</span>
                </a>
            </li>

            <li>
                <a href="{{ action('DisasterController@index') }}" id="disaster">
                    <i class="fa fa-suitcase"></i> <span>Disaster Response</span>
                </a>
            </li>

            <li>
                <a href="{{ action('GetinvolveController@index') }}" id="getinvolve">
                    <i class="fa fa-suitcase"></i> <span>Get Involve</span>
                </a>
            </li>

            <li>
                <a href="{{ action('ImpactController@index') }}" id="impact">
                    <i class="fa fa-suitcase"></i> <span>Our Impacts</span>
                </a>
            </li>

            <li>
                <a href="{{ action('PartnerController@index') }}" id="partner">
                    <i class="fa fa-suitcase"></i> <span>Our Partners</span>
                </a>
            </li>

            {{--<li>--}}
                {{--<a href="{{ route('admin.notice.index') }}">--}}
                    {{--<i class="fa fa-comment-o"></i> <span>Notice</span>--}}
                {{--</a>--}}
            {{--</li>--}}

            <li class="treeview">
                <a href="{{ route('admin.publication.index') }}">
                    <i class="fa fa-book"></i> <span>Resources</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li>
                        <a href="{{ route('admin.publication.index') }}"><i class="fa fa-circle-o"></i>Manage
                            Resources</a>
                    </li>
                    @foreach($publications as $publication)
                        <li>
                            <a href="{{ route('admin.publication.show', [$publication->slug]) }}">
                                <i class="fa fa-circle-o"></i>
                                {{ $publication->category }}
                            </a>
                        </li>
                    @endforeach
                </ul>
            </li>

            <li class="treeview">
                <a href="{{ action('SiteNewsController@index') }}">
                    <i class="fa fa-newspaper-o"></i> <span>News</span>
                    <i class="fa"></i>
                </a>
            </li>

            {{--<li class="treeview">--}}
                {{--<a href="#">--}}
                    {{--<i class="fa fa-file-photo-o"></i> <span>Media</span>--}}
                    {{--<i class="fa fa-angle-left pull-right"></i>--}}
                {{--</a>--}}
                {{--<ul class="treeview-menu">--}}
                    {{--<li>--}}
                        {{--<a href="{{ route('admin.albums.index')  }}"><i class="fa fa-circle-o"></i> Albums</a>--}}
                    {{--</li>--}}
                    {{--<li>--}}
                        {{--<a href="{{ route('admin.video-categories.index')  }}"><i class="fa fa-circle-o"></i> Videos</a>--}}
                    {{--</li>--}}
                {{--</ul>--}}
            {{--</li>--}}

            <li class="treeview">

                <a href="#">
                    <i class="fa fa-life-ring"></i> <span>Contact</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ action('ContactController@edit') }}"><i class="fa fa-cogs"></i>Manage Settings</a>
                    </li>
                    {{--<li><a href="{{ action('ContactMessageController@index') }}"><i class="fa fa-envelope"></i>Manage--}}
                            {{--Messages</a></li>--}}
                </ul>
            </li>

            {{--<li>--}}
                {{--<a href="{{ action('LinkController@index') }}">--}}
                    {{--<i class="fa fa-anchor"></i> <span>Link</span>--}}
                {{--</a>--}}
            {{--</li>--}}


            @include('partials.admin.sidebar-junk')
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>