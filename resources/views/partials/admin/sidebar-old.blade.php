<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <ul class="sidebar-menu">

            <li class="header">MAIN NAVIGATION</li>

            <li>
                <a href="{{ action('BannerController@index') }}">
                    <i class="fa fa-aw fa-picture-o"></i> <span>Banner</span>
                </a>
            </li>

            <li>
                <a href="{{ action('PartnerController@index') }}">
                    <i class="fa fa-user"></i> <span>Partners</span>
                </a>
            </li>

            <li class="{{ (starts_with($currentUrl, 'admin/project')) ? 'active' : '' }}">
                <a href="{{ action('ProgramController@index') }}" id="project" >
                    <i class="fa fa-suitcase"></i> <span>Project</span>
                </a>
            </li>

            <li>
                <a href="{{ action('NewsletterController@index') }}">
                    <i class="fa fa-book"></i> <span>Newsletter</span>
                </a>
            </li>

            <li class="treeview">

                <a href="#">
                    <i class="fa fa-life-ring"></i> <span>Shop</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ action('PublicationController@index') }}"><i class="fa fa-cogs"></i>Publication</a></li>
                    <li><a href="{{ action('MerchandiseController@index') }}"><i class="fa fa-envelope"></i>Merchandise</a></li>
                </ul>
            </li>

            <li class="treeview">

                <a href="#">
                    <i class="fa fa-life-ring"></i> <span>Donation</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('donationProgram.index') }}"><i class="fa fa-cogs"></i>Donation Program</a></li>
                    <li><a href="{{ route('donationInquiry.index') }}"><i class="fa fa-envelope"></i>Donation Inquiry</a></li>
                </ul>
            </li>

            <li class="treeview">

                <a href="#">
                    <i class="fa fa-life-ring"></i> <span>Membership</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('admin.membership-category.index') }}"><i class="fa fa-cogs"></i>Membership Category</a></li>
                    <li><a href="{{ route('admin.membership-inquiry.index') }}"><i class="fa fa-envelope"></i>Membership Inquiry</a></li>
                    <li><a href="{{ route('membershipConfirmed') }}"><i class="fa fa-envelope"></i>Confirmed Membership</a></li>
                </ul>
            </li>

            <li class="treeview">

                <a href="#">
                    <i class="fa fa-life-ring"></i> <span>Contact</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ action('ContactController@edit') }}"><i class="fa fa-cogs"></i>Manage Settings</a></li>
                    <li><a href="{{ action('ContactMessageController@index') }}"><i class="fa fa-envelope"></i>Manage Messages</a></li>
                </ul>
            </li>

            <li>
                <a href="{{ action('SiteNewsController@index') }}">
                    <i class="fa fa-book"></i> <span>News</span>
                </a>
            </li>

            <li>
                <a href="{{ action('EventController@index') }}">
                    <i class="fa fa-book"></i> <span>Events</span>
                </a>
            </li>

            <li>
                <a href="{{ action('AdvertisementController@index') }}">
                    <i class="fa fa-book"></i> <span>Advertisement</span>
                </a>
            </li>

            <li>
                <a href="{{ action('LinkController@index') }}">
                    <i class="fa fa-book"></i> <span>Link</span>
                </a>
            </li>



            <li class="treeview {{ !empty($module) && $module == 'about' ? 'active' : '' }}">
                <a href="#">
                    <i class="fa fa-university"></i> <span>About Us</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li class="{{ !empty($page) && $page->slug == 'about-us' ? 'active' : '' }}">
                        <a href="{{ route('admin.pages.edit', ['about-us']) }}"><i class="fa fa-circle-o"></i> About Us Page</a>
                    </li>
                    <li class="{{ !empty($currentPage) && $currentPage == 'committees' ? 'active' : '' }}">
                        <a href="{{ route('admin.committee.index')  }}"><i class="fa fa-circle-o"></i> Committees</a>
                    </li>
                </ul>
            </li>

            <li class="treeview {{ !empty($module) && $module == 'cities' ? 'active' : '' }}">
                <a href="#">
                    <i class="fa fa-twitter"></i> <span>Birds</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li class="{{ !empty($page) && $page->slug == 'introduction-birds' ? 'active' : '' }}">
                        <a href="{{ route('admin.pages.edit', ['introduction-birds']) }}"><i class="fa fa-circle-o"></i> Introduction</a>
                    </li>
                    <li class="{{ !empty($page) && $page->slug == 'status-of-birds' ? 'active' : '' }}">
                        <a href="{{ route('admin.pages.edit', ['status-of-birds']) }}"><i class="fa fa-circle-o"></i> Status Of Birds</a>
                    </li>
                    <li class="{{ !empty($page) && $page->slug == 'important-birds-areas' ? 'active' : '' }}">
                        <a href="{{ route('admin.pages.edit', ['important-birds-areas']) }}"><i class="fa fa-circle-o"></i> Important Birds Area</a>
                    </li>
                    <li class="{{ !empty($currentPage) && $currentPage == 'cities-category' ? 'active' : '' }}">
                        <a href="{{ route('admin.cities-categories.index') }}"><i class="fa fa-circle-o"></i> CITIES Categories</a></li>
                    <li class="{{ !empty($currentPage) && $currentPage == 'birds' ? 'active' : '' }}">
                        <a href="{{ route('admin.birds.index') }}"><i class="fa fa-circle-o"></i> Important Birds</a>
                    </li>
                </ul>
            </li>

            @include('partials.admin.sidebar-junk')
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>