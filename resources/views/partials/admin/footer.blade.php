      <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <a href="http://www.kalpacreatives.com" target="_blank">Kalpa Creatives</a>
        </div>
        <strong>Copyright &copy; 2014-2015 <a href="http://kalpacreatives.com.com">Kalpacreatives Pvt. Ltd.</a>.</strong> All rights reserved.
      </footer>