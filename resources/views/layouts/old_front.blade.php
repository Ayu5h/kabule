<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en">
<![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8" lang="en">
<![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en"> <!--<![endif]-->
<head>
    <title>Bird Conservation Nepal</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Icare - Nonprofit, Fundraising HTML Template">
    <meta name="author" content="Esmeth">
    <meta charset="UTF-8">

    <!-- CSS Bootstrap & Custom -->
    <link href="{{ asset('front/css/bootstrap.min.css') }}" rel="stylesheet" media="screen">
    <link href="{{ asset('front/css/font-awesome.min.css') }}" rel="stylesheet" media="screen">
    <link href="{{ asset('front/css/animate.css') }}" rel="stylesheet" media="screen">
    <link href="{{ asset('front/style.css') }}" rel="stylesheet" media="screen">

    <!-- Favicons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
    <link rel="shortcut icon" href="images/ico/favicon.ico">

    <!-- JavaScripts -->
    <script src="{{ asset('front/js/jquery-1.10.2.min.js') }}"></script>
    <script src="{{ asset('front/js/min/modernizr.min.js') }}"></script>
    <!--[if lt IE 8]>
    <div style=' clear: both; text-align:center; position: relative;'>
        <a href="http://www.microsoft.com/windows/internet-explorer/default.aspx?ocid=ie6_countdown_bannercode"><img
                src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0"
                alt=""/></a>
    </div>
    <![endif]-->
</head>
<body>


@include('partials.front.responsive-menu')

@include('partials.front.site-header')


<div class="container">

    @yield('content')

</div>
<!-- /.container -->


<footer class="site-footer">
    <div class="top-footer">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-4 col-xs-12 contact-info footer-widget">
                    <h4 class="footer-widget-title">Contact Info</h4>

                    <p>Curabitur a orci et nulla gravida.</p>
                    <ul>
                        <li><span>Phone:</span><a href="skype:+34234235322?action">(123) 2445 - 2443</a></li>
                        <li><span>Email:</span><a href="mailto:your@supportemail.com">your@supportemail.com</a></li>
                    </ul>
                </div>
                <!-- /.col-md-3 -->
                <div class="col-md-5 col-sm-8 col-xs-12 footer-widget">
                    <h4 class="footer-widget-title">Stay in touch with us</h4>
                    <ul class="footer-social">
                        <li><a href="#" data-toggle="tooltip" title="Facebook" class="fa fa-facebook"></a></li>
                        <li><a href="#" data-toggle="tooltip" title="Twitter" class="fa fa-twitter"></a></li>
                        <li><a href="#" data-toggle="tooltip" title="Linkedin" class="fa fa-linkedin"></a></li>
                        <li><a href="#" data-toggle="tooltip" title="Flickr" class="fa fa-flickr"></a></li>
                        <li><a href="#" data-toggle="tooltip" title="Youtube" class="fa fa-youtube"></a></li>
                        <li><a href="#" data-toggle="tooltip" title="RSS" class="fa fa-rss"></a></li>
                    </ul>
                </div>
                <!-- /.col-md-5 -->
                <div class="col-md-4 col-sm-12 col-xs-12 footer-widget">
                    <h4 class="footer-widget-title">More about US</h4>

                    <p>Curabitur a orci et nulla gravida laoreet. Pellentesque dictum ipsum nec placerat dignissim.
                        Vivamus porta pellentesque libero, sit amet non.</p>
                </div>
                <!-- /.col-md-4 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->
    </div>
    <!-- /.top-footer -->
    <div class="main-footer">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-6 footer-widget">

                    <p>BCN is a registered environmental NGO with the Government of Nepal (77/049/050). All the
                        donations
                        are tax deductible. <br/>
                        All rights reserved. Contents from this site may not be reproduced in any form without prior
                        permission from Bird Conservation Nepal.</p>
                </div>
                <!-- /.col-md-3 -->
                <div class="col-md-3 col-sm-6 footer-widget">
                    <h4 class="footer-widget-title">Popular Posts</h4>
                    <ul>
                        <li><a href="blog-single.html">The Importance of an MOU</a></li>
                        <li><a href="blog-single.html">Detecting and learning from failure</a></li>
                        <li><a href="blog-single.html">Limited Support on Memorial Day</a></li>
                        <li><a href="blog-single.html">Fundraising Reading Round-Up</a></li>
                        <li><a href="blog-single.html">Ugly Design or Beautiful Design?</a></li>
                    </ul>
                </div>
                <!-- /.col-md-3 -->
                <div class="col-md-2 col-sm-6 footer-widget">
                    <h4 class="footer-widget-title">Useful Links</h4>
                    <ul>
                        <li><a href="staff-sidebar.html">Meet the Team</a></li>
                        <li><a href="blog-grid.html">Read Our Blog</a></li>
                        <li><a href="#">Partnerships</a></li>
                        <li><a href="#">Developer Tools</a></li>
                    </ul>
                </div>
                <!-- /.col-md-2 -->
                <div class="col-md-4 col-sm-6 footer-widget">
                    <h4 class="footer-widget-title">Our Gallery</h4>

                    <div class="footer-gallery">
                        <div class="gallery-thumb">
                            <a href="images/gallery-placeholder.png" class="fancybox" data-fancybox-group="group1">
                                <img src="http://placehold.it/80x80" alt="">
                            </a>
                        </div>
                        <!-- /.gallery-thumb -->
                        <div class="gallery-thumb">
                            <a href="images/gallery-placeholder.png" class="fancybox" data-fancybox-group="group1">
                                <img src="http://placehold.it/80x80" alt="">
                            </a>
                        </div>
                        <!-- /.gallery-thumb -->
                        <div class="gallery-thumb">
                            <a href="images/gallery-placeholder.png" class="fancybox" data-fancybox-group="group1">
                                <img src="http://placehold.it/80x80" alt="">
                            </a>
                        </div>
                        <!-- /.gallery-thumb -->
                        <div class="gallery-thumb">
                            <a href="images/gallery-placeholder.png" class="fancybox" data-fancybox-group="group1">
                                <img src="http://placehold.it/80x80" alt="">
                            </a>
                        </div>
                        <!-- /.gallery-thumb -->
                    </div>
                    <!-- /.footer-gallery -->
                </div>
                <!-- /.col-md-4 -->
            </div>
            <!-- /.row -->
            <div class="copyright">


                <div class="row">
                    <div class="col-md-9 col-sm-9">
                        <p class="small-text">Copyright 2011 &copy; Bird Conservation Nepal.</p>
                    </div>
                    <!-- /.col-md-6 -->
                    <div class="col-md-3 col-sm-3">
                        <div class="credits">
                            <img src="http://hitwebcounter.com/counter/counter.php?page=5241267&style=0006&nbdigits=7&type=page&initCount=0"
                                 title="nice webcounter" Alt="nice webcounter" border="0" style="text-align: center">

                            <p class="small-text">Powered <i class="fa fa-power-off"></i> by
                                <a href="http://kalpacreatives.com" target="_blank">Kalpa Creatives</a></p>
                        </div>
                    </div>
                </div>
                <!-- /.col-md-6 -->
            </div>
            <!-- /.copyright -->
        </div>
        <!-- /.container -->
    </div>
    <!-- /.main-footer -->
</footer>
<!-- /.site-footer -->

<a href="#top" id="top-link" class="fa fa-angle-up"></a>

<!-- JavaScripts -->
<script src="{{ asset('front/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('front/js/min/plugins.min.js') }}"></script>
<script src="{{ asset('front/js/min/custom.min.js') }}"></script>

@yield('scripts.footer')

</body>
</html>