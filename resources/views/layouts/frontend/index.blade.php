<!DOCTYPE html>
<html lang="en">
    @include('layouts.frontend.partials.head')
<body>
    @include('layouts.frontend.partials.header')

    @yield('content')

    @include('layouts.frontend.partials.footer')

    @include('layouts.frontend.partials.foot')
</body>
</html>