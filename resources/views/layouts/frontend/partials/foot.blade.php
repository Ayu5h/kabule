<script src="{{ asset('frontend')}}/js/jquery-1.11.1.min.js"></script>
<script src="{{ asset('frontend')}}/bootstrap/js/bootstrap.min.js"></script>
<script src="{{ asset('frontend')}}/js/bootstrap-hover-dropdown.min.js"></script>
<script src="{{ asset('frontend')}}/js/wow.min.js"></script>
<script src="{{ asset('frontend')}}/js/retina-1.1.0.min.js"></script>
<script src="{{ asset('frontend')}}/js/jquery.magnific-popup.min.js"></script>
<script src="{{ asset('frontend')}}/flexslider/jquery.flexslider-min.js"></script>
<script src="{{ asset('frontend')}}/js/jflickrfeed.min.js"></script>
<script src="{{ asset('frontend')}}/js/masonry.pkgd.min.js"></script>
<script src="http://maps.google.com/maps/api/js?sensor=true"></script>
<script src="{{ asset('frontend')}}/js/jquery.ui.map.min.js"></script>
<script src="{{ asset('frontend') }}/js/scripts.js"></script>