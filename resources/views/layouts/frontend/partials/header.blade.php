<nav class="navbar" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#top-navbar-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="" href="index.php" style="font-size:65px"><img src="{{ asset('frontend') }}/img/logo.jpg"></a>

            <div style="display:inline-block;">
                <h1>KABULE</h1>
                <h5>THE WISE LEADER</h5>
            </div>

        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="top-navbar-1">
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a href="index.php"><!-- <span aria-hidden="true" class="icon_house"></span> --><br>Training</a>
                </li>
                <li>
                    <a href="portfolio.php"><!-- <span aria-hidden="true" class="icon_camera"></span> -->
                        <br>Speakers</a>
                </li>
                <li>
                    <a href="about.php"><!-- <span aria-hidden="true" class="icon_comment"></span> --><br>About</a>
                </li>
                <li>
                    <a href="services.php"><!-- <span aria-hidden="true" class="icon_tools"></span> --><br>Events</a>
                </li>
                <li class="">
                    <a href="about.php"><!-- <span aria-hidden="true" class="icon_profile"></span> --><br>FAQ</a>
                </li>
                <li>
                    <a href="contact.php"><!-- <span aria-hidden="true" class="icon_mail"></span> --><br>Contact Us</a>
                </li>
            </ul>
        </div>
    </div>
</nav>