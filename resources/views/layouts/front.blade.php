<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>KABULE</title>
    <!--[if lt IE 9]>
    <script type="text/javascript" src="{{ asset('front/js/html5.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('front/style/ie.css') }}" type="text/css" media="all">
    <![endif]-->
    <link rel="stylesheet" href="{{ asset('front/style/style.css')  }}" type="text/css" media="all">
    <link rel="stylesheet" href="{{ asset('front/style/responsive.css')  }}" type="text/css" media="all">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('fonts/font-awesome-4.5.0/css/font-awesome.min.css') }}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{ asset('css/ionicons.min.css') }}">

    <link rel="stylesheet" href="{{ asset('front/style/doa/custom.css') }}" type="text/css" media="all"/>

    <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('front/images/favico/apple-icon-57x57.png') }}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('front/images/favico/apple-icon-60x60.png') }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('front/images/favico/apple-icon-72x72.png') }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('front/images/favico/apple-icon-76x76.png') }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('front/images/favico/apple-icon-114x114.png') }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('front/images/favico/apple-icon-120x120.png') }}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('front/images/favico/apple-icon-144x144.png') }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('front/images/favico/apple-icon-152x152.png') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('front/images/favico/apple-icon-180x180.png') }}">
    <link rel="icon" type="image/png" sizes="192x192"  href="{{ asset('front/images/favico/android-icon-192x192.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('front/images/favico/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('front/images/favico/favicon-96x96.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('front/images/favico/favicon-16x16.png') }}">
    <link rel="manifest" href="{{ asset('front/images/favico/manifest.json') }}">
</head>
<body class="wide">
<!--[if lt IE 8]>
<div class="sc_infobox sc_infobox_style_error">
    It looks like you're using an old version of Internet Explorer. For the best web site experience, please <a
        href="http://windows.microsoft.com/en-us/internet-explorer/ie-10-worldwide-languages">update your browser</a> or learn how to <a href="http://browsehappy.com">browse
    happy</a>!
</div>
<![endif]-->
<div id="page">

    @include('partials.front.header')

    <div class="right_sidebar" id="main">
        <div class="inner">
            <div class="general_content clearboth">
                <div class="main_content page">

                    @yield('content')

                </div>
                <!--/.main_content-->

                @include('partials.front.right_sidebar')

            </div>
            <!-- /.general_content -->
        </div>
        <!-- /.inner -->
    </div>
    <!-- /#main -->

    @include('partials.front.footer')

    <!-- PopUp -->
    <div id="overlay"></div>

    @include('partials.front.login_popup')

    @include('partials.front.registration_popup')

    <!-- go Top-->
    <a id="toTop" href="#"><span></span></a></div>
<!--page-->

<script type="text/javascript" src="{{ asset('front/js/jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('front/js/jquery-ui-1.9.2.custom.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('front/js/superfish.js') }}"></script>
<script type="text/javascript" src="{{ asset('front/js/jquery.jclock.js') }}"></script>
<script type="text/javascript" src="{{ asset('front/js/jquery.flexslider-min.js') }}"></script>
<script type="text/javascript" src="{{ asset('front/js/jquery.prettyPhoto.js') }}"></script>
<script type="text/javascript" src="{{ asset('front/js/jquery.jcarousel.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('front/js/jquery.elastislide.js') }}"></script>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript" src="{{ asset('front/js/googlemap_init.js') }}"></script>
<script type="text/javascript" src="{{ asset('front/js/mediaelement.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('front/js/lib.js') }}"></script>

@yield('scripts.footer')


</body>
</html>