<h3>Hi Admin! You have new Contact via {{ $name }}</h3>

<h6>{{ $subject }}</h6>

<div>
    {{ $bodyMessage }}
</div>

<p>Sent Via {{ $email }}</p>